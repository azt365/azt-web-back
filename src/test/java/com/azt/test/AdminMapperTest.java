package com.azt.test;

import java.util.List;

import javax.annotation.Resource;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.azt.api.pojo.Admin;
import com.azt.provider.mapper.AdminMapper;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({"classpath:spring/spring-mybatis.xml"})
public class AdminMapperTest {

	private static final Logger log = LoggerFactory.getLogger(AdminMapperTest.class);
	
	@Resource
	private AdminMapper  dao;
	
	@Test
	public void testGetEnqEnquiryByPage() throws Exception{
		Admin adminByName = dao.getAdminByName("zhnagdong");
		System.out.println(adminByName.getName());
		
		try {
			List<Admin> allAdmin = dao.getAllAdmin();
			System.out.println(allAdmin.get(0).getRolenames());
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	@Test
	public void testGetAdmin() throws Exception{
		try {
			Admin adminByName = dao.getAdminByName("zhangdong");
			System.out.println(adminByName.getName());
			System.out.println(adminByName.getRolenames());
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}

	
	
	
}
