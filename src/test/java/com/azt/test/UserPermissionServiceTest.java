package com.azt.test;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.azt.api.service.UserPermissionService;
import com.azt.api.service.UserService;
import com.azt.provider.mapper.CompanyMapper;
import com.azt.provider.mapper.GiftInfoMapper;
import com.azt.provider.mapper.IntegralRecordMapper;
import com.azt.provider.mapper.PrizeRecordMapper;
import com.azt.provider.mapper.PrizeSetMapper;
import com.azt.provider.mapper.UserRoleMapper;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({"classpath:spring/spring-service.xml"})
public class UserPermissionServiceTest {

	private static final Logger log = LoggerFactory.getLogger(UserPermissionServiceTest.class);
	@Autowired
	private UserPermissionService userPermissionService;
	@Resource
	private UserRoleMapper userRoleMapper;
	@Resource
	private GiftInfoMapper giftInfoMapper;
	@Autowired
	private PrizeSetMapper prizeSetMapper;

	@Autowired
	private PrizeRecordMapper prizeRecordMapper;
	@Autowired
	private IntegralRecordMapper integralRecordMapper;
	@Autowired
	private UserService userService;
	
	@Autowired
	private CompanyMapper companyMapper;
	@Test
	public void  getAllBranchCompany(){
		Map<String, String> params=new HashMap();
		params.put("so_customerinfo", "体验使用");
		companyMapper.queryCompanysBySQL(params);
	}
/*	@Test
	public void getPrizeRecord(){
		Map<String,String> params=new HashMap<String,String>();
		prizeRecordMapper.selectRecordList(params);
	}
	
	@Test
	public void getuserRoleMapper(){
		userRoleMapper.getUserRoleById(29049);
	}
	@Test
	public void getUserByIngralRule(){
//		integralRecordMapper.getUserByIngralRule("2017-01-01");
	}
	*/
/*	@Test
	public void testUserService(){
		UserApplyCompany ua=new UserApplyCompany();
		ua.setUserId(167);
		ua.setCompanyId(113);
		ua.setId(1);
		ua.setAuditState(0);
		ua.setRoleId(1);
		userService.agreeUserApply(ua);
	}*/
	
}
