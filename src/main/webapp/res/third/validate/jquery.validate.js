loadjscssfile("/res/third/validate/css/valid.css","css");
loadjscssfile("/res/third/powerFloat/css/powerFloat.css","css");
loadjscssfile("/res/third/powerFloat/js/mini/jquery-powerFloat-min.js","js");

/*!
 * jQuery Validation Plugin v1.13.0
 *
 * http://jqueryvalidation.org/
 *
 * Copyright (c) 2014 Jörn Zaefferer
 * Released under the MIT license
 */
(function(a) {
    if (typeof define === "function" && define.amd) {
        define(["jquery"], a)
    } else {
        a(jQuery)
    }
} (function(b) {
    b.extend(b.fn, {
        validate: function(e) {
            if (!this.length) {
                if (e && e.debug && window.console) {
                    console.warn("Nothing selected, can't validate, returning nothing.")
                }
                return
            }
            var f = b.data(this[0], "validator");
            if (f) {
                return f
            }
            this.attr("novalidate", "novalidate");
            f = new b.validator(e, this[0]);
            b.data(this[0], "validator", f);
            if (f.settings.onsubmit) {
                this.validateDelegate(":submit", "click",
                function(g) {
                    if (f.settings.submitHandler) {
                        f.submitButton = g.target
                    }
                    if (b(g.target).hasClass("cancel")) {
                        f.cancelSubmit = true
                    }
                    if (b(g.target).attr("formnovalidate") !== undefined) {
                        f.cancelSubmit = true
                    }
                });
                this.submit(function(g) {
                    if (f.settings.debug) {
                        g.preventDefault()
                    }
                    function h() {
                        var i;
                        if (f.settings.submitHandler) {
                            if (f.submitButton) {
                                i = b("<input type='hidden'/>").attr("name", f.submitButton.name).val(b(f.submitButton).val()).appendTo(f.currentForm)
                            }
                            f.settings.submitHandler.call(f, f.currentForm, g);
                            if (f.submitButton) {
                                i.remove()
                            }
                            return false
                        }
                        return true
                    }
                    if (f.cancelSubmit) {
                        f.cancelSubmit = false;
                        return h()
                    }
                    if (f.form()) {
                        if (f.pendingRequest) {
                            f.formSubmitted = true;
                            return false
                        }
                        return h()
                    } else {
                        f.focusInvalid();
                        return false
                    }
                })
            }
            return f
        },
        valid: function() {
            var f, e;
            if (b(this[0]).is("form")) {
                f = this.validate().form()
            } else {
                f = true;
                e = b(this[0].form).validate();
                this.each(function() {
                    f = e.element(this) && f
                })
            }
            return f
        },
        removeAttrs: function(f) {
            var g = {},
            e = this;
            b.each(f.split(/\s/),
            function(h, i) {
                g[i] = e.attr(i);
                e.removeAttr(i)
            });
            return g
        },
        rules: function(h, e) {
            var j = this[0],g,l,m,i,f,k;
            if (h) {
                g = b.data(j.form, "validator").settings;
                l = g.rules;
                m = b.validator.staticRules(j);
                switch (h) {
                case "add":
                    b.extend(m, b.validator.normalizeRule(e));
                    delete m.messages;
                    l[j.name] = m;
                    if (e.messages) {
                        g.messages[j.name] = b.extend(g.messages[j.name], e.messages)
                    }
                    break;
                case "remove":
                    if (!e) {
                        delete l[j.name];
                        return m
                    }
                    k = {};
                    b.each(e.split(/\s/),
                    function(n, o) {
                        k[o] = m[o];
                        delete m[o];
                        if (o === "required") {
                            b(j).removeAttr("aria-required")
                        }
                    });
                    return k
                }
            }
            i = b.validator.normalizeRules(b.extend({},
            b.validator.classRules(j), b.validator.attributeRules(j), b.validator.dataRules(j), b.validator.staticRules(j)), j);
            if (i.required) {
                f = i.required;
                delete i.required;
                i = b.extend({
                    required: f
                },
                i);
                b(j).attr("aria-required", "true")
            }
            if (i.remote) {
                f = i.remote;
                delete i.remote;
                i = b.extend(i, {
                    remote: f
                })
            }
            return i
        }
    });
    b.extend(b.expr[":"], {
        blank: function(e) {
            return ! b.trim("" + b(e).val())
        },
        filled: function(e) {
            return !! b.trim("" + b(e).val())
        },
        unchecked: function(e) {
            return ! b(e).prop("checked")
        }
    });
    b.validator = function(e, f) {
        this.settings = b.extend(true, {},
        b.validator.defaults, e);
        this.currentForm = f;
        this.init()
    };
    b.validator.format = function(e, f) {
        if (arguments.length === 1) {
            return function() {
                var g = b.makeArray(arguments);
                g.unshift(e);
                return b.validator.format.apply(this, g)
            }
        }
        if (arguments.length > 2 && f.constructor !== Array) {
            f = b.makeArray(arguments).slice(1)
        }
        if (f.constructor !== Array) {
            f = [f]
        }
        b.each(f,
        function(g, h) {
            e = e.replace(new RegExp("\\{" + g + "\\}", "g"),
            function() {
                return h
            })
        });
        return e
    };
    b.extend(b.validator, {
        defaults: {
    		showErrors: function(map, list) {
    			var focussed = document.activeElement;
    			this.currentElements.removeAttr("rel").removeClass("valid_false").removeClass("valid_true");
    			$.each(list, function(index, error) {
    				$(error.element).attr("rel", error.message).addClass("valid_false").powerFloat({targetMode: "remind",zIndex:9999});
    			});
    			$.each(this.successList, function(index, suc) {
    				$(suc).removeAttr("rel").addClass("valid_true");
    			});
    		},
            messages: {},
            groups: {},
            rules: {},
            errorClass: "error",
            validClass: "valid",
            errorElement: "label",
            focusInvalid: true,
            errorContainer: b([]),
            errorLabelContainer: b([]),
            onsubmit: true,
            ignore: ":hidden",
            ignoreTitle: false,
            onfocusin: function(e) {
                this.lastActive = e;
                if (this.settings.focusCleanup && !this.blockFocusCleanup) {
                    if (this.settings.unhighlight) {
                        this.settings.unhighlight.call(this, e, this.settings.errorClass, this.settings.validClass)
                    }
                    this.hideThese(this.errorsFor(e))
                }
            },
            onfocusout: function(e) {
                if (!this.checkable(e) && (e.name in this.submitted || !this.optional(e))) {
                    this.element(e)
                }
            },
            onkeyup: function(e, f) {
                if (f.which === 9 && this.elementValue(e) === "") {
                    return
                } else {
                    if (e.name in this.submitted || e === this.lastElement) {
                        this.element(e)
                    }
                }
            },
            onclick: function(e) {
                if (e.name in this.submitted) {
                    this.element(e)
                } else {
                    if (e.parentNode.name in this.submitted) {
                        this.element(e.parentNode)
                    }
                }
            },
            highlight: function(f, g, e) {
                if (f.type === "radio") {
                    this.findByName(f.name).addClass(g).removeClass(e)
                } else {
                    b(f).addClass(g).removeClass(e)
                }
            },
            unhighlight: function(f, g, e) {
                if (f.type === "radio") {
                    this.findByName(f.name).removeClass(g).addClass(e)
                } else {
                    b(f).removeClass(g).addClass(e)
                }
            }
        },
        setDefaults: function(e) {
            b.extend(b.validator.defaults, e)
        },
        messages: {
    		required: "此项内容必须填写",
    		remote: "请修正此栏位",
    		email: "请输入有效的电子邮件",
    		url: "请输入有效的网址",
    		date: "请输入有效的日期",
    		dateISO: "请输入有效的日期 (YYYY-MM-DD)",
    		number: "请输入正确的数字",
    		digits: "只可输入大于0的数字",
    		creditcard: "请输入有效的信用卡号码",
    		equalTo: "你的输入不相同",
    		extension: "请输入有效的后缀",
    		maxlength: $.validator.format("最多 {0} 个字"),
    		minlength: $.validator.format("最少 {0} 个字"),
    		rangelength: $.validator.format("请输入长度为 {0} 至 {1}"),
    		range: $.validator.format("请输入 {0} 至 {1} 之间的数值"),
    		max: $.validator.format("请输入不大于 {0} 的数值"),
    		min: $.validator.format("请输入不小于 {0} 的数值")
        },
        autoCreateRanges: false,
        prototype: {
            init: function() {
                this.labelContainer = b(this.settings.errorLabelContainer);
                this.errorContext = this.labelContainer.length && this.labelContainer || b(this.currentForm);
                this.containers = b(this.settings.errorContainer).add(this.settings.errorLabelContainer);
                this.submitted = {};
                this.valueCache = {};
                this.pendingRequest = 0;
                this.pending = {};
                this.invalid = {};
                this.reset();
                var g = (this.groups = {}),
                f;
                b.each(this.settings.groups,
                function(h, i) {
                    if (typeof i === "string") {
                        i = i.split(/\s/)
                    }
                    b.each(i,
                    function(k, j) {
                        g[j] = h
                    })
                });
                f = this.settings.rules;
                b.each(f,
                function(h, i) {
                    f[h] = b.validator.normalizeRule(i)
                });
                function e(j) {
                    var h = b.data(this[0].form, "validator"),
                    k = "on" + j.type.replace(/^validate/, ""),
                    i = h.settings;
                    if (i[k] && !this.is(i.ignore)) {
                        i[k].call(h, this[0], j)
                    }
                }
                b(this.currentForm).validateDelegate(":text, [type='password'], [type='file'], select, textarea, [type='number'], [type='search'] ,[type='tel'], [type='url'], [type='email'], [type='datetime'], [type='date'], [type='month'], [type='week'], [type='time'], [type='datetime-local'], [type='range'], [type='color'], [type='radio'], [type='checkbox']", "focusin focusout keyup", e).validateDelegate("select, option, [type='radio'], [type='checkbox']", "click", e);
                if (this.settings.invalidHandler) {
                    b(this.currentForm).bind("invalid-form.validate", this.settings.invalidHandler)
                }
                b(this.currentForm).find("[required], [data-rule-required], .required").attr("aria-required", "true")
            },
            form: function() {
                this.checkForm();
                b.extend(this.submitted, this.errorMap);
                this.invalid = b.extend({},
                this.errorMap);
                if (!this.valid()) {
                    b(this.currentForm).triggerHandler("invalid-form", [this])
                }
                this.showErrors();
                return this.valid()
            },
            checkForm: function() {
                this.prepareForm();
                for (var e = 0,
                f = (this.currentElements = this.elements()); f[e]; e++) {
                    this.check(f[e])
                }
                return this.valid()
            },
            element: function(e) {
                var f = this.clean(e),
                h = this.validationTargetFor(f),
                g = true;
                this.lastElement = h;
                if (h === undefined) {
                    delete this.invalid[f.name]
                } else {
                    this.prepareElement(h);
                    this.currentElements = b(h);
                    g = this.check(h) !== false;
                    if (g) {
                        delete this.invalid[h.name]
                    } else {
                        this.invalid[h.name] = true
                    }
                }
                b(e).attr("aria-invalid", !g);
                if (!this.numberOfInvalids()) {
                    this.toHide = this.toHide.add(this.containers)
                }
                this.showErrors();
                return g
            },
            showErrors: function(f) {
                if (f) {
                    b.extend(this.errorMap, f);
                    this.errorList = [];
                    for (var e in f) {
                        this.errorList.push({
                            message: f[e],
                            element: this.findByName(e)[0]
                        })
                    }
                    this.successList = b.grep(this.successList,
                    function(g) {
                        return ! (g.name in f)
                    })
                }
                if (this.settings.showErrors) {
                    this.settings.showErrors.call(this, this.errorMap, this.errorList)
                } else {
                    this.defaultShowErrors()
                }
            },
            resetForm: function() {
                if (b.fn.resetForm) {
                    b(this.currentForm).resetForm()
                }
                this.submitted = {};
                this.lastElement = null;
                this.prepareForm();
                this.hideErrors();
                this.elements().removeClass(this.settings.errorClass).removeData("previousValue").removeAttr("aria-invalid")
            },
            numberOfInvalids: function() {
                return this.objectLength(this.invalid)
            },
            objectLength: function(f) {
                var e = 0,
                g;
                for (g in f) {
                    e++
                }
                return e
            },
            hideErrors: function() {
                this.hideThese(this.toHide)
            },
            hideThese: function(e) {
                e.not(this.containers).text("");
                this.addWrapper(e).hide()
            },
            valid: function() {
                return this.size() === 0
            },
            size: function() {
                return this.errorList.length
            },
            focusInvalid: function() {
                if (this.settings.focusInvalid) {
                    try {
                        b(this.findLastActive() || this.errorList.length && this.errorList[0].element || []).filter(":visible").focus().trigger("focusin")
                    } catch(e) {}
                }
            },
            findLastActive: function() {
                var e = this.lastActive;
                return e && b.grep(this.errorList,
                function(f) {
                    return f.element.name === e.name
                }).length === 1 && e
            },
            elements: function() {
                var f = this,
                e = {};
                return b(this.currentForm).find("input, select, textarea").not(":submit, :reset, :image, [disabled]").not(this.settings.ignore).filter(function() {
                    if (!this.name && f.settings.debug && window.console) {
                        console.error("%o has no name assigned", this)
                    }
                    if (this.name in e || !f.objectLength(b(this).rules())) {
                        return false
                    }
                    e[this.name] = true;
                    return true
                })
            },
            clean: function(e) {
                return b(e)[0]
            },
            errors: function() {
                var e = this.settings.errorClass.split(" ").join(".");
                return b(this.settings.errorElement + "." + e, this.errorContext)
            },
            reset: function() {
                this.successList = [];
                this.errorList = [];
                this.errorMap = {};
                this.toShow = b([]);
                this.toHide = b([]);
                this.currentElements = b([])
            },
            prepareForm: function() {
                this.reset();
                this.toHide = this.errors().add(this.containers)
            },
            prepareElement: function(e) {
                this.reset();
                this.toHide = this.errorsFor(e)
            },
            elementValue: function(h) {
                var f, g = b(h),
                e = h.type;
                if (e === "radio" || e === "checkbox") {
                    return b("input[name='" + h.name + "']:checked").val()
                } else {
                    if (e === "number" && typeof h.validity !== "undefined") {
                        return h.validity.badInput ? false: g.val()
                    }
                }
                f = g.val();
                if (typeof f === "string") {
                    return f.replace(/\r/g, "")
                }
                return f
            },
            check: function(f) {
                f = this.validationTargetFor(this.clean(f));
                var j = b(f).rules(),
                h = b.map(j,
                function(n, o) {
                    return o
                }).length,
                k = false,
                e = this.elementValue(f),
                l,
                m,
                i;
                for (m in j) {
                    i = {
                        method: m,
                        parameters: j[m]
                    };
                    try {
                        l = b.validator.methods[m].call(this, e, f, i.parameters);
                        if (l === "dependency-mismatch" && h === 1) {
                            k = true;
                            continue
                        }
                        k = false;
                        if (l === "pending") {
                            this.toHide = this.toHide.not(this.errorsFor(f));
                            return
                        }
                        if (!l) {
                            this.formatAndAdd(f, i);
                            return false
                        }
                    } catch(g) {
                        if (this.settings.debug && window.console) {
                            console.log("Exception occurred when checking element " + f.id + ", check the '" + i.method + "' method.", g)
                        }
                        throw g
                    }
                }
                if (k) {
                    return
                }
                if (this.objectLength(j)) {
                    this.successList.push(f)
                }
                return true
            },
            customDataMessage: function(e, f) {
                return b(e).data("msg" + f.charAt(0).toUpperCase() + f.substring(1).toLowerCase()) || b(e).data("msg")
            },
            customMessage: function(e, f) {
                var g = this.settings.messages[e];
                return g && (g.constructor === String ? g: g[f])
            },
            findDefined: function() {
                for (var e = 0; e < arguments.length; e++) {
                    if (arguments[e] !== undefined) {
                        return arguments[e]
                    }
                }
                return undefined
            },
            defaultMessage: function(e, f) {
                return this.findDefined(this.customMessage(e.name, f), this.customDataMessage(e, f), !this.settings.ignoreTitle && e.title || undefined, b.validator.messages[f], "<strong>Warning: No message defined for " + e.name + "</strong>")
            },
            formatAndAdd: function(h, f) {
                var e = this.defaultMessage(h, f.method),
                g = /\$?\{(\d+)\}/g;
                if (typeof e === "function") {
                    e = e.call(this, f.parameters, h)
                } else {
                    if (g.test(e)) {
                        e = b.validator.format(e.replace(g, "{$1}"), f.parameters)
                    }
                }
                this.errorList.push({
                    message: e,
                    element: h,
                    method: f.method
                });
                this.errorMap[h.name] = e;
                this.submitted[h.name] = e
            },
            addWrapper: function(e) {
                if (this.settings.wrapper) {
                    e = e.add(e.parent(this.settings.wrapper))
                }
                return e
            },
            defaultShowErrors: function() {
                var e, f, g;
                for (e = 0; this.errorList[e]; e++) {
                    g = this.errorList[e];
                    if (this.settings.highlight) {
                        this.settings.highlight.call(this, g.element, this.settings.errorClass, this.settings.validClass)
                    }
                    this.showLabel(g.element, g.message)
                }
                if (this.errorList.length) {
                    this.toShow = this.toShow.add(this.containers)
                }
                if (this.settings.success) {
                    for (e = 0; this.successList[e]; e++) {
                        this.showLabel(this.successList[e])
                    }
                }
                if (this.settings.unhighlight) {
                    for (e = 0, f = this.validElements(); f[e]; e++) {
                        this.settings.unhighlight.call(this, f[e], this.settings.errorClass, this.settings.validClass)
                    }
                }
                this.toHide = this.toHide.not(this.toShow);
                this.hideErrors();
                this.addWrapper(this.toShow).show()
            },
            validElements: function() {
                return this.currentElements.not(this.invalidElements())
            },
            invalidElements: function() {
                return b(this.errorList).map(function() {
                    return this.element
                })
            },
            showLabel: function(j, l) {
                var i, g, f, k = this.errorsFor(j),
                h = this.idOrName(j),
                e = b(j).attr("aria-describedby");
                if (k.length) {
                    k.removeClass(this.settings.validClass).addClass(this.settings.errorClass);
                    k.html(l)
                } else {
                    k = b("<" + this.settings.errorElement + ">").attr("id", h + "-error").addClass(this.settings.errorClass).html(l || "");
                    i = k;
                    if (this.settings.wrapper) {
                        i = k.hide().show().wrap("<" + this.settings.wrapper + "/>").parent()
                    }
                    if (this.labelContainer.length) {
                        this.labelContainer.append(i)
                    } else {
                        if (this.settings.errorPlacement) {
                            this.settings.errorPlacement(i, b(j))
                        } else {
                            i.insertAfter(j)
                        }
                    }
                    if (k.is("label")) {
                        k.attr("for", h)
                    } else {
                        if (k.parents("label[for='" + h + "']").length === 0) {
                            f = k.attr("id");
                            if (!e) {
                                e = f
                            } else {
                                if (!e.match(new RegExp("\b" + f + "\b"))) {
                                    e += " " + f
                                }
                            }
                            b(j).attr("aria-describedby", e);
                            g = this.groups[j.name];
                            if (g) {
                                b.each(this.groups,
                                function(m, n) {
                                    if (n === g) {
                                        b("[name='" + m + "']", this.currentForm).attr("aria-describedby", k.attr("id"))
                                    }
                                })
                            }
                        }
                    }
                }
                if (!l && this.settings.success) {
                    k.text("");
                    if (typeof this.settings.success === "string") {
                        k.addClass(this.settings.success)
                    } else {
                        this.settings.success(k, j)
                    }
                }
                this.toShow = this.toShow.add(k)
            },
            errorsFor: function(e) {
                var h = this.idOrName(e),
                f = b(e).attr("aria-describedby"),
                g = "label[for='" + h + "'], label[for='" + h + "'] *";
                if (f) {
                    g = g + ", #" + f.replace(/\s+/g, ", #")
                }
                return this.errors().filter(g)
            },
            idOrName: function(e) {
                return this.groups[e.name] || (this.checkable(e) ? e.name: e.id || e.name)
            },
            validationTargetFor: function(e) {
                if (this.checkable(e)) {
                    e = this.findByName(e.name).not(this.settings.ignore)[0]
                }
                return e
            },
            checkable: function(e) {
                return (/radio|checkbox/i).test(e.type)
            },
            findByName: function(e) {
                return b(this.currentForm).find("[name='" + e + "']")
            },
            getLength: function(f, e) {
                switch (e.nodeName.toLowerCase()) {
                case "select":
                    return b("option:selected", e).length;
                case "input":
                    if (this.checkable(e)) {
                        return this.findByName(e.name).filter(":checked").length
                    }
                }
                return f.length
            },
            depend: function(f, e) {
                return this.dependTypes[typeof f] ? this.dependTypes[typeof f](f, e) : true
            },
            dependTypes: {
                "boolean": function(e) {
                    return e
                },
                string: function(f, e) {
                    return !! b(f, e.form).length
                },
                "function": function(f, e) {
                    return f(e)
                }
            },
            optional: function(e) {
                var f = this.elementValue(e);
                return ! b.validator.methods.required.call(this, f, e) && "dependency-mismatch"
            },
            startRequest: function(e) {
                if (!this.pending[e.name]) {
                    this.pendingRequest++;
                    this.pending[e.name] = true
                }
            },
            stopRequest: function(e, f) {
                this.pendingRequest--;
                if (this.pendingRequest < 0) {
                    this.pendingRequest = 0
                }
                delete this.pending[e.name];
                if (f && this.pendingRequest === 0 && this.formSubmitted && this.form()) {
                    b(this.currentForm).submit();
                    this.formSubmitted = false
                } else {
                    if (!f && this.pendingRequest === 0 && this.formSubmitted) {
                        b(this.currentForm).triggerHandler("invalid-form", [this]);
                        this.formSubmitted = false
                    }
                }
            },
            previousValue: function(e) {
                return b.data(e, "previousValue") || b.data(e, "previousValue", {
                    old: null,
                    valid: true,
                    message: this.defaultMessage(e, "remote")
                })
            }
        },
        classRuleSettings: {
            required: {
                required: true
            },
            email: {
                email: true
            },
            url: {
                url: true
            },
            date: {
                date: true
            },
            dateISO: {
                dateISO: true
            },
            number: {
                number: true
            },
            digits: {
                digits: true
            },
            creditcard: {
                creditcard: true
            }
        },
        addClassRules: function(e, f) {
            if (e.constructor === String) {
                this.classRuleSettings[e] = f
            } else {
                b.extend(this.classRuleSettings, e)
            }
        },
        classRules: function(e) {
            var f = {},
            g = b(e).attr("class");
            if (g) {
                b.each(g.split(" "),
                function() {
                    if (this in b.validator.classRuleSettings) {
                        b.extend(f, b.validator.classRuleSettings[this])
                    }
                })
            }
            return f
        },
        attributeRules: function(h) {
            var e = {},
            g = b(h),
            i = h.getAttribute("type"),
            f,
            j;
            for (f in b.validator.methods) {
                if (f === "required") {
                    j = h.getAttribute(f);
                    if (j === "") {
                        j = true
                    }
                    j = !!j
                } else {
                    j = g.attr(f)
                }
                if (/min|max/.test(f) && (i === null || /number|range|text/.test(i))) {
                    j = Number(j)
                }
                if (j || j === 0) {
                    e[f] = j
                } else {
                    if (i === f && i !== "range") {
                        e[f] = true
                    }
                }
            }
            if (e.maxlength && /-1|2147483647|524288/.test(e.maxlength)) {
                delete e.maxlength
            }
            return e
        },
        dataRules: function(h) {
            var f, i, e = {},
            g = b(h);
            for (f in b.validator.methods) {
                i = g.data("rule" + f.charAt(0).toUpperCase() + f.substring(1).toLowerCase());
                if (i !== undefined) {
                    e[f] = i
                }
            }
            return e
        },
        staticRules: function(e) {
            var f = {},
            g = b.data(e.form, "validator");
            if (g.settings.rules) {
                f = b.validator.normalizeRule(g.settings.rules[e.name]) || {}
            }
            return f
        },
        normalizeRules: function(f, e) {
            b.each(f,
            function(i, h) {
                if (h === false) {
                    delete f[i];
                    return
                }
                if (h.param || h.depends) {
                    var g = true;
                    switch (typeof h.depends) {
                    case "string":
                        g = !!b(h.depends, e.form).length;
                        break;
                    case "function":
                        g = h.depends.call(e, e);
                        break
                    }
                    if (g) {
                        f[i] = h.param !== undefined ? h.param: true
                    } else {
                        delete f[i]
                    }
                }
            });
            b.each(f,
            function(g, h) {
                f[g] = b.isFunction(h) ? h(e) : h
            });
            b.each(["minlength", "maxlength"],
            function() {
                if (f[this]) {
                    f[this] = Number(f[this])
                }
            });
            b.each(["rangelength", "range"],
            function() {
                var g;
                if (f[this]) {
                    if (b.isArray(f[this])) {
                        f[this] = [Number(f[this][0]), Number(f[this][1])]
                    } else {
                        if (typeof f[this] === "string") {
                            g = f[this].replace(/[\[\]]/g, "").split(/[\s,]+/);
                            f[this] = [Number(g[0]), Number(g[1])]
                        }
                    }
                }
            });
            if (b.validator.autoCreateRanges) {
                if (f.min && f.max) {
                    f.range = [f.min, f.max];
                    delete f.min;
                    delete f.max
                }
                if (f.minlength && f.maxlength) {
                    f.rangelength = [f.minlength, f.maxlength];
                    delete f.minlength;
                    delete f.maxlength
                }
            }
            return f
        },
        normalizeRule: function(f) {
            if (typeof f === "string") {
                var e = {};
                b.each(f.split(/\s/),
                function() {
                    e[this] = true
                });
                f = e
            }
            return f
        },
        addMethod: function(g, f, e) {
            b.validator.methods[g] = f;
            b.validator.messages[g] = e !== undefined ? e: b.validator.messages[g];
            if (f.length < 3) {
                b.validator.addClassRules(g, b.validator.normalizeRule(g))
            }
        },
        methods: {
            required: function(h, g, f) {
                if (!this.depend(f, g)) {
                    return "dependency-mismatch"
                }
                if (g.nodeName.toLowerCase() === "select") {
                    var e = b(g).val();
                    return e && e.length > 0
                }
                if (this.checkable(g)) {
                    return this.getLength(h, g) > 0
                }
                return b.trim(h).length > 0
            },
            email: function(f, e) {
                return this.optional(e) || /^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/.test(f)
            },
            url: function(f, e) {
                return this.optional(e) || /^(https?|s?ftp):\/\/(((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:)*@)?(((\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5]))|((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?)(:\d*)?)(\/((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)+(\/(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)*)*)?)?(\?((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|[\uE000-\uF8FF]|\/|\?)*)?(#((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|\/|\?)*)?$/i.test(f)
            },
            date: function(f, e) {
                return this.optional(e) || !/Invalid|NaN/.test(new Date(f).toString())
            },
            dateISO: function(f, e) {
                return this.optional(e) || /^\d{4}[\/\-](0?[1-9]|1[012])[\/\-](0?[1-9]|[12][0-9]|3[01])$/.test(f)
            },
            number: function(f, e) {
                return this.optional(e) || /^-?(?:\d+|\d{1,3}(?:,\d{3})+)?(?:\.\d+)?$/.test(f)
            },
            digits: function(f, e) {
                return this.optional(e) || /^\d+$/.test(f)
            },
            creditcard: function(k, f) {
                if (this.optional(f)) {
                    return "dependency-mismatch"
                }
                if (/[^0-9 \-]+/.test(k)) {
                    return false
                }
                var e = 0,
                i = 0,
                j = false,
                h, g;
                k = k.replace(/\D/g, "");
                if (k.length < 13 || k.length > 19) {
                    return false
                }
                for (h = k.length - 1; h >= 0; h--) {
                    g = k.charAt(h);
                    i = parseInt(g, 10);
                    if (j) {
                        if ((i *= 2) > 9) {
                            i -= 9
                        }
                    }
                    e += i;
                    j = !j
                }
                return (e % 10) === 0
            },
            minlength: function(e, g, f) {
                var h = b.isArray(e) ? e.length: this.getLength(b.trim(e), g);
                return this.optional(g) || h >= f
            },
            maxlength: function(e, g, f) {
                var h = b.isArray(e) ? e.length: this.getLength(b.trim(e), g);
                return this.optional(g) || h <= f
            },
            rangelength: function(e, g, f) {
                var h = b.isArray(e) ? e.length: this.getLength(b.trim(e), g);
                return this.optional(g) || (h >= f[0] && h <= f[1])
            },
            min: function(e, g, f) {
                return this.optional(g) || e >= f
            },
            max: function(e, g, f) {
                return this.optional(g) || e <= f
            },
            range: function(e, g, f) {
                return this.optional(g) || (e >= f[0] && e <= f[1])
            },
            equalTo: function(h, g, f) {
                var e = b(f);
                if (this.settings.onfocusout) {
                    e.unbind(".validate-equalTo").bind("blur.validate-equalTo",
                    function() {
                        b(g).valid()
                    })
                }
                return h === e.val()
            },
            remote: function(e, h, g) {
                if (this.optional(h)) {
                    return "dependency-mismatch"
                }
                var i = this.previousValue(h),
                f,
                j;
                if (!this.settings.messages[h.name]) {
                    this.settings.messages[h.name] = {}
                }
                i.originalMessage = this.settings.messages[h.name].remote;
                this.settings.messages[h.name].remote = i.message;
                g = typeof g === "string" && {
                    url: g
                } || g;
                if (i.old === e) {
                    return i.valid
                }
                i.old = e;
                f = this;
                this.startRequest(h);
                j = {};
                j[h.name] = e;
                b.ajax(b.extend(true, {
                    url: g,
                    mode: "abort",
                    port: "validate" + h.name,
                    dataType: "json",
                    data: j,
                    context: f.currentForm,
                    success: function(k) {
                        var m = k === true || k === "true",
                        n, l, o;
                        f.settings.messages[h.name].remote = i.originalMessage;
                        if (m) {
                            o = f.formSubmitted;
                            f.prepareElement(h);
                            f.formSubmitted = o;
                            f.successList.push(h);
                            delete f.invalid[h.name];
                            f.showErrors()
                        } else {
                            n = {};
                            l = k || f.defaultMessage(h, "remote");
                            n[h.name] = i.message = b.isFunction(l) ? l(e) : l;
                            f.invalid[h.name] = true;
                            f.showErrors(n)
                        }
                        i.valid = m;
                        f.stopRequest(h, m)
                    }
                },
                g));
                return "pending"
            }
        }
    });
    b.format = function c() {
        throw "$.format has been deprecated. Please use $.validator.format instead."
    };
    var d = {},
    a;
    if (b.ajaxPrefilter) {
        b.ajaxPrefilter(function(e, h, f) {
            var g = e.port;
            if (e.mode === "abort") {
                if (d[g]) {
                    d[g].abort()
                }
                d[g] = f
            }
        })
    } else {
        a = b.ajax;
        b.ajax = function(e) {
            var f = ("mode" in e ? e: b.ajaxSettings).mode,
            g = ("port" in e ? e: b.ajaxSettings).port;
            if (f === "abort") {
                if (d[g]) {
                    d[g].abort()
                }
                d[g] = a.apply(this, arguments);
                return d[g]
            }
            return a.apply(this, arguments)
        }
    }
    b.extend(b.fn, {
        validateDelegate: function(f, e, g) {
            return this.bind(e,
            function(h) {
                var i = b(h.target);
                if (i.is(f)) {
                    return g.apply(i, arguments)
                }
            })
        }
    })
}));								
												
												
$.validator.addMethod("regExp", function(value, element, params) {
	if (this.optional(element)) {
		return true
	}
	var b = new RegExp(params);
	if (b.test($.trim(value))) {
		return true
	} else {
		return false
	}
});


$.validator.addMethod("mobile", function(value, element, params) {
	if(value=="")
		return true;
	if (/^1[3|4|5|8][0-9]\d{8}$/.test(value)) {
		return true
	} else {
		return false
	}
}, "请填写正确的手机号码");

