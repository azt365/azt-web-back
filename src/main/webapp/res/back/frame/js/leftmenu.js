﻿$(function() {
	history.pushState({},"","/");
	tabClose();
	tabCloseEven();
	// 释放内存
	$.fn.panel.defaults = $.extend({}, $.fn.panel.defaults, {
		onBeforeDestroy : function() {
			var frame = $('iframe', this);
			if (frame.length > 0) {
				frame[0].contentWindow.document.write('');
				frame[0].contentWindow.close();
				frame.remove();
			}
		}
	});

	$(".left-menu li").click(function(){
		$(".left-menu .active").removeClass("active");
		$(this).addClass("active");
	});
	$("._icons li").hover(function(){
		$("#big_ico").removeAttr("class").addClass($(this).attr("class"));
	});
	$("._icons li").click(function(){
		var newc =$(this).attr("class");
		var b = $("#icon_"+currentid).hasClass(newc);
		if(!b){
			$("#icon_"+currentid).removeAttr("class").addClass(newc);
			$.get("/function/addIcon.do",{id:currentid,icon:newc});
		}
        $("#for_icons").fadeOut();
	});

	$('#maintabs').tabs({
		onSelect:function (a, b) {
			var tab = $('#maintabs').tabs('getSelected');
			var l = tab.find("iframe");
			if(l!=0){
				var url = l.attr("src");
				if(url!=null){
					window.location.hash=url;
				}
			}
		}
	});
});

function chooseIcon(id){
	currentid =id;
	$("#for_icons").show();
	return false;
}
function firstclick(){
	if($(".active a").size()!=0){
		var href = $(".active a").attr("href");
		window.parent.rightFrame.location.href=href;
	}
}

/**创建临时
 * @param callrefresh
 * @autor 张栋
 */
function addTempTab(url , title){

	//先获取当前的
	var tab = $('#maintabs').tabs('getSelected');

	//拿到当前tab的名称
	var oldtab = tab.panel('options').title;
	var subtitle= title || ("_"+oldtab);

	if ($('#maintabs').tabs('exists', subtitle)) {
		//存在则关掉原来的
		$('#maintabs').tabs('close', subtitle);
	}
	window.location.hash =url;
	$('#maintabs').tabs('add', {});
	var tab = $('#maintabs').tabs('getSelected');
	$('#maintabs').tabs('update', {
		tab: tab,
		options:{
			title : subtitle,
			content : createFrame(url),
			closable : true
		}
	});

	tabClose();

	return false;
}
/**半闭临时
 * @param callrefresh  是否调用刷新
 * @autor 张栋
 */
function closeTempTab(callrefresh){
	//先获取当前的
	var tab = $('#maintabs').tabs('getSelected');
	//拿到当前tab的名称
	var oldtab = tab.panel('options').title;
	//关闭当前
	$('#maintabs').tabs('close', oldtab);

	//获取父title
	var ptitle = oldtab.replace("_","");

	//切换到当前tab
	$('#maintabs').tabs('select', ptitle);
	//获取当前
	var currTab = $('#maintabs').tabs('getSelected');
	//根据id操作js
	var framename = $(currTab.panel('options').content).attr('name');

	if(callrefresh){
		parent.frames[framename].refresh();
	}
}

function addTab(subtitle, url, icon) {
	window.location.hash =url;
	if (!$('#maintabs').tabs('exists', subtitle)) {
		//判断是否进行href方式打开tab，默认为iframe方式
		if(url.indexOf('isHref') != -1){
			$('#maintabs').tabs('add', {
				title : subtitle,
				href : url,
				closable : true,
				icon : icon
			});
		}else if(url.indexOf("http")!=-1){
			window.open(url);
			return false;
		}else{
			$('#maintabs').tabs('add',{
					title : subtitle,
					content : createFrame(url),
					closable : true,
					icon : icon
			});
		}
	} else {
		$('#maintabs').tabs('select', subtitle);
		var currTab = $('#maintabs').tabs('getSelected');
		//var url = $(currTab.panel('options').content).attr('src');
		$('#maintabs').tabs('update', {
			tab : currTab,
			options : {
				content : createFrame(url)
			}
		});

	}



	$.cookie("b.azt.f",subtitle+","+url+","+icon);
	tabClose();
	return false;
}

function addmask() {
	$.messager.progress({
		text : loading,
		interval : 100
	});
}
function createFrame(url) {
	var m = Math.random();
	var s = '<iframe name="tabiframe'+m+'"  frameborder="no" border="0″ marginwidth="0″ marginheight="0″ scrolling="no" allowtransparency="yes"  height="100%"  src="'+url+'" style="width:100%;height:100%;"></iframe>';
	return s;
}

function tabClose() {

	/* 双击关闭TAB选项卡 */
	$(".tabs-inner").dblclick(function() {
		var subtitle = $(this).children(".tabs-closable").text();
		$('#maintabs').tabs('close', subtitle);
	})
	/* 为选项卡绑定右键 */
	$(".tabs-inner").bind('contextmenu', function(e) {
		e.preventDefault();

		$('#mm').menu('show', {
			left : e.pageX,
			top : e.pageY
		});

		var subtitle = $(this).children(".tabs-closable").text();

		$('#mm').data("currtab", subtitle);
		// $('#maintabs').tabs('select',subtitle);
		return false;
	});
}



// 绑定右键菜单事件
function tabCloseEven() {
	// 刷新
	$('#mm-tabupdate').click(function() {
		var currTab = $('#maintabs').tabs('getSelected');
		var url = $(currTab.panel('options').content).attr('src');
		$('#maintabs').tabs('update', {
			tab : currTab,
			options : {
				content : createFrame(url)
			}
		})
	})
	// 关闭当前
	$('#mm-tabclose').click(function() {
		var currtab_title = $('#mm').data("currtab");
		$('#maintabs').tabs('close', currtab_title);
	})
	// 全部关闭
	$('#mm-tabcloseall').click(function() {
		$('.tabs-inner span').each(function(i, n) {
			var t = $(n).text();
			if(t != '首页'){
				$('#maintabs').tabs('close', t);
			}
		});
	});
	// 关闭除当前之外的TAB
	$('#mm-tabcloseother').click(function() {
		$('#mm-tabcloseright').click();
		$('#mm-tabcloseleft').click();
	});
	// 关闭当前右侧的TAB
	$('#mm-tabcloseright').click(function() {
		var nextall = $('.tabs-selected').nextAll();
		if (nextall.length == 0) {
			return false;
		}
		nextall.each(function(i, n) {
			var t = $('a:eq(0) span', $(n)).text();
			$('#maintabs').tabs('close', t);
		});
		return false;
	});
	// 关闭当前左侧的TAB
	$('#mm-tabcloseleft').click(function() {
		var prevall = $('.tabs-selected').prevAll();
		if (prevall.length == 0) {
			return false;
		}
		prevall.each(function(i, n) {
			var t = $('a:eq(0) span', $(n)).text();
			if(t != '首页'){
				$('#maintabs').tabs('close', t);
			}
		});
		return false;
	});

	// 退出
	$("#mm-exit").click(function() {
		$('#mm').menu('hide');
	});
}
$.parser.onComplete = function() {
	top.frameFullScreen();
};
