﻿
$(function() {
	$("#func_list li").click(function() {
		$(".active").removeClass("active");
		$(this).addClass("active");
	});
})
var onlinedialog=null;
function online(){
	//当前在线
	if(onlinedialog!=null){
		onlinedialog.close();
	}
	onlinedialog =art.dialog.load("当前在线","/frame/online.do");
	return false;
}


function changeLeft(obj){
	var panel = $("#easyui-layout").layout("panel",'west');
	panel.panel('open').panel('refresh',$(obj).attr("href"));
	$("#body").click();
	
	return false;
}


// 修改密码
function sub() {
	var v = $("#passw").val();
	if (v != "") {
		$.ajax({
			type : "POST",
			dataType : "json",
			url : "/admin/edit_password.do",
			data : {
				password : v
			},
			success : function(data) {// 成功的时候调用
				if (data.success) {
					$("#passw").val(""); // 把输入框中的函数至为空。
				}
			}
		});
	}
}
function tooglepass() {
	$("#changepasw").fadeToggle("slow", "linear");
}
function notify() {
	if (window.webkitNotifications) {
		if (window.webkitNotifications.checkPermission() == 0) {
			getNoti();
			setInterval(getNoti, 1000 * 60);
		} else {
			$("#_notify").show();
		}
	} else {

	}
}

function getNoti() {
	$.ajax({
		type : "get",
		dataType : "json",
		url : "/notify/get.do",
		success : function(obj) {
			if (obj.success) {
				if (obj.obj != null) {
					var jso = obj.obj;
					for (var i = 0; i < jso.length; i++) {
						var j = jso[i];
						var img = "";
						var title = "";
						var content = "";
						var time = "";
						if (j.img)
							img = "http://" + location.host + j.img;
						alert(img);

						if (j.title)
							title = j.title;
						if (j.content)
							content = j.content;
						if (j.time)
							time = j.time;
						var newTime = new Date(time.time);
						var abc = formatDate(newTime, "yyyy-MM-dd");
						var notification = window.webkitNotifications
								.createNotification(img, title, content + "\n"
										+ abc);
						// notification.ondisplay = closeNotify();
						notification.show();
					}
				}
			}
		}
	});
}

function kaiqi() {
	window.webkitNotifications.requestPermission(notify);
}

function notifybut() {
	kaiqi();
}

// 格式化CST日期的字串
function formatCSTDate(strDate, format) {
	return formatDate(new Date(strDate), format);
}

// 格式化日期,
function formatDate(date, format) {
	var paddNum = function(num) {
		num += "";
		return num.replace(/^(\d)$/, "0$1");
	}
	// 指定格式字符
	var cfg = {
		yyyy : date.getFullYear() // 年 : 4位
		,
		yy : date.getFullYear().toString().substring(2)// 年 : 2位
		,
		M : date.getMonth() + 1 // 月 : 如果1位的时候不补0
		,
		MM : paddNum(date.getMonth() + 1) // 月 : 如果1位的时候补0
		,
		d : date.getDate() // 日 : 如果1位的时候不补0
		,
		dd : paddNum(date.getDate())// 日 : 如果1位的时候补0
		,
		hh : date.getHours() // 时
		,
		mm : date.getMinutes() // 分
		,
		ss : date.getSeconds()
	// 秒
	}
	format || (format = "yyyy-MM-dd hh:mm:ss");
	return format.replace(/([a-z])(\1)*/ig, function(m) {
		return cfg[m];
	});
}
notify();
