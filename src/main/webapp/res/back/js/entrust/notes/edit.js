$(function () {
    // 查询企业
    $("#companyName").select2({
        language : 'zh-CN',
        ajax: {
            url: "/company/query.do",
            dataType: 'json',
            delay: 250,
            data: function (params) {
                return {
                    q: params.term, // search term
                    page: params.page
                };
            },
            processResults: function (req, params) {
                params.page = params.page || 1;

                var data =req.obj;

                return {
                    results: data.list,
                    pagination: {
                        more: params.page < data.totalPage
                    }
                };
            },
            cache: true
        }
    });

    // validate
    $("#form").validator({
        timely:2,
        fields: {
            companyId: "required",
            amount: "required;range(0.01~)",
            contactName: "required;length(~10)",
            contactTel: "required;length(~20)"
        },
        valid: function (form) {
            $(form).ajaxSubmit(function (response) {
                response = $.parseJSON(response);
                if (response.success) {
                    alert("操作成功！");
                    window.location.href = "/entrust/notes/list.do";
                } else {
                    alert(response.message);
                }
            });
        }
    });
});


