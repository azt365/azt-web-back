/**
 * 新版省市区地址选择公共js
 * @author Snowler 2016-08-09
 * @param prefix 前缀 必填项
 * @param uiType 省市区页面元素类别(select或者ul或者别的什么) 必填项
 * @param init 是否需要初始化  1 需要  0 不需要
 * @param level_start 开始级别  0 country  ,  1 province , 2 city  , 3 district 必填项 暂时把0级别屏蔽 不考虑国家级别  只用来选取国内的省市区
 * @param level_end 结束级别  0 country  ,  1 province , 2 city  , 3 district 必填项 暂时把0级别屏蔽 不考虑国家级别  只用来选取国内的省市区
 * @param edit 是否是编辑状态  1 是   是的话 传入1即可   非编辑状态不用传  选填项 
 * 注意：若是编辑状态，在select元素中传入属性“value”即可 如<select id="prefix_country" value="1" ></select>  这样就会选中code为1的国家  
 */
function newAddress(prefix  , uiType ,  init , level_start , level_end , edit){
	
	//地址元素页面上面的ids（是约定好的，用户传入前缀即可）
	var ADDRESS_IDS = [ prefix + '_country', prefix + '_province', prefix + '_city', prefix + '_district' ];
	
	if(uiType == "select"){//当以select作为下拉元素时
		var each_level = level_start;
		if(edit && edit == 1){//如果是编辑状态，既有值带入时，遍历显示值
			var edit_level = level_start;
			do{
				if(edit_level == level_start && edit_level == 0){
					ajaxForAdd(edit_level,null , 'world' , ADDRESS_IDS[edit_level] , $("#" +  ADDRESS_IDS[edit_level]).attr("value"));
				}else if(edit_level == level_start && edit_level == 1){
					ajaxForAdd(edit_level,1 , null , ADDRESS_IDS[edit_level] , $("#" +  ADDRESS_IDS[edit_level]).attr("value"));
				}else{
					ajaxForAdd(edit_level,null , $("#" + ADDRESS_IDS[edit_level - 1]).attr("value") , ADDRESS_IDS[edit_level] , $("#" +  ADDRESS_IDS[edit_level]).attr("value"));
				}
				edit_level ++ ;
			}while(edit_level < (level_end + 1))
		}else if (init == 1){
			if(level_start == 0){
				ajaxForAdd(level_start,null , 'world' , ADDRESS_IDS[level_start]);
			}
			if(level_start == 1){
				ajaxForAdd(level_start,1 , null , ADDRESS_IDS[level_start]);
			}
		}
		do{
			var ADDRESS_ID = ADDRESS_IDS[each_level++];
			if($("#"+ADDRESS_ID)){
				$("#"+ADDRESS_ID).on('change' , function(){
					var $this = $(this);
					var indexOfAddress = ADDRESS_IDS.indexOf($this.attr("id"));
					var targetId;
					if(indexOfAddress < level_end){
						targetId = ADDRESS_IDS[indexOfAddress + 1];
					}
					if($this.val() && targetId){
						ajaxForAdd(indexOfAddress + 1,null , $this.val() , targetId);
					}
				});
			}
		}while(each_level < (level_end + 1));
	}
	function ajaxForAdd(level, id , code , targetId , value){
		$.ajax({
	 		   type : "post",
	 		   dataType : "json",
	 		   url : "querycityBycode.do",
	 		   data : {id:id , code:code},
	 		   success : function(data){
	 			   if(targetId){
	 				  $("#" + targetId).children().remove();
	 				  //$("#" + targetId).append("<option value=''>请选择</option>");
	 				  if(data.length>0 ){
	 					  for (var i = 0; i < data.length; i++) {
	 						  if(value && data[i].code == value){
	 							  $("#" + targetId).append("<option value='"+data[i].code+"' selected>"+data[i].name+"</option>");
	 						  }else{
	 							  $("#" + targetId).append("<option value='"+data[i].code+"'>"+data[i].name+"</option>");
	 						  }
	 					  }
	 				  }

	 				  if(level < level_end) {
						  //继续加载
						  level++;
						  ajaxForAdd(level, null, $('#' + targetId).val(), ADDRESS_IDS[level]);
					  }
	 			   }
	 		   }
	 	  });
	}
}