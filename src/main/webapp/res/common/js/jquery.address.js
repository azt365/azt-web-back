/**
 * 地区选择插件
 * 使用示例：$.address({province:'', city : '', district : '', success : function() {}})
 * 另外在select中使用data-val来标记默认值来进行初始化
 * @author shangwq
 * */

(function (factory) {
    if (typeof define === 'function' && define.amd) {
        define(['jquery'], factory);
    } else if (typeof exports === 'object') {
        factory(require('jquery'));
    } else {
        factory(jQuery);
    }
}(function ($) {
    'use strict';
    $.address = function (options) {
        var $province = $('#' + options.province);
        var $city = $('#' + options.city);
        var $district = $('#' + options.district);

        if($province && $city && $district) {
            var defaultProvince = $province.attr('data-val');
            var defaultCity = $city.attr('data-val');
            var defaultDistrict = $district.attr('data-val');

            //开始加载省
            $.ajax({
                type : "post",
                dataType : "json",
                url : "querycityBycode.do",
                data : { code: '000000' },
                success : function(res) {
                    renderOptions(res, $province, defaultProvince);

                    //加载市
                    $.ajax({
                        type : "post",
                        dataType : "json",
                        url : "querycityBycode.do",
                        data : { code: $province.val() },
                        success : function(res) {
                            renderOptions(res, $city, defaultCity);

                            //加载区
                            $.ajax({
                                type : "post",
                                dataType : "json",
                                url : "querycityBycode.do",
                                data : { code: $city.val() },
                                success : function(res) {
                                    renderOptions(res, $district, defaultDistrict);

                                    //执行success回调
                                    if(options.success) {
                                        options.success();
                                    }
                                }
                            });
                        }
                    });
                }
            });

            $province.on('change', function() {
                //加载市
                $.ajax({
                    type : "post",
                    dataType : "json",
                    url : "querycityBycode.do",
                    data : { code: $(this).val() },
                    success : function(res) {
                        renderOptions(res, $city);

                        //加载区
                        $.ajax({
                            type : "post",
                            dataType : "json",
                            url : "querycityBycode.do",
                            data : { code: $city.val() },
                            success : function(res) {
                                renderOptions(res, $district);
                            }
                        });
                    }
                });
            });

            $city.on('change', function() {
                //加载区
                $.ajax({
                    type : "post",
                    dataType : "json",
                    url : "querycityBycode.do",
                    data : { code: $(this).val() },
                    success : function(res) {
                        renderOptions(res, $district);
                    }
                });
            });
        }

        var renderOptions = function(data, $obj, value) {
            var options = "";
            $.each(data, function() {
                if(value && this.code == value) {
                    options += "<option value='" + this.code + "' selected>" + this.name + "</option>";
                } else {
                    options += "<option value='" + this.code + "'>" + this.name + "</option>";
                }
            });
            $obj.html(options);
        }
    };
}));
