package com.azt.provider.mapper;

import com.azt.api.pojo.AdminRole;
import tk.mybatis.mapper.common.Mapper;
import tk.mybatis.mapper.common.MySqlMapper;

public interface AdminRoleMapper extends Mapper<AdminRole>,MySqlMapper<AdminRole> {
}