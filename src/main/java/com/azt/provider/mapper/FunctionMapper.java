package com.azt.provider.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.azt.api.pojo.Function;

import tk.mybatis.mapper.common.Mapper;

public interface FunctionMapper extends Mapper<Function> {

	/**
	 * 组装成tree格式
	 * @return
	 * @author 张栋  2016年9月7日下午9:17:31
	 */
	List<Map<String, Object>> getFunctionsTreeData();

	/**
	 * 获取树形数据,主项及子项均查出
	 * @return
	 * @author 张栋  2016年9月7日下午9:23:43
	 */
	List<Function> getFunctionListForTree();

	/**
	 * @param adminid
	 * @param pid
	 * @return
	 * @author 张栋  2016年9月7日下午10:41:30
	 */
	List<Function> getFunctionByAdminIdAndPid(@Param("adminid") Integer adminid, @Param("pid")Integer pid);

	/**获取所有功能地址
	 * @return
	 * @author 张栋  2016年9月7日下午10:47:19
	 */
	List<String> getAllFunctionPath();

	/**
	 * 获取当前admin的所有地址
	 * @param adminid
	 * @return
	 * @author 张栋  2016年9月7日下午10:48:58
	 */
	List<String> getAllFunctionPathByAdminid(Integer adminid);
}