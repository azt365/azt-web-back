package com.azt.provider.mapper;

import java.util.List;

import com.azt.api.pojo.Admin;

import tk.mybatis.mapper.common.Mapper;
import tk.mybatis.mapper.common.MySqlMapper;

public interface AdminMapper extends Mapper<Admin>,MySqlMapper<Admin> {
	
	/**
	 * 根据名称查
	 * @param name
	 * @return
	 * @author 张栋  2016年9月7日下午3:30:59
	 */
	Admin getAdminByName(String name);

	/**
	 * 根据adminid 查询对应的功能地址
	 * @param adminId
	 * @return
	 * @author 张栋  2016年9月7日下午3:32:28
	 */
	List<String> getFunctionPathByAdminid(int adminId);

	/**获取所有管理员信息
	 * @return
	 * @author 张栋  2016年9月7日下午3:36:28
	 */
	List<Admin> getAllAdmin();

	/**根据id获取管理员
	 * @param id
	 * @return
	 * @author 张栋  2016年9月7日下午3:39:08
	 */
	Admin getAdminById(Integer id);
	
	
	
}