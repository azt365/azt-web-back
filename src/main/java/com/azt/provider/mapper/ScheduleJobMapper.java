package com.azt.provider.mapper;

import com.azt.api.pojo.quartz.ScheduleJob;
import tk.mybatis.mapper.common.Mapper;

public interface ScheduleJobMapper extends Mapper<ScheduleJob> {
}