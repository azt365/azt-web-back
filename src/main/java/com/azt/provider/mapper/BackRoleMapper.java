package com.azt.provider.mapper;

import com.azt.api.pojo.BackRole;

import tk.mybatis.mapper.common.Mapper;

public interface BackRoleMapper extends Mapper<BackRole> {
}