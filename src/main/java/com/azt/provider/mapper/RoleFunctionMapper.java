package com.azt.provider.mapper;

import com.azt.api.pojo.RoleFunction;
import tk.mybatis.mapper.common.Mapper;

public interface RoleFunctionMapper extends Mapper<RoleFunction> {
}