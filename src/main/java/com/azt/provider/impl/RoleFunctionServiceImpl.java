package com.azt.provider.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.azt.api.pojo.RoleFunction;
import com.azt.api.service.RoleFunctionService;
import com.azt.provider.mapper.RoleFunctionMapper;

import tk.mybatis.mapper.entity.Example;

@Service
@Transactional
public class RoleFunctionServiceImpl implements RoleFunctionService {

	
	@Autowired
	private RoleFunctionMapper roleFunctionMapper;

	@Override
	public List<RoleFunction> getRFListByRoleId(Integer roleId) {
		Example example = new Example(RoleFunction.class);
		example.createCriteria().andEqualTo("roleId",roleId);
		return roleFunctionMapper.selectByExample(example);
//		return commonService.getListByParams(RoleFunction.class, "roleId", roleId);
	}

	@Override
	public void save(RoleFunction rf) {
		roleFunctionMapper.insert(rf);
//		commonService.save(rf);
	}

	@Override
	public void delAllRoleFunction(List<RoleFunction> rflist) {
		for (int i = 0; i < rflist.size(); i++) {
			RoleFunction roleFunction = rflist.get(i);
			roleFunctionMapper.deleteByPrimaryKey(roleFunction);
		}
//		commonService.deleteAllEntitie(rflist);
	}
}
