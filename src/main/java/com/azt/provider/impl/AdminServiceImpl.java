package com.azt.provider.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.azt.api.pojo.Admin;
import com.azt.api.pojo.AdminRole;
import com.azt.api.service.AdminService;
import com.azt.provider.mapper.AdminMapper;
import com.azt.provider.mapper.AdminRoleMapper;
import com.azt.utils.PasswordUtil;

@Service
@Transactional
public class AdminServiceImpl  implements AdminService {
	
	@Autowired
	private AdminMapper  adminMapper;
	
	@Autowired
	private AdminRoleMapper   adminRoleMapper;
	
	

	@Override
	public Admin validateSubmit(String name, String password) {
		Admin admin = adminMapper.getAdminByName(name);
		if (admin != null) {
			String encodePassword = PasswordUtil.encodePassword(password, admin.getSalt());
			if (encodePassword.equals(admin.getPassword())) {// 且密码正确
				return admin;
			}
		}
		return null;
	}

	public List<String> functionPath(int adminId) {
		List<String> paths = adminMapper.getFunctionPathByAdminid(adminId);
		return paths;
	}

	@Override
	public List<Admin> getAllAdmin() {
		List<Admin>  admins =  adminMapper.getAllAdmin(); 
		return admins;
	}

	@Override
	public Admin getAdminById(Integer id) {
		return adminMapper.getAdminById(id);
	}

	/**
	 * 重载方法
	 * 
	 * @param ad
	 * @param oldadmin
	 */
	@Override
	public void editAdmin(Admin ad, Admin oldadmin) {
		List<AdminRole> adminRoles = ad.getAdminRoles();
//		List<AdminRole> adminRoles2 = oldadmin.getAdminRoles();
		//删除老的
		AdminRole  adminRole = new AdminRole();
		adminRole.setAdmin_id(ad.getId());
		adminRoleMapper.delete(adminRole);
		
		for (int i = 0; i < adminRoles.size(); i++) {
			AdminRole  r = adminRoles.get(i);
			r.setAdmin_id(ad.getId());
			r.setRole_id(r.getBackRole().getId());
		}


		for (int i = 0; i < adminRoles.size(); i++) {
			AdminRole adminRole1 = adminRoles.get(i);
			adminRoleMapper.insert(adminRole1);
		}

		// 增加新的
//		adminRoleMapper.insertList(adminRoles);
		//更新admin
		adminMapper.updateByPrimaryKeySelective(oldadmin);
		
	}

	@Override
	public void saveAdmin(Admin ad) {
		adminMapper.insert(ad);
		//将adminid 设置进去
		List<AdminRole> adminRoles = ad.getAdminRoles();
		if(adminRoles!=null){
			for (int i = 0; i < adminRoles.size(); i++) {
				AdminRole adminRole = adminRoles.get(i);
				adminRole.setAdmin_id(ad.getId());
			}
			//保存中间表
			adminRoleMapper.insertList(adminRoles);
		}
		
	}

	@Override
	public void delAdmin(Integer id) {
		//删除主表
		adminMapper.deleteByPrimaryKey(new Admin(id));
		
		//根据id
		AdminRole adminRole = new AdminRole();
		adminRole.setAdmin_id(id);
		//删除关联表
		adminRoleMapper.delete(adminRole);
		
	}

	@Override
	public void saveOrUpdate(Admin ad) {
		if(ad.getId()==null){
			adminMapper.insert(ad);
		}else{
			adminMapper.updateByPrimaryKey(ad);
		}
	}

}
