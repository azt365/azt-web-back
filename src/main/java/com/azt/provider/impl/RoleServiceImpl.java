package com.azt.provider.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.azt.api.pojo.AdminRole;
import com.azt.api.pojo.BackRole;
import com.azt.api.pojo.RoleFunction;
import com.azt.api.service.RoleService;
import com.azt.provider.mapper.AdminRoleMapper;
import com.azt.provider.mapper.BackRoleMapper;
import com.azt.provider.mapper.RoleFunctionMapper;

import tk.mybatis.mapper.entity.Example;

/**
 * @author 张栋 2016年4月5日下午4:40:54
 */
@Service
@Transactional
public class RoleServiceImpl implements RoleService {
	
	@Autowired
	private BackRoleMapper   backRoleMapper;
	@Autowired
	private RoleFunctionMapper roleFunctionMapper;
	
	@Autowired
	private AdminRoleMapper  adminRoleMapper;
	
	@Override
	public List<BackRole> getAllRole() {
		return backRoleMapper.selectAll();
	}

	/**
	 * 重载方法
	 * 
	 * @param backRole
	 */
	@Override
	public void deleteAllcorrelation(BackRole backRole) {
		
		Example  example  = new Example(RoleFunction.class);
		example.createCriteria().andEqualTo("roleId",backRole.getId());
		roleFunctionMapper.deleteByExample(example);
		// 删除与function的中间表
//		List rolefunctions = commonService.getListByParams(RoleFunction.class, "roleId", backRole.getId());
//		commonService.deleteAllEntitie(rolefunctions);

		// 删除与用户之前的中间表
		
		Example  example2  = new Example(AdminRole.class);
		example2.createCriteria().andEqualTo("role_id",backRole.getId());
		adminRoleMapper.deleteByExample(example2);
//		List ars = commonService.getListByParams(AdminRole.class, "role.id", backRole.getId());
//		commonService.deleteAllEntitie(ars);
		
//		Session session = commonService.getDao().getSession();
//		session.flush();
//		session.clear();
		// 删除角色
		backRoleMapper.deleteByPrimaryKey(backRole);
//		commonService.delete(backRole);
	}

	@Override
	public void save(BackRole backRole) {
		backRoleMapper.insert(backRole);
//		commonService.save(backRole);
	}

	@Override
	public BackRole getRoleByid(Integer id) {
		BackRole backRole = new BackRole();
		backRole.setId(id);
		return backRoleMapper.selectOne(backRole);
//		return commonService.getEntity(BackRole.class, id);
	}

	@Override
	public void update(BackRole backRole) {
		backRoleMapper.updateByPrimaryKeySelective(backRole);		
	}
}
