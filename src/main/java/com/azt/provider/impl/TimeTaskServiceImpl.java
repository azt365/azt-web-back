package com.azt.provider.impl;

import com.azt.api.pojo.quartz.ScheduleJob;
import com.azt.api.service.quartz.TimeTaskService;
import com.azt.provider.mapper.ScheduleJobMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class TimeTaskServiceImpl  implements TimeTaskService {

	@Autowired
	private ScheduleJobMapper  mapper;


	@Override
	public List<ScheduleJob> getAllStarted() {
		ScheduleJob sj = new ScheduleJob();
		sj.setJob_status(1);
		return mapper.select(sj);
	}
	@Override
	public ScheduleJob getScheduleJobById(Integer scheID) {
		return mapper.selectByPrimaryKey(new ScheduleJob(scheID));
	}
	@Override
	public void updateEntitie(ScheduleJob job) {
		mapper.updateByPrimaryKey(job);
	}
	@Override
	public void delScheduleJob(ScheduleJob sJob) {
		if(sJob != null) {
			mapper.delete(sJob);
		}
	}
	@Override
	public List<ScheduleJob> getAllScheduleJob() {
		return mapper.selectAll();
	}
	@Override
	public void save(ScheduleJob task) {
		mapper.insert(task);
	}
	
}
