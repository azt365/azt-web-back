package com.azt.provider.impl;

import com.azt.api.pojo.Function;
import com.azt.api.service.FuncService;
import com.azt.model.easy.EasyTree;
import com.azt.model.easy.EasyTreeModel;
import com.azt.provider.mapper.FunctionMapper;
import com.azt.service.EasyService;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.apache.commons.lang3.ArrayUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
@Transactional
public class FuncServiceImpl implements FuncService {

	@Autowired
	private FunctionMapper functionMapper;
	
	
	@Autowired
	private EasyService es;

	@Override
	public JSONArray getZTreeJsonArray() {
		List<Map<String, Object>> funcTree =  functionMapper.getFunctionsTreeData();
		JSONArray fromObject = JSONArray.fromObject(funcTree);
		return fromObject;
	}
	@Override
	public List<EasyTree> getFuncTree(){
		List<Function> functionList = functionMapper.getFunctionListForTree();
//		String hql = "From Function f where parentId=0 order by sortnum";
//		List<Function> functionList = commonService.getListByHql(hql);
		EasyTreeModel model = new EasyTreeModel("id", "name", "children","uri");
		model.setSortField("sortnum");
		
		return es.easyTree(functionList, model, null, true,true);
	}
	
	
	

	@Override
	public JSONArray getETreeJsonArray() {
//		String hql = "From Function f where parentId=0 order by sortnum";
//		List<Function> functionList = commonService.getListByHql(hql);
		List<Function> functionList = functionMapper.getFunctionListForTree();
		JSONArray funcs2jsonArray = funcs2jsonArray(functionList);
		return funcs2jsonArray;
	}

	@Override
	public JSONArray getFunETreeJsonArray(String funcs) {
		String[] funcids = funcs == null ? null : funcs.split(",");
		
		List<Function> functionList = functionMapper.getFunctionListForTree();
//		String hql = "From Function f where parentId=0 order by sortnum";
//		List<Function> functionList = commonService.getListByHql(hql);

		JSONArray funcs3jsonArray = funcs3jsonArray(functionList, funcids);
		return funcs3jsonArray;
	}

	private JSONArray funcs2jsonArray(List<Function> functionList) {
		JSONArray arr = new JSONArray();
		for (Function f : functionList) {
			JSONObject obj = new JSONObject();
			obj.put("id", f.getId());
			obj.put("name", f.getName());
			obj.put("uri", f.getUri());
			obj.put("sortnum", f.getSortnum());
			List<Function> children = f.getChildren();
			if (f.getParentId() != 0 && children.size() != 0) {//
				obj.put("state", "closed");
			}
			if (children != null && children.size() != 0) {
				JSONArray atts = funcs2jsonArray(f.getChildren());
				obj.put("children", atts);
			}
			arr.add(obj);
		}
		return arr;
	}

	/**
	 * function tree json 封装器
	 * 
	 * @Description:
	 * @author zouheyuan 下午04:47:39
	 * @param functionList
	 * @return
	 */
	private JSONArray funcs3jsonArray(List<Function> functionList, String[] funcids) {
		JSONArray arr = new JSONArray();
		for (Function f : functionList) {
			JSONObject jsonForAttr = new JSONObject();
			jsonForAttr.put("parentId", f.getParentId());
			JSONObject obj = new JSONObject();
			obj.put("id", f.getId());
			obj.put("text", f.getName());
			if (f.getParentId() == 0) {
				obj.put("state", "closed");
			}
			if (ArrayUtils.contains(funcids, f.getId().toString())) {
				obj.put("checked", true);
			}
			obj.put("attributes", jsonForAttr);

			List<Function> children = f.getChildren();
			if (children != null && children.size() != 0) {
				JSONArray atts = funcs3jsonArray(f.getChildren(), funcids);
				obj.put("children", atts);
			}
			arr.add(obj);
		}
		return arr;
	}

	/**
	 * 重载方法
	 * 
	 * @param func
	 */
	@Override
	public void saveFunction(Function func) {
		functionMapper.insert(func);
//		commonService.save(func);
	}

	@Override
	public List<Function> getAllFunction() {
		return functionMapper.selectAll();
//		return commonService.getAllEntity(Function.class);
	}

	@Override
	public List<Function> getFunctionsByPid(Integer id) {
		Example example = new Example(Function.class);
		example.createCriteria().andEqualTo("parentId",id);
		example.orderBy("sortnum").asc();
		return functionMapper.selectByExample(example);
//		String hql = "From Function f where parentId=:pid order by sortnum";
//		return commonService.getListByHql(hql, id);
	}

	@Override
	public Function getFunctionById(Integer id) {
		return functionMapper.selectOne(new Function(id));
//		return commonService.getEntity(Function.class, id);
	}

	@SuppressWarnings("unchecked")
	@Override
	public void del(Function f) {
		if(f != null) {
			functionMapper.delete(f);
		}
	}

	/**
	 * 重载方法
	 * 
	 * @return
	 */
	@Override
	public List<Function> getLimitedFunction(Integer adminid, Integer pid) {
		List<Function>   list  =   functionMapper.getFunctionByAdminIdAndPid(adminid,pid);
		return list;
//		String sql = "SELECT o4.* FROM back_admin o1 LEFT JOIN back_admin_role o2 ON o1.id =o2.admin_id LEFT JOIN back_role_function o3 ON o2.role_id = o3.role_id LEFT JOIN back_function o4 ON o3.function_id=o4.id WHERE o1.id=? AND o4.parent_id=?   group by o4.id order by o4.sort_num";
//		return commonService.getListBySql(Function.class, sql, adminid, pid);
	}

	/**
	 * 重载方法
	 * 
	 * @param adminid
	 * @return
	 */
	@Override
	public List<String> functionpath(Integer adminid) {
		//
		if (adminid == null) {
			List<String> paths=functionMapper.getAllFunctionPath();
			return paths;
//			String sql = "SELECT o4.uri FROM  back_function o4  where o4.uri is not null and o4.uri!=''  group by o4.id";
//			return commonService.getDao().getSession().createSQLQuery(sql).list();
		} else {
			
			List<String> paths=functionMapper.getAllFunctionPathByAdminid(adminid);
//			String sql = "SELECT o4.uri FROM back_admin o1 LEFT JOIN back_admin_role o2 ON o1.id =o2.admin_id LEFT JOIN back_role_function o3 ON o2.role_id = o3.role_id LEFT JOIN back_function o4 ON o3.function_id=o4.id WHERE o1.id=:adminid  group by o4.id";
//			return commonService.getDao().getSession().createSQLQuery(sql).setParameter("adminid", adminid).list();
			return paths;
		}
	}

	@Override
	public void saveOrUpdate(Function oldfunc) {
		if(oldfunc.getId()==null){
			functionMapper.insert(oldfunc);
		}else{
			functionMapper.updateByPrimaryKey(oldfunc);
		}
//		commonService.saveOrUpdate(oldfunc);
	}

	@Override
	public void delFunction(Integer id) {
	
		//删除自己
		functionMapper.delete(new Function(id));
		
		//删除子集
		Example example = new Example(Function.class);
		example.createCriteria().andEqualTo("parentId", id);
		functionMapper.deleteByExample(example);
		
	}

}
