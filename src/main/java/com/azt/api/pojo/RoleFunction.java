package com.azt.api.pojo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.azt.pojo.BaseEntity;

@Entity
@Table(name="back_role_function")
public class RoleFunction extends BaseEntity{
	
	
	
	@Column(name="role_id")
	Integer roleId;
	
	@Column(name="function_id")
	Integer functionId;
	
	public Integer getRoleId() {
		return roleId;
	}
	public void setRoleId(Integer roleId) {
		this.roleId = roleId;
	}
	public Integer getFunctionId() {
		return functionId;
	}
	public void setFunctionId(Integer functionId) {
		this.functionId = functionId;
	}
	
	
}
