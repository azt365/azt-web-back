package com.azt.api.pojo;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Table(name = "back_admin")
public class Admin {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	private String name;
	private String salt;
	private String password;

	private Date createtime;
	private Date updatetime;

	// 角色名称,多个以逗号分开
	@Transient
	private String rolenames;

	// 角色名称,多个以逗号分开
	@Transient
	private String roleids;

	@Transient
	private List<AdminRole> adminRoles = new ArrayList<AdminRole>();

	public void setRolenames(String rolenames) {
		this.rolenames = rolenames;
	}

	public Admin() {

	}

	public Admin(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSalt() {
		return salt;
	}

	public void setSalt(String salt) {
		this.salt = salt;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * 获取所有角色名
	 * 
	 * @author 张栋
	 * @return
	 * @see [类、类#方法、类#成员]
	 */
	@Transient
	public String getRolenames() {
		return rolenames;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Date getCreatetime() {
		return createtime;
	}

	public void setCreatetime(Date createtime) {
		this.createtime = createtime;
	}

	public Date getUpdatetime() {
		return updatetime;
	}

	public void setUpdatetime(Date updatetime) {
		this.updatetime = updatetime;
	}

	public List<AdminRole> getAdminRoles() {
		return adminRoles;
	}

	public void setAdminRoles(List<AdminRole> adminRoles) {
		this.adminRoles = adminRoles;
	}

	public String getRoleids() {
		return roleids;
	}

	public void setRoleids(String roleids) {
		this.roleids = roleids;
	}

}
