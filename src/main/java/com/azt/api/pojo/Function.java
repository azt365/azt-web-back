package com.azt.api.pojo;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.azt.pojo.BaseEntity;

@Table(name = "back_function")
public class Function extends BaseEntity {
	@Column(name = "name")
	private String name;
	
	@Column(name = "uri")
	private String uri;
	
	@Column(name = "parent_id")
	private Integer parentId;
	
	@Column(name = "sort_num")
	private Integer sortnum;
	
	@Column(name = "icon")
	private String icon;

	@Transient
	private List<Function> children;

	public Function() {
	}
	public Function(Integer id) {
		setId(id);
	}

	
	public List<Function> getChildren() {
		return children;
	}

	public void setChildren(List<Function> children) {
		this.children = children;
	}

	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}


	public String getUri() {
		return uri;
	}

	public void setUri(String uri) {
		this.uri = uri;
	}

	
	public Integer getParentId() {
		return parentId;
	}

	public void setParentId(Integer parentId) {
		this.parentId = parentId;
	}

	
	public Integer getSortnum() {
		return sortnum;
	}

	public void setSortnum(Integer sortnum) {
		this.sortnum = sortnum;
	}

	
	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

}
