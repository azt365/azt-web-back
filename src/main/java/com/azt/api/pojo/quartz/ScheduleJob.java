package com.azt.api.pojo.quartz;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;

@Table(name = "back_schedule_job")
public class ScheduleJob implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    /**
     * 表达式
     */
    private String cron_expression;

    /**
     * 0未启用，1启用
     */
    private Integer job_status;

    /**
     * 任务描述
     */
    private String job_desc;

    private String bean_class;

    /**
     * 任务名，class名称
     */
    private String job_name;

    private Date createtime;

    private Date updatetime;

    private static final long serialVersionUID = 1L;

    public ScheduleJob() {
    }
    public ScheduleJob(Integer scheID) {
        this.id =scheID;
    }

    /**
     * @return id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取表达式
     *
     * @return cron_expression - 表达式
     */
    public String getCron_expression() {
        return cron_expression;
    }

    /**
     * 设置表达式
     *
     * @param cron_expression 表达式
     */
    public void setCron_expression(String cron_expression) {
        this.cron_expression = cron_expression;
    }

    /**
     * 获取0未启用，1启用
     *
     * @return job_status - 0未启用，1启用
     */
    public Integer getJob_status() {
        return job_status;
    }

    /**
     * 设置0未启用，1启用
     *
     * @param job_status 0未启用，1启用
     */
    public void setJob_status(Integer job_status) {
        this.job_status = job_status;
    }

    /**
     * 获取任务描述
     *
     * @return job_desc - 任务描述
     */
    public String getJob_desc() {
        return job_desc;
    }

    /**
     * 设置任务描述
     *
     * @param job_desc 任务描述
     */
    public void setJob_desc(String job_desc) {
        this.job_desc = job_desc;
    }

    /**
     * @return bean_class
     */
    public String getBean_class() {
        return bean_class;
    }

    /**
     * @param bean_class
     */
    public void setBean_class(String bean_class) {
        this.bean_class = bean_class;
    }

    /**
     * 获取任务名，class名称
     *
     * @return job_name - 任务名，class名称
     */
    public String getJob_name() {
        return job_name;
    }

    /**
     * 设置任务名，class名称
     *
     * @param job_name 任务名，class名称
     */
    public void setJob_name(String job_name) {
        this.job_name = job_name;
    }

    /**
     * @return createtime
     */
    public Date getCreatetime() {
        return createtime;
    }

    /**
     * @param createtime
     */
    public void setCreatetime(Date createtime) {
        this.createtime = createtime;
    }

    /**
     * @return updatetime
     */
    public Date getUpdatetime() {
        return updatetime;
    }

    /**
     * @param updatetime
     */
    public void setUpdatetime(Date updatetime) {
        this.updatetime = updatetime;
    }
}