package com.azt.api.pojo;

import javax.persistence.Table;
import javax.persistence.Transient;

import com.azt.pojo.BaseEntity;

@Table(name = "back_admin_role")
public class AdminRole extends BaseEntity {
	
	@Transient
	private Admin admin;
	
	
	@Transient
	private BackRole backRole;

	private Integer admin_id;

	private Integer role_id;

	
	public Admin getAdmin() {
		return admin;
	}

	public void setAdmin(Admin admin) {
		this.admin = admin;
	}

	public BackRole getRole() {
		return backRole;
	}

	public void setRole(BackRole backRole) {
		this.backRole = backRole;
	}

	public BackRole getBackRole() {
		return backRole;
	}

	public void setBackRole(BackRole backRole) {
		this.backRole = backRole;
	}

	public Integer getAdmin_id() {
		return admin_id;
	}

	public void setAdmin_id(Integer admin_id) {
		this.admin_id = admin_id;
	}

	public Integer getRole_id() {
		if(backRole!=null){
			return backRole.getId();
		}
		return role_id;
	}

	public void setRole_id(Integer role_id) {
		this.role_id = role_id;
	}
	
	
	

}
