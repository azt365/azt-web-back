package com.azt.api.service;

import java.util.List;

import com.azt.api.pojo.Admin;

/**
 * @author Administrator
 *
 */
public interface AdminService {

	/**
	 * 验证登陆信息
	 * @Title: validateSubmit   
	 * @Description: TODO  
	 * @param @param name
	 * @param @param password
	 * @param @return  
	 * @return String  
	 * @throws
	 */
	Admin validateSubmit(String name, String password);

	/**
	 * 
	 */
	List<Admin> getAllAdmin();

	/**
	 * 根据id查对象
	 * @param id
	 * @return
	 */
	Admin getAdminById(Integer id);

	/** 
	 * @author  张栋
	 * @param ad
	 * @param oldadmin
	 * @see [类、类#方法、类#成员]
	 */
	void editAdmin(Admin ad, Admin oldadmin);


	
	/**
	 * 获取权限
	 * @param adminId
	 * @return
	 */
	List<String> functionPath(int adminId);

	void saveAdmin(Admin ad);

	void delAdmin(Integer id);

	void saveOrUpdate(Admin ad);

}
