package com.azt.api.service;

import java.util.List;

import com.azt.api.pojo.BackRole;

/**
 * @author vipdon
 *
 */
public interface RoleService {
	List<BackRole> getAllRole();

	/** 
	 * 删除角色所有相关的表
	 * @author  张栋
	 * @param backRole
	 * @see [类、类#方法、类#成员]
	 */
	void deleteAllcorrelation(BackRole backRole);

	void save(BackRole backRole);

	BackRole getRoleByid(Integer id);

	void update(BackRole backRole);
}
