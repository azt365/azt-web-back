package com.azt.api.service.quartz;

import com.azt.api.pojo.quartz.ScheduleJob;

import java.util.List;


public interface TimeTaskService {

	/**
	 * 获取所有已经开始的
	 * @return
	 */
	List<ScheduleJob> getAllStarted();

	ScheduleJob getScheduleJobById(Integer scheID);

	void updateEntitie(ScheduleJob job);

	void delScheduleJob(ScheduleJob sJob);

	List<ScheduleJob> getAllScheduleJob();

	void save(ScheduleJob task);

}
