package com.azt.api.service;

import java.util.List;

import com.azt.api.pojo.RoleFunction;

public interface RoleFunctionService{
	List<RoleFunction> getRFListByRoleId(Integer roleId);

	void save(RoleFunction rf);

	void delAllRoleFunction(List<RoleFunction> rflist);
}
