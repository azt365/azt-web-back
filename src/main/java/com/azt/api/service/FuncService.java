package com.azt.api.service;

import java.util.List;

import com.azt.api.pojo.Function;
import com.azt.model.easy.EasyTree;

import net.sf.json.JSONArray;


public interface FuncService {
	
	/**   
	 * ztree的json树
	 * @Title: getZTreeJsonArray   
	 * @Description: TODO  
	 * @param @return  
	 * @return JSONArray  
	 * @throws   
	 */
	@Deprecated
	JSONArray getZTreeJsonArray();
	/**   
	 * 
	 * easyui的json树
	 * @Title: getETreeJsonArray   
	 * @Description: TODO  
	 * @param @return  
	 * @return JSONArray  
	 * @throws   
	 */
	JSONArray getETreeJsonArray();
	
	
	/**
	 * easyui的方法的json树
	 * @Description:
	 * @author zouheyuan  下午04:46:18
	 * @return
	 */
	JSONArray getFunETreeJsonArray(String funcs);
	/**   
	 * 添加菜单项
	 * @Title: saveFunction   
	 * @Description: TODO  
	 * @param @param func  
	 * @return void  
	 * @throws   
	 */
	void saveFunction(Function func);
	/**  获取所有方法
	 * @Title: getAllFunction   
	 * @Description: TODO  
	 * @param @return  
	 * @return List<Function>  
	 * @throws   
	 */
	List<Function> getAllFunction();
	
	/**   
	 * 根据父节点查下面的所有子节点
	 * @Title: getFunctionsByPid   
	 * @Description: TODO  
	 * @param @param id
	 * @param @return  
	 * @return List<Function>  
	 * @throws   
	 */
	List<Function> getFunctionsByPid(Integer id);
	
	
	/**   
	 * @Title: getFunctionById   
	 * @Description: TODO  
	 * @param @param id
	 * @param @return  
	 * @return Function  
	 * @throws   
	 */
	Function  getFunctionById(Integer id);
	
	void del(Function f);
	/** 
	 * 获取有权限限制的
	 * @author  张栋
	 * @param adminid 
	 * @param pid 
	 * @return
	 * @see [类、类#方法、类#成员]
	 */
	List<Function> getLimitedFunction(Integer adminid, Integer pid);
	
	/** 
	 * 获取指定用户的所有权限路径
	 * @author  张栋
	 * @param adminid
	 * @return
	 * @see [类、类#方法、类#成员]
	 */
	List<String> functionpath(Integer adminid);
	void saveOrUpdate(Function oldfunc);
	void delFunction(Integer id);
	List<EasyTree> getFuncTree();
	
}
