package com.azt.back.model;

import com.azt.api.pojo.EnqEnquiryItem;
import com.azt.api.pojo.EnqQuoteItem;

import java.util.List;

/**
 * 报价item,用于报价分析后提交
 * Created by 张栋 on 2016/11/1 9:47
 */
public class EnqQuoteItemForm {

    //第一行数据
    private List<EnqQuoteItem> item;

    private String[] typenames;
    private String[] oldtitles;

    private Integer quoteid;

    public List<EnqQuoteItem> getItem() {
        return item;
    }

    public void setItem(List<EnqQuoteItem> item) {
        this.item = item;
    }

    public String[] getTypenames() {
        return typenames;
    }

    public void setTypenames(String[] typenames) {
        this.typenames = typenames;
    }

    public String[] getOldtitles() {
        return oldtitles;
    }

    public void setOldtitles(String[] oldtitles) {
        this.oldtitles = oldtitles;
    }

    public Integer getQuoteid() {
        return quoteid;
    }

    public void setQuoteid(Integer quoteid) {
        this.quoteid = quoteid;
    }
}
