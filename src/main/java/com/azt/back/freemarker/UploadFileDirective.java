package com.azt.back.freemarker;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Component;

import freemarker.core.Environment;
import freemarker.template.TemplateDirectiveBody;
import freemarker.template.TemplateDirectiveModel;
import freemarker.template.TemplateException;
import freemarker.template.TemplateModel;

/**
 * 上传文件
 * 
 * @author 张栋 2016年5月9日上午11:49:44
 */
@Component("upload_file")
public class UploadFileDirective implements TemplateDirectiveModel {

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public void execute(Environment env, Map params, TemplateModel[] loopVars, TemplateDirectiveBody body) throws TemplateException, IOException {

		Map<String, TemplateModel> paramWrap = new HashMap<String, TemplateModel>(params);
		// 如果自定义的参数key值，与环境变里的key相同，则先将env里同名的移动到
		// 下面的map。用来临时存放，渲染页面后，再将其还原到env里,并把上面map里的参数复制到env里
		Map<String, TemplateModel> origMap = DirectiveUtils.addParamsToVariable(env, paramWrap);

		env.include("./back/template/uploadfile.html", "UTF-8", true);

		// 还原env里原有的参数
		DirectiveUtils.removeParamsFromVariable(env, paramWrap, origMap);

	}

}
