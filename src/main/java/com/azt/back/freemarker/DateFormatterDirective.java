package com.azt.back.freemarker;

import freemarker.template.SimpleDate;
import freemarker.template.TemplateMethodModelEx;
import freemarker.template.TemplateModelException;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;

/**
 * 模板方法
 * Created by LiQZ on 2017/3/17.
 */
@Component("date_fmt")
public class DateFormatterDirective implements TemplateMethodModelEx {

    @Override
    public Object exec(List arguments) throws TemplateModelException {
        try {
            if (arguments.size() == 2) {
                SimpleDate sdate = (SimpleDate) arguments.get(0);
                // return UploadFileUtils.addPrefix(arguments.iterator().next().toString());
                return DateFormatUtils.format(sdate.getAsDate(), String.valueOf(arguments.get(1)));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return StringUtils.EMPTY;
    }


}
