package com.azt.back.freemarker;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.azt.api.pojo.Admin;
import com.azt.api.pojo.Function;
import com.azt.api.service.FuncService;
import com.azt.back.util.ContextUtils;

import freemarker.core.Environment;
import freemarker.template.TemplateDirectiveBody;
import freemarker.template.TemplateDirectiveModel;
import freemarker.template.TemplateException;
import freemarker.template.TemplateModel;

/**
 * 后台功能列表 top页面的标签
 */
@Component("frame_list")
public class FrameListDirective implements TemplateDirectiveModel {

	@Autowired
	private FuncService manager;

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void execute(Environment env, Map params, TemplateModel[] loopVars, TemplateDirectiveBody body) throws TemplateException, IOException {

		Integer pid = DirectiveUtils.getInt("pid", params);
		List<Function> allFunction;

		/**
		 * 如果有父id,则根据父id查子功能
		 */
		if (pid != null) {
			Admin admin = ContextUtils.getAdmin();
			allFunction = manager.getLimitedFunction(admin.getId(), pid);
		} else {
			// 过滤权限的查询
			allFunction = manager.getAllFunction();
		}

		DirectiveUtils.writer("functions", allFunction, env, params, body);

	}
}
