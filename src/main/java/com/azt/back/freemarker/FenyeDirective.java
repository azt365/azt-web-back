package com.azt.back.freemarker;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Component;

import freemarker.core.Environment;
import freemarker.template.TemplateDirectiveBody;
import freemarker.template.TemplateDirectiveModel;
import freemarker.template.TemplateException;
import freemarker.template.TemplateModel;

/**
 * 后台分页
 * @author 张栋  2016年4月26日下午12:36:51
 * p:   第一个参数是页面中的pagination变量，其中有页面的分页，查询条件的相关信息；
 * form: 第二个参数是自定义的页面查询form表单的id，form表单封装在fenye.html中，此form中存放页面的页码信息和相关查询信息，在页码跳转和页面查询时，提交此表单即可。
 */
@Component("fenye")
public class FenyeDirective implements TemplateDirectiveModel {

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public void execute(Environment env, Map params, TemplateModel[] loopVars,
			TemplateDirectiveBody body) throws TemplateException, IOException {
		
		Map<String, TemplateModel> paramWrap = new HashMap<String, TemplateModel>(params);
		//如果自定义的参数key值，与环境变里的key相同，则先将env里同名的移动到 下面的map。用来临时存放，渲染页面后，再将其还原到env里,并把上面map里的参数复制到env里
		Map<String, TemplateModel> origMap = DirectiveUtils.addParamsToVariable(env, paramWrap);
		
		env.include("./back/template/fenye.html", "UTF-8", true);
		
		//还原env里原有的参数
		DirectiveUtils.removeParamsFromVariable(env, paramWrap, origMap);
		
	}

}
