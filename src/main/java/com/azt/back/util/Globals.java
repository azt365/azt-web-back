package com.azt.back.util;

/**
 * 
 * @author 张栋  2016年4月26日下午5:49:30
 */
public final class Globals {
	//后台管理员session
	public static String ADMIN_SESSION = "admin_session";
	//管理员默认密码
	public static String PASSWORD ="123qwe";
	/**
	 * 用户所拥有的权限
	 */
	public static String PERMISSION_MODEL ="_permission_key";
	
	/**
	 * 列表展示条数
	 */
	public static int PAGE_SIZE =10;
	
	/**
	 * 页中页显示10行
	 */
	public static int PAGE_SIZE_TEN = 10;

	/**
	 * 首页轮播图广告位code
	 * */
	public static final String INDEX_AD_CODE = "INDEX_BANNER";
}
