package com.azt.back.util;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.azt.api.pojo.Admin;

/**
 * @author 张栋
 * @version [版本号, 2014-4-11]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
public class ContextUtils {

	/**
	 * 是否更改了权限 添加或者更新 function 菜单时会将此值设为true 第一次查询后可将些值 设为false;
	 */
	public static Boolean has_changed = true;

	private static List<String> permission_all;

	/**
	 * SpringMvc下获取request
	 * 
	 * @return
	 */
	public static HttpServletRequest getRequest() {
		HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
		return request;

	}

	/**
	 * SpringMvc下获取session
	 * 
	 * @return
	 */
	public static HttpSession getSession() {
		HttpSession session = getRequest().getSession();
		return session;

	}

	public static Admin getAdmin() {
		Admin admin = (Admin) getSession().getAttribute(Globals.ADMIN_SESSION);
		return admin;
	}

	public static Admin setAdmin(Admin admin) {
		getSession().setAttribute(Globals.ADMIN_SESSION, admin);
		return admin;
	}

	public static void setFunctions(List<String> funcs) {
		getSession().setAttribute(Globals.PERMISSION_MODEL, funcs);
	}

	/**
	 * 登录用户所拥有的权限
	 * 
	 * @author 张栋
	 * @return
	 * @see [类、类#方法、类#成员]
	 */
	@SuppressWarnings("unchecked")
	public static List<String> getFunctions() {
		return (List<String>) getSession().getAttribute(Globals.PERMISSION_MODEL);
	}

	/**
	 * 网站的所有权限
	 * 
	 * @author 张栋
	 * @return
	 * @see [类、类#方法、类#成员]
	 */
	public static List<String> getAllPermission() {
		return permission_all;
	}

	public static void setAllPermission(List<String> perms) {
		permission_all = perms;
	}

}
