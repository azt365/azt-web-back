package com.azt.back.action.recommend;

import java.util.Date;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.azt.api.pojo.RecommendCompany;
import com.azt.api.service.RecommendCompanyService;
import com.azt.back.util.Globals;
import com.azt.model.AjaxJson;
import com.azt.model.page.Pagination;
import com.azt.utils.RequestUtils;

@Controller
@RequestMapping("/recommendCompany")
public class RecommendCompanyAction {
	
	@Autowired
	private RecommendCompanyService recommendCommpanyService;
	
	@RequestMapping("/recommendCPList.do")
	public String listRecommenCP(HttpServletRequest request, Integer pageNo,  HttpServletResponse response, ModelMap model){
		Map<String, String> searchMap = RequestUtils.getSearchMap(request);
		Pagination pagination = recommendCommpanyService.selectMainRecommendCp(pageNo == null ? 1 : pageNo,Globals.PAGE_SIZE, searchMap);
		model.put("pagination", pagination);
		RequestUtils.MapToModelMap(searchMap, request);
		return "recommend/recommend_company_list";
	}
	
	@RequestMapping("/editOradd.do")
	@ResponseBody
	public AjaxJson doAddOrEditRecommendCompany(RecommendCompany rc){
		AjaxJson aj=new AjaxJson();
		if(rc.getId()!=null){
			//编辑
			try {
				rc.setUpdatetime(new Date());
				recommendCommpanyService.editRecommendCompany(rc);
			} catch (Exception e) {
				aj=AjaxJson.error("编辑失败");
				e.printStackTrace();
			}
		}else{
			//添加
			try {
				Date da=new Date();
				rc.setUpdatetime(da);
				rc.setCreatetime(da);
				recommendCommpanyService.addRecommendCompany(rc);
			} catch (Exception e) {
				aj=AjaxJson.error("添加失败");
				e.printStackTrace();
			}
		}
		return aj;
	}
	
	@ResponseBody
	@RequestMapping("/queryEditRC.do")
	public AjaxJson queryEditRecommendCompany(Integer id){
		AjaxJson aj=new AjaxJson();
		try {
			RecommendCompany rc=recommendCommpanyService.getRecommendCompanyByid(id);
			aj.setObj(rc);
		} catch (Exception e) {
			aj=AjaxJson.error("请求失败");
			e.printStackTrace();
		}
		return aj;
	}
	
	@RequestMapping("/deleteCP")
	@ResponseBody
	public AjaxJson deleteCP(Integer id){
		AjaxJson aj=new AjaxJson();
		try {
			recommendCommpanyService.delRecommendCompany(id);
		} catch (Exception e) {
			aj=AjaxJson.error("删除失败");
			e.printStackTrace();
		}
		return aj;
	}
}
