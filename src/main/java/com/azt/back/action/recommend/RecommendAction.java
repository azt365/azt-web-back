package com.azt.back.action.recommend;

import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.azt.api.pojo.ChannelRecommend;
import com.azt.api.pojo.Content;
import com.azt.api.pojo.ContentRecommend;
import com.azt.api.service.ContentService;
import com.azt.api.service.RecommendService;
import com.azt.back.util.Globals;
import com.azt.model.AjaxJson;
import com.azt.model.page.Pagination;
import com.azt.utils.RequestUtils;

/**
 * @ClassName: RecommendAction
 * @Description: TODO(消息管理)
 * @author zhoutianchi
 * @date 2016年8月10日 下午3:07:36
 *       
 */
@Controller
@RequestMapping("/recommend")
public class RecommendAction
{

	@Autowired
	private RecommendService recommendService;
	
	@Autowired
	private ContentService contentService;
    
    /**
     * 列表页面
     * 
     * @author 周天驰
     * @param request
     * @param pageNo
     * @param response
     * @param model
     * @return String
     * @see [类、类#方法、类#成员]
     */
    @RequestMapping(value = "/v_list.do")
    public String channelList(HttpServletRequest request, Integer pageNo, HttpServletResponse response, ModelMap model)
    {
        Map<String, String> searchMap = RequestUtils.getSearchMap(request);
        Pagination pagination = recommendService.getChannelPage(pageNo == null ? 1 : pageNo, Globals.PAGE_SIZE, searchMap);
        model.put("pagination", pagination);
        
        RequestUtils.MapToModelMap(searchMap, request);
        return "recommend/channel_list";
    }
    
    /**
     * 根据id删除
     * 
     * @author 周天驰
     * @param aztmsg
     * @param request
     * @param response
     * @param model
     * @return
     * @see [类、类#方法、类#成员]
     */
    @RequestMapping(value = "/ajax_del.do", method = RequestMethod.GET)
    @ResponseBody
    public AjaxJson ajaxDel(ChannelRecommend channel, HttpServletRequest request, HttpServletResponse response, ModelMap model)
    {
        Integer id = channel.getId();
        if (id != null && !"".equals(id))
        {
            // 删除父子级
        	recommendService.deleteChannelById(id);
        }
        return AjaxJson.success();
    }
    
    /**
     * 保存新增
     * 
     * @author 周天驰
     * @param channel
     * @return
     * @see [类、类#方法、类#成员]
     */
    
    @RequestMapping(value = "/ajax_save.do", method = RequestMethod.POST)
    @ResponseBody
    public AjaxJson saveChannel(ChannelRecommend channel)
    {
        boolean exist = recommendService.findOrtherChannel(channel);
        if (exist)
        {
            return AjaxJson.error("该code值已经存在");
        }
        channel.setCreatetime(new Date());
        recommendService.saveChannel(channel);
        return AjaxJson.success();
    }
    
    /**
     * 保存编辑
     * 
     * @author 周天驰
     * @param channel
     * @return
     * @see [类、类#方法、类#成员]
     */
    
    @RequestMapping(value = "/ajax_edit.do", method = RequestMethod.POST)
    @ResponseBody
    public AjaxJson saveEdit(ChannelRecommend channel)
    {
        boolean exist = recommendService.findOrtherChannel(channel);
        if (exist)
        {
            return AjaxJson.error("该code值已经存在");
        }
        recommendService.editChannel(channel);
        return AjaxJson.success();
    }
    
    /**
     * 列表页面
     * 
     * @author 周天驰
     * @param request
     * @param pageNo
     * @param response
     * @param model
     * @return String
     * @see [类、类#方法、类#成员]
     */
    @RequestMapping(value = "/{channelid}/content_list.do")
    public String contentList(HttpServletRequest request, Integer pageNo, HttpServletResponse response, ModelMap model,@PathVariable("channelid") String channelid)
    {
        Map<String, String> searchMap = RequestUtils.getSearchMap(request);
        searchMap.put("channelid", channelid);
        Pagination pagination = recommendService.getContentPage(pageNo == null ? 1 : pageNo, Globals.PAGE_SIZE, searchMap);
        model.put("pagination", pagination);
        
        List<ChannelRecommend> list = recommendService.getChannelList();
        model.put("channels", list);
        
        List<Content> contentList = contentService.getContentList();
        model.put("contents", contentList);
        
        model.put("channelid", channelid);
        
        RequestUtils.MapToModelMap(searchMap, request);
        
        
        
        
        return "recommend/content_list";
    }
    
    @RequestMapping(value = "/content_del.do", method = RequestMethod.GET)
    @ResponseBody
    public AjaxJson contentAjaxDel(ContentRecommend content, HttpServletRequest request, HttpServletResponse response, ModelMap model)
    {
        Integer id = content.getId();
        if (id != null && !"".equals(id))
        {
            // 删除父子级
        	recommendService.deleteContentById(id);
        }
        return AjaxJson.success();
    }
    
    
    @RequestMapping(value = "/content_save.do", method = RequestMethod.POST)
    @ResponseBody
    public AjaxJson saveContent(ContentRecommend content)
    {
    	recommendService.saveContent(content);
        return AjaxJson.success();
    }

    
    @RequestMapping(value = "/content_edit.do", method = RequestMethod.POST)
    @ResponseBody
    public AjaxJson contentEdit(ContentRecommend content)
    {

    	recommendService.editContent(content);
        return AjaxJson.success();
    }
}
