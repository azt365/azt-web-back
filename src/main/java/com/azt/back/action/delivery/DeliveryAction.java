/*
 * 文 件 名:  DeliveryAction.java
 * 版    权:  ZF Technologies Co., Ltd. Copyright 2013-,  All rights reserved
 * 描    述:  <描述>
 * 修 改 人:  周天驰
 * 修改时间:  2016-10-11
 * 跟踪单号:  <跟踪单号>
 * 修改单号:  <修改单号>
 * 修改内容:  <修改内容>
 */
package com.azt.back.action.delivery;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.azt.api.pojo.Delivery;
import com.azt.api.service.DeliveryService;
import com.azt.back.util.Globals;
import com.azt.model.AjaxJson;
import com.azt.model.page.Pagination;
import com.azt.utils.RequestUtils;


@Controller
public class DeliveryAction
{
    
    @Autowired
    private DeliveryService manager;
    
    @RequestMapping(value = "delivery/v_list.do")
    public String deliveryslist(HttpServletRequest request, Integer pageNo,HttpServletResponse response, ModelMap model)
    {
        Map<String, String> searchMap = RequestUtils.getSearchMap(request);
        Pagination pagination = manager.getDeliveryPage(pageNo == null ? 1 : pageNo, Globals.PAGE_SIZE, searchMap);
        model.put("pagination", pagination);
   
        
        RequestUtils.MapToModelMap(searchMap, request);
        return "delivery/delivery_list";
    }
    
    /**
     * 保存或者更新
     * 
     * @author 周天驰
     * @param delivery
     * @param request
     * @param response
     * @param model
     * @return
     * @see [类、类#方法、类#成员]
     */
    @RequestMapping(value = "delivery/ajax_save.do", method = RequestMethod.POST)
    @ResponseBody
    public AjaxJson ajaxSave(Delivery delivery, HttpServletRequest request, HttpServletResponse response,
        ModelMap model)
    {
        Integer id = delivery.getId();
        if (id != null && !"".equals(id))
        {
            Delivery old = manager.getDeliveryById(id);
            old.setCompanyName(delivery.getCompanyName());
            old.setCompanyCode(delivery.getCompanyCode());
            old.setIfSupportApi(delivery.getIfSupportApi());
            old.setSeq(delivery.getSeq());
            manager.saveEdit(old);
        }
        else
        {
            manager.saveDelivery(delivery);
        }
        return AjaxJson.success();
    }
    
    /**
     * 删除
     * 
     * @author 周天驰
     * @param delivery
     * @param request
     * @param response
     * @param model
     * @return
     * @see [类、类#方法、类#成员]
     */
    @RequestMapping(value = "delivery/ajax_del.do", method = RequestMethod.GET)
    @ResponseBody
    public AjaxJson ajaxDel(Delivery delivery, HttpServletRequest request, HttpServletResponse response,
        ModelMap model)
    {
        Integer id = delivery.getId();
        if (id != null && !"".equals(id))
        {
            // 删除父子级
            manager.deleteAllById(id);
        }
        return AjaxJson.success();
    }
    
}
