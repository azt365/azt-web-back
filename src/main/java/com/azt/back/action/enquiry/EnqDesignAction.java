package com.azt.back.action.enquiry;

import com.azt.api.enums.EnqEnquiryEnum;
import com.azt.api.enums.EnqEnquiryQtypeEnum;
import com.azt.api.enums.EnqProjectEnum;
import com.azt.api.service.EnqEnquiryService;
import com.azt.back.util.Globals;
import com.azt.model.page.Pagination;
import com.azt.utils.FreemarkerUtils;
import com.azt.utils.RequestUtils;
import freemarker.template.TemplateModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;

/**
 * 预算询价
 * @author 张栋  2016年8月23日上午10:11:42
 */
@Controller
@RequestMapping("/enq")
public class EnqDesignAction {

	@Autowired
	private EnqEnquiryService service;
	
	
	/**
	 * 询价项目列表
	 * 
	 * @author 张栋 2016年8月8日下午3:25:06
	 */
	@RequestMapping("/v_design_list.do")
	public String v_project_list(HttpServletRequest request, HttpServletResponse response, ModelMap model, Integer pageNo) {

		Map<String, String> searchMap = RequestUtils.getSearchMap(request);
		String so_enq_or = searchMap.get("so_enq_or");
		if(so_enq_or==null){
			searchMap.put("so_enq_or", EnqEnquiryQtypeEnum.ENQ_TYPE_SHEJI.getIndex()+"");
		}
		
		Pagination pagination = service.getEnqEnquiryPage(pageNo == null ? 1 : pageNo, Globals.PAGE_SIZE, searchMap);
		model.put("pagination", pagination);

		// 项目状态
		Map<String, String> allProjectStat = EnqProjectEnum.getAllProjectStat();
		model.put("allstats", allProjectStat);

		// 询价设计状态
		Map<String, String> allEnqStat = EnqEnquiryEnum.getAllEnqStat();
		model.put("allEnqStat", allEnqStat);

		
		
		TemplateModel qtypes = FreemarkerUtils.getStaticModel(EnqEnquiryQtypeEnum.class);
		model.put("EnqEnquiryQtypeEnum", qtypes);
		
		

		RequestUtils.MapToModelMap(searchMap, request);
		TemplateModel staticModel = FreemarkerUtils.getStaticModel(EnqEnquiryEnum.class);
		model.put("EnqEnquiryEnum", staticModel);
		
		
		return "enquiry/design_list";
	}

}

