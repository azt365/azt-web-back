package com.azt.back.action.enquiry;

import java.util.Date;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.azt.api.pojo.ProRepositoryTitles;
import com.azt.api.pojo.ProRepositoryTitlesExcept;
import com.azt.api.service.ProRepositoryTitleService;
import com.azt.back.util.Globals;
import com.azt.model.AjaxJson;
import com.azt.model.page.Pagination;
import com.azt.utils.PasswordUtil;
import com.azt.utils.RequestUtils;

/**
 * 表单标题维护
 * */
@Controller
@RequestMapping("/repository")
public class RepositoryTitlesAction {
	@Autowired
	private ProRepositoryTitleService repositoryService;
	//列表
	@RequestMapping("/v_repositorytitle_list.do")
	public String list(Integer pageNo,HttpServletRequest request,Model model){
		Integer so_repositorytype=0;
		Map<String, String> searchMap=RequestUtils.getSearchMap(request);
		Pagination page=repositoryService.getRepositoryPage(pageNo==null?1:pageNo, Globals.PAGE_SIZE, searchMap);
		if(StringUtils.isNotBlank(searchMap.get("so_repositorytype"))){
			so_repositorytype=Integer.parseInt(searchMap.get("so_repositorytype"));
		}
		model.addAttribute("pagination", page);
		model.addAttribute("repositoryTitles", searchMap.get("so_repositoryTitles"));
		model.addAttribute("repositorytype", so_repositorytype);
		return "repository/repositorytitle_list";
	}
	
	//删除
	@RequestMapping("/delrepositorytitle.do")
	@ResponseBody
	public Integer del(Integer id){
		return repositoryService.delrepository(id);
	}
	
	//编辑或者新增
	@RequestMapping("/o_editorAddTitle.do")
	public String editAdd(HttpServletRequest request, HttpServletResponse response, ModelMap model){
		Integer id = RequestUtils.getInteger(request, "id");
		Integer pid = RequestUtils.getInteger(request, "pid");
		String title = RequestUtils.getString(request, "title");
		String type = RequestUtils.getString(request, "type");
		String md5_16 = PasswordUtil.MD5_16(title);
		//判断标题是否重复
		
			if(id!=null){
				ProRepositoryTitles repositoryTitle = repositoryService.getRepositoryTitleById(id);
				if(repositoryTitle!=null){
					repositoryTitle.setType(Integer.parseInt(type));
					repositoryTitle.setTitle(title);
					repositoryTitle.setUpdatetime(new Date());
					repositoryTitle.setPid(pid);
					repositoryTitle.setSecret(md5_16);
					repositoryService.updateRepositoryTitle(repositoryTitle);
				}
			}else{
				ProRepositoryTitles prt=new ProRepositoryTitles();
				prt.setTitle(title);
				prt.setType(Integer.parseInt(type));
				prt.setCreatetime(new Date());
				prt.setPid(pid);
				prt.setSecret(md5_16);
				repositoryService.save(prt);
			}

		return "redirect:/repository/v_repositorytitle_list.do";
	}
	
	//检查是否重复
	@RequestMapping("/o_checktitle.do")
	@ResponseBody
	public AjaxJson check(Integer id,String title,HttpServletRequest request, HttpServletResponse response){
		AjaxJson  json = new AjaxJson();
		boolean du = repositoryService.checkDuplicative(id,title);
		json.setSuccess(du);
		return json;
	}
	
	
	
	
	
	
	//-------------------------------标题仓库排除项----------------------------------------------------
	
	/**
	 * 明确排除掉的标题名称
	 * @author 张栋  2016年7月11日下午2:17:34
	 */
	@RequestMapping("/v_list_title_except.do")
	public String v_list_title_except(HttpServletRequest request, HttpServletResponse response, ModelMap model,Integer pageNo) {
		Map<String, String> searchMap=RequestUtils.getSearchMap(request);
		Pagination pagination =  repositoryService.getAllExceptPager(pageNo==null?1:pageNo, 15, searchMap);
		model.put("pagination", pagination);
		return "repository/repositorytitle_list_except";
	}
	
	/**
	 * 删除排除项
	 * @author 张栋  2016年7月11日下午2:20:04
	 */
	@RequestMapping("/o_del_except.do")
	@ResponseBody
	public AjaxJson o_del_except(HttpServletRequest request, HttpServletResponse response, ModelMap model,Integer id) {
		AjaxJson j = new AjaxJson();
		repositoryService.delrepositoryExcept(id);
		return j;
	}
	
	/**
	 * 保存一个排除项
	 * @author 张栋  2016年7月11日下午2:35:05
	 */
	@RequestMapping("/o_add_except.do")
	@ResponseBody
	public AjaxJson o_add_except(HttpServletRequest request, HttpServletResponse response, ModelMap model,ProRepositoryTitlesExcept except) {
		AjaxJson j = new AjaxJson();
		String saveExcept = repositoryService.saveExcept(except);
		if(saveExcept!=null){
			j.setMsg(saveExcept);
			j.setSuccess(false);
		}
		
		return j;
	}
	
	
}
