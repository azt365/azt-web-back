package com.azt.back.action.enquiry;

import com.azt.api.pojo.ProRepository;
import com.azt.api.service.ProRepositoryService;
import com.azt.model.AjaxJson;
import com.azt.model.page.Pagination;
import com.azt.utils.MyStringUtils;
import com.azt.utils.PasswordUtil;
import com.azt.utils.RequestUtils;
import net.sf.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/repository")
public class RepositoryAction {


	@Autowired
	private ProRepositoryService prs;

	@RequestMapping("/v_list.do")
	public String list(HttpServletRequest request, HttpServletResponse response, Integer pageNo ,ModelMap model , Integer pid,String se) {


		Map<String, String> searchMap = RequestUtils.getSearchMap(request);
		boolean haskey = searchMap.containsKey("so_pid");
		model.put("haskey",haskey);
		Pagination pagination = prs.getRepositoryByPage(pageNo == null ? 1 : pageNo, 15, searchMap);

		model.put("p", pagination);
		model.put("form", "reForm");
		model.put("mulity", "reMu");

		return "repository/repository_list";
	}

	@RequestMapping(value="/o_newadd.do")
	@ResponseBody
	public AjaxJson ajNewAdd(HttpServletRequest req,HttpServletResponse res,ProRepository pr,ModelMap map){
		AjaxJson aj=new AjaxJson();
		String title = MyStringUtils.replaceBlank(pr.getTitle());
		pr.setTitle(title);
		String titlemd5 = PasswordUtil.MD5_16(title);
		pr.setSecret(titlemd5);
		List<ProRepository> pro=prs.getRepositoryByPidAndSecret(pr.getSecret(),pr.getPid());
		if(pro.size()==0){
			prs.saveOrUpdateEntity(pr);
		}
		return aj;
	}

	@RequestMapping(value="/o_add.do")
	@ResponseBody
	public JSONObject ajaxAdd( ModelMap model ,ProRepository repository){
		JSONObject json = new JSONObject();
		try{
			if(repository.getId()!=null&&!"".equals(repository.getId())){
				json.put("msg" , "编辑成功");
				ProRepository oldpr = prs.getEntityById(repository.getId());
				String title = repository.getTitle().replaceAll(" ", "");
				oldpr.setTitle(title);
				oldpr.setUpdatetime(new Date());
				String titlemd5 = PasswordUtil.MD5_16(title);
				oldpr.setSecret(titlemd5);
				if(!oldpr.getSecret().equals(titlemd5) && !prs.ifTitleExist(titlemd5 , repository.getProCategory().getId())){
					throw new Exception("同一类目下的仓库标题不能重复");
				}else{
					this.prs.saveOrUpdateEntity(oldpr);
				}
			}else{
				json.put("msg" , "新增成功");
				String title = repository.getTitle().replaceAll(" ", "");
				repository.setTitle(title);
				String titlemd5 = PasswordUtil.MD5_16(title);
				repository.setSecret(titlemd5);
				repository.setCreatetime(new Date());
				if(prs.ifTitleExist(titlemd5 , repository.getProCategory().getId())){
					this.prs.saveOrUpdateEntity(repository);
				}else{
					throw new Exception("同一类目下的仓库标题不能重复");
				}
			}
			json.put("success", true);
		}catch(Exception e){
			e.printStackTrace();
			json.put("success", false);
			json.put("msg",	"操作失败："+e.getMessage());
		}
		return json;
	}
	
	
	@RequestMapping(value="/o_del.do")
	@ResponseBody
	public JSONObject ajaxDel(ProRepository pr){
		JSONObject json = new JSONObject();
		try{
			if(pr.getId()!=null&&!"".equals(pr.getId())){
				json.put("msg" , "删除成功");
				prs.deleteEntity(pr);
				json.put("success", true);
			}else{
				json.put("msg" , "删除失败，记录不存在");
				json.put("success", false);
			}
		}catch(Exception e){
			e.printStackTrace();
			json.put("success", false);
			json.put("msg",	"删除失败");
		}
		return json;
	}
	
	@RequestMapping(value="/o_delete_more.do")
	@ResponseBody
	public JSONObject ajaxDel(HttpServletRequest request,
			HttpServletResponse response , String rids){
		JSONObject json = new JSONObject();
		try{
			if(rids.length() > 0){
				prs.deleteMore(rids.split(","));
			}
			json.put("msg" , "删除成功");
			json.put("success", true);
		}catch(Exception e){
			e.printStackTrace();
			json.put("success", false);
			json.put("msg",	"删除失败");
		}
		return json;
	}
	
	/**
	 *根据 secret 与  类目id 进行删除
	 * @author 张栋  2016年7月21日上午11:02:49
	 */
	@RequestMapping("/o_delete_secret.do")
	@ResponseBody
	public AjaxJson list(ModelMap model,String secret,Integer categoryid) {
		AjaxJson j = new AjaxJson();
		
		prs.deleteByCategoryIdAndSecret(secret,categoryid);
		
		return j;
	}
	
}
