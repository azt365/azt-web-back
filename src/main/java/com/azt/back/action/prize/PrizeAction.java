package com.azt.back.action.prize;

import java.io.IOException;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.xiaoleilu.hutool.util.StrUtil;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.azt.api.pojo.Delivery;
import com.azt.api.pojo.PrizeRecord;
import com.azt.api.pojo.PrizeSet;
import com.azt.api.service.DeliveryService;
import com.azt.api.service.PrizeRecordService;
import com.azt.api.service.PrizeSetService;
import com.azt.back.util.Globals;
import com.azt.model.AjaxJson;
import com.azt.model.page.Pagination;
import com.azt.utils.CommonUtil;
import com.azt.utils.RequestUtils;
@Controller
@RequestMapping("/prize")
public class PrizeAction {
	//分页代码展示
	@Autowired
	private PrizeRecordService prs;
	@Autowired
	private PrizeSetService pss;
	@Autowired
	//物流公司
	private DeliveryService delivery;
	@RequestMapping(value="/prizerecord_list.do")
	public String selectRecordList(HttpServletRequest req,HttpServletResponse res,ModelMap map,Integer pageNo){
		 Map<String, String> searchMap = RequestUtils.getSearchMap(req);
		 Pagination pagination = prs.selectRecordList(pageNo == null ? 1 : pageNo, Globals.PAGE_SIZE, searchMap);
		 map.addAttribute("pagination", pagination);
		 RequestUtils.MapToModelMap(searchMap, req);
		 List<Delivery> so_alldelivery=delivery.findAllDelivery();
		 map.addAttribute("so_alldelivery", so_alldelivery);
		 return"prize/record_list";
	 }
	//保存新增或编辑
	@RequestMapping(value="/saveorupdate.do")
	@ResponseBody
	public AjaxJson istrueAjax(HttpServletRequest request,HttpServletResponse response,ModelMap map,PrizeRecord prizeRecord){
		AjaxJson aj=new AjaxJson();
		Integer courierCompanyId=prizeRecord.getCourierCompanyId();
		Delivery de=delivery.getDeliveryById(courierCompanyId);
		prizeRecord.setDevCompanyName(de.getCompanyName());
		prs.SaveOrUpdateRecord(prizeRecord);
		return aj;
	}
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value="/importRecoToExcel.do")
	public void importRecoToExcel(HttpServletRequest request,HttpServletResponse response,String state) throws IOException{
        List<PrizeRecord> allm=prs.selectExportExcelRecord(state);

         HSSFWorkbook wb=new HSSFWorkbook();
   		 HSSFSheet sheet=wb.createSheet();
   		 HSSFRow row=sheet.createRow(0);
   		 HSSFCell cell=row.createCell(0);
   		 String[] titles={"中奖账户","奖品名称","中奖时间","联系人","联系电话","邮寄地址","状态"};
		 for (int i = 0; i < titles.length; i++) {
			 cell.setCellValue(titles[i]);
			 cell=row.createCell(i+1);
		}
   		 
		 int index = 1;
   		 for(PrizeRecord record : allm) {
   			HSSFRow row1 = sheet.createRow(index);
   			HSSFCell mobileCell = row1.createCell(0);
   			HSSFCell giftnameCell = row1.createCell(1);
   			HSSFCell createtimeCell = row1.createCell(2);
   			HSSFCell contactnameCell = row1.createCell(3);
   			HSSFCell contacttelCell = row1.createCell(4);
   			HSSFCell contactaddCell = row1.createCell(5);
   			HSSFCell stateCell =row1.createCell(6);
   			mobileCell.setCellValue(record.getMobile());
   			PrizeSet prize = record.getPrizeSet();
   			Integer prizeId=record.getPrizeId();

   			Integer prizeSetType=record.getType();
   			giftnameCell.setCellValue(prize != null ? prize.getGiftName() : "");
   			createtimeCell.setCellValue(CommonUtil.formatDate(record.getCreatetime(), "yyyy-MM-dd HH:mm")); 
   			contactnameCell.setCellValue(record.getContactName());
   			contacttelCell.setCellValue(record.getContactTel());
            contactaddCell.setCellValue(record.getContactAdd());
            Integer stateName=record.getState();
            if(stateName==0 && prizeSetType==2){
            	stateCell.setCellValue("未寄出");
            }
            if(stateName==0 && prizeSetType==1){
            	stateCell.setCellValue("未充值");
			}
            if(stateName==1){
            	stateCell.setCellValue("已寄出");
            }
            if(stateName==2){
            	stateCell.setCellValue("已充值");
            }
            if(stateName==3){
            	stateCell.setCellValue("已充入积分");
            }
            if(stateName==-1){
            	stateCell.setCellValue("放弃");
            }
            
   			index ++;
   		 }
            
   		    java.util.Date date = new java.util.Date();
   		    SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
   		    String currenttime=df.format(date);
   		    if(state.equals("0")){
   		    	String title = new String(("未兑换奖品清单"+""+currenttime+".xls").getBytes("UTF-8"), "iso8859-1");
   		    	response.setContentType("application/vnd.ms-excel;");
   	            response.setHeader("Content-Disposition", "attachment;filename="+title);
   		    }else{
   		    	String title = new String(("已兑换奖品清单"+""+currenttime+".xls").getBytes("UTF-8"), "iso8859-1");
   		    	response.setContentType("application/vnd.ms-excel;");
   	            response.setHeader("Content-Disposition", "attachment;filename="+title);
   		    }
            
            ServletOutputStream outputStream = response.getOutputStream();
   	        wb.write(outputStream);
   	        outputStream.flush();  
   	        outputStream.close();  
   	}

   		 
   
   @RequestMapping(value="/prizeset_list.do")
   public String selectSetList(HttpServletRequest req,HttpServletResponse res,ModelMap map,Integer pageNo){
	   Map<String, String> searchMap = RequestUtils.getSearchMap(req);
	   Pagination pagination = pss.selectSetList(pageNo == null ? 1 : pageNo, Globals.PAGE_SIZE, searchMap);
	   map.addAttribute("pagination", pagination);
	   RequestUtils.MapToModelMap(searchMap, req);
	   return "prize/set_list";
  
    }
//   @RequestMapping(value="/inser.do")
//   @ResponseBody
//   public AjaxJson  addAndUpdate(PrizeSet ps,HttpServletRequest request,HttpServletResponse response,ModelMap map){
//	 AjaxJson aj=new AjaxJson();
//	 if(ps.getId()==null){
//	   pss.saveOrUpdate(ps);
//	}
//   return aj;
//  }
   @RequestMapping(value="/inser.do")
   public String insert(ModelMap model){
	   List<PrizeSet> pse=pss.selectSetList();
	   if(pse.size()>=11){
		   model.addAttribute("canAdd", false);
	   }
	   return "prize/sett_list";
   }
   
   
   @RequestMapping(value="/updat_list.do")
   public String updat(PrizeSet ps,HttpServletRequest request,HttpServletResponse response,ModelMap map,Integer id){
	   PrizeSet selectone =pss.getPrizeSetById(id);
	   map.addAttribute("selectone",selectone );
	   return "prize/sett_list";
   }
   
   //新增和编辑写在同一个界面上
   @RequestMapping(value="/updat.do")
   @ResponseBody
   public AjaxJson  ajaxUpdate(PrizeSet ps,HttpServletRequest request,HttpServletResponse response,ModelMap map,String giftName){
	AjaxJson aj=new AjaxJson();
	  pss.saveOrUpdate(ps);
      return aj;
  }
   
   @RequestMapping(value="/trueset.do")
   @ResponseBody
   public AjaxJson  ajaxTrueset(HttpServletRequest request,HttpServletResponse response,ModelMap map,PrizeSet set){
	   AjaxJson aj=new AjaxJson();
	   pss.saveOrUpdate(set);
	   return aj;
	   
  }
   //假删
   @RequestMapping(value="/deleteById.do")
   @ResponseBody
   public AjaxJson  ajaxdelete(HttpServletRequest request,HttpServletResponse response,ModelMap map,PrizeSet set){
	   AjaxJson aj=new AjaxJson();
	   pss.deleteById(set);
	   return aj;
	   
  }
   
}
		



