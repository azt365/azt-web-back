package com.azt.back.action.conference;
import com.azt.api.pojo.SignupMember;
import com.azt.api.service.SignupMemberService;
import com.azt.back.util.Globals;
import com.azt.model.page.Pagination;
import com.azt.utils.RequestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;

/**
 * Created by 叶青 on 2017/3/28.
 */
@Controller
@RequestMapping("/signup")
public class SignupMemberAction {
    @Autowired
    SignupMemberService signupMemberService;
    @RequestMapping("/signupmember_list.do")
    public String selectSignupMemberList(HttpServletRequest req, HttpServletResponse rep, ModelMap map, Integer pageNo){
        Map<String, String> searchMap = RequestUtils.getSearchMap(req);
        Pagination pagination = signupMemberService.signupMemberList(pageNo == null ? 1 : pageNo, Globals.PAGE_SIZE, searchMap);
        map.addAttribute("pagination", pagination);
        RequestUtils.MapToModelMap(searchMap, req);
        return "conference/signup_member_list";
    }

}
