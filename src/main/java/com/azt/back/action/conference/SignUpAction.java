package com.azt.back.action.conference;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.dubbo.common.utils.StringUtils;
import com.azt.api.dto.UserMsgDto;
import com.azt.api.pojo.MsgConfig;
import com.azt.api.pojo.SignUp;
import com.azt.api.service.ConfigService;
import com.azt.api.service.DictionaryService;
import com.azt.api.service.MsgConfigService;
import com.azt.api.service.SignUpService;
import com.azt.back.util.Globals;
import com.azt.model.AjaxJson;
import com.azt.model.page.Pagination;
import com.azt.utils.BeanUtils;
import com.azt.utils.FormatUtil;
import com.azt.utils.RequestUtils;

/**
 * @ClassName: SignUpAction
 * @Description: 报名管理
 * @author: zhaheng
 * @date: 2017年3月28日 上午11:07:14
 */
@Controller
@RequestMapping("/signup")
public class SignUpAction {

	@Autowired
	private SignUpService signUpservice;

	@Autowired
	private DictionaryService dictionaryService;

	@Autowired
	private MsgConfigService msgConfigService;
    
	@Autowired
	private ConfigService configService;

	@RequestMapping(value = "/v_signup_list.do")
	public String SignList(HttpServletRequest request, Integer pageNo, HttpServletResponse response, ModelMap model) {
		// 查询所有报名信息
		Map<String, String> searchMap = RequestUtils.getSearchMap(request);
		Pagination pagination = signUpservice.getSignUpPage(pageNo == null ? 1 : pageNo, Globals.PAGE_SIZE, searchMap);
		model.put("pagination", pagination);
		model.addAttribute("wantrole", dictionaryService.getDictionByParentCode("wantrole"));
		RequestUtils.MapToModelMap(searchMap, request);
		return "conference/v_signup_list";
	}

	// 新增或编辑安排座位
	@ResponseBody
	@RequestMapping(value = "/o_editsign.do")
	public AjaxJson editSign(SignUp signup) {
		// 查看不同的会议座位号是发重复
		try {
			SignUp signUpByseatNo = signUpservice.getSignUpByseatNo(signup.getSeatNo());
			// 编辑
			if (signup.getId() != null) {
				SignUp signUpBy = signUpservice.getSignUpById(signup.getId());
				if (signUpByseatNo != null && !signUpByseatNo.getId().equals(signUpBy.getId())) {
					return AjaxJson.error("该会议座位号已安排！");
				}
				BeanUtils.copyBean2Bean(signUpBy, signup);
				signUpservice.saveOrUpdateSignUp(signUpBy);
			} else {
				if (signUpByseatNo != null) {
					return AjaxJson.error("该会议座位号已安排！");
				}
			}
			signUpservice.saveOrUpdateSignUp(signup);
		} catch (Exception e) {
			return AjaxJson.error("保存失败");
		}

		return AjaxJson.success();
	}

	// 删除数据
	@ResponseBody
	@RequestMapping("/ajax_delsign.do")
	public AjaxJson delSign(Integer id) {
		AjaxJson json = new AjaxJson();
		try {
			signUpservice.delSignById(id);
			return json.success();
		} catch (Exception e) {
			json.setMsg("删除失败");
			json.setSuccess(false);
			return json;
		}
	}

	// 短信发送
	@ResponseBody
	@RequestMapping("/ajax_signMsg.do")
	public AjaxJson sendSignMsg(Integer id) {
		Map<String, String> params = new HashMap();
		SignUp signUpBy = signUpservice.getSignUpById(id);
		String weburl = "http://"+configService.getConfigValueByCode("WEB_SITE");
		MsgConfig msgConfig = msgConfigService.getMsgConfigByCode("SIGN_MESSAGE");
		try {
			if(msgConfig==null){
				return AjaxJson.error("消息记录没有配置会员短信模板");
			}
			
			if(signUpBy.getMeeting()!=null){
				params.put("conferenceName", signUpBy.getMeeting().getMeetingName());// 会议名称
				params.put("signAddress", signUpBy.getMeeting().getAddress());// 参会地址
				params.put("signtime", signUpBy.getMeeting().getSigntime() != null ? FormatUtil.getDateStr(signUpBy.getMeeting().getSigntime(),FormatUtil.ISO8601DATE_NORMAL_FORMAT2) : "");// 签到时间
				params.put("meettime", signUpBy.getMeeting().getMeettime() != null ? FormatUtil.getDateStr(signUpBy.getMeeting().getMeettime(),FormatUtil.ISO8601DATE_NORMAL_FORMAT2) : "");// 会议时间
			}
		
			String website = weburl + "/pressconference/z/" + id;
			params.put("website", website);// 二维码地址
			
			if(StringUtils.isBlank(params.get("conferenceName"))){
				return AjaxJson.error("会议名称没有内容");
			}
			if(StringUtils.isBlank(params.get("signAddress"))){
				return AjaxJson.error("会议地址没有内容");
			}
			if(StringUtils.isBlank(params.get("signtime"))){
				return AjaxJson.error("会议签到时间没有内容");
			}
			if(StringUtils.isBlank(params.get("meettime"))){
				return AjaxJson.error("会议开始时间没有内容");
			}
			if(StringUtils.isBlank(params.get("website"))){
				return AjaxJson.error("会议二维码地址没有内容");
			}
			
			List usersMsg = new ArrayList();
			UserMsgDto umd = new UserMsgDto();
			umd.setMobile(signUpBy.getMobile());
			usersMsg.add(umd);
			msgConfigService.sendMessage("SIGN_MESSAGE", params, null, usersMsg);
			// 设置发送成功
			signUpBy.setIfMsg(1);
			signUpservice.saveOrUpdateSignUp(signUpBy);
		} catch (Exception e) {
			return AjaxJson.error("发送失败");
		}

		return AjaxJson.success();
	}

	// 查看短信
	@ResponseBody
	@RequestMapping("/get_signMsg.do")
	public AjaxJson getMsg(Integer id) {
		Map<String, String> params = new HashMap();
		SignUp signUpBy = signUpservice.getSignUpById(id);
		String weburl = "http://"+configService.getConfigValueByCode("WEB_SITE");
		MsgConfig msgConfig = msgConfigService.getMsgConfigByCode("SIGN_MESSAGE");
		try {
			if (msgConfig != null) {
				String telcotent = msgConfig.getTelContent();
				
				if(signUpBy.getMeeting()!=null){
					params.put("conferenceName", signUpBy.getMeeting().getMeetingName());// 会议名称
					params.put("signAddress", signUpBy.getMeeting().getAddress());// 参会地址
					params.put("signtime", signUpBy.getMeeting().getSigntime() != null ? FormatUtil.getDateStr(signUpBy.getMeeting().getSigntime(),FormatUtil.ISO8601DATE_NORMAL_FORMAT2) : "");// 签到时间
					params.put("meettime", signUpBy.getMeeting().getMeettime() != null ? FormatUtil.getDateStr(signUpBy.getMeeting().getMeettime(),FormatUtil.ISO8601DATE_NORMAL_FORMAT2) : "");// 会议时间
				}
				
				String website = weburl + "/pressconference/z/" + id;
				params.put("website", website);
				
				if(StringUtils.isBlank(params.get("conferenceName"))){
					return AjaxJson.error("会议名称没有内容");
				}
				if(StringUtils.isBlank(params.get("signAddress"))){
					return AjaxJson.error("会议地址没有内容");
				}
				if(StringUtils.isBlank(params.get("signtime"))){
					return AjaxJson.error("会议签到时间没有内容");
				}
				if(StringUtils.isBlank(params.get("meettime"))){
					return AjaxJson.error("会议开始时间没有内容");
				}
				if(StringUtils.isBlank(params.get("website"))){
					return AjaxJson.error("会议二维码地址没有内容");
				}
				
				telcotent = telcotent.replace("{conferenceName}", params.get("conferenceName")).replace("{signAddress}", params.get("signAddress"))
						.replace("{signtime}", params.get("signtime"))
						.replace("{meettime}", params.get("meettime"))
						.replace("{website}", params.get("website"));
				return AjaxJson.success(telcotent);
			} else {
				return AjaxJson.error("查看失败,消息记录没有配置会员短信模板");
			}
		} catch (Exception e) {
			return AjaxJson.error("查看失败");
		}

	}

	//查看详情
	@RequestMapping("/sign_detail.do")
	public String signdetail(Integer id,ModelMap model){
		SignUp signUpBy = signUpservice.getSignUpById(id);
		model.addAttribute("wantrole", dictionaryService.getDictionByParentCode("wantrole"));
		model.addAttribute("signtype", dictionaryService.getDictionByParentCode("signtype"));
		model.put("signUpBy", signUpBy);
		return "conference/v_signup_detail";
	}
	
	
/*	public static void main(String[] args) {
		String code = "1";
		Integer sign = 1;
		字符串不能和Integer比较
		if (Integer.valueOf(code).equals(sign)) {
			System.out.println("相等");
		} else {
			System.out.println("不相等");
		}

		Integer a = new Integer(3);
		Integer b = new Integer(3);
		if (a.equals(b)) {
			System.out.println("a相等");
		} else {
			System.out.println("a不相等");
		}

	}
*/
}
