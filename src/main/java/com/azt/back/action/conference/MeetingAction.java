package com.azt.back.action.conference;

import com.azt.api.pojo.req.MeetingReq;
import com.azt.api.service.MeetingService;
import com.azt.back.util.Globals;
import com.azt.model.AjaxJson;
import com.azt.model.page.Pagination;
import com.azt.utils.RequestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * 会议报名管理页面
 * Created by LiQZ on 2017/4/6.
 */
@Controller
@RequestMapping("meeting")
public class MeetingAction {

    @Autowired
    private MeetingService meetingService;

    @RequestMapping("list.do")
    public String list(HttpServletRequest request, @RequestParam(defaultValue = "1") Integer pageNo, ModelMap model) {
        Map<String, String> searchMap = RequestUtils.getSearchMap(request);
        Pagination pagination = meetingService.getPage(pageNo, Globals.PAGE_SIZE, searchMap);
        model.put("pagination", pagination);
        RequestUtils.MapToModelMap(searchMap, request);
        return "conference/meeting_list";
    }

    @ResponseBody
    @RequestMapping("add.do")
    public AjaxJson add(MeetingReq req) {
        meetingService.save(req);
        return AjaxJson.success();
    }

    @ResponseBody
    @RequestMapping("change.do")
    public AjaxJson change(Integer id) {
        meetingService.updateToCurrent(id);
        return AjaxJson.success();
    }

    @ResponseBody
    @RequestMapping("delete.do")
    public AjaxJson delete(Integer id) {
        meetingService.delete(id);
        return AjaxJson.success();
    }


}
