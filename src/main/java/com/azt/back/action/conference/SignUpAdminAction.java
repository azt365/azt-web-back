package com.azt.back.action.conference;

import com.azt.api.service.SignUpAdminService;
import com.azt.back.util.Globals;
import com.azt.model.AjaxJson;
import com.azt.model.page.Pagination;
import com.azt.service.wechat.WeChatConstant;
import com.azt.utils.RequestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * 会议报名管理员信息
 * Created by LiQZ on 2017/3/28.
 */
@Controller
@RequestMapping("conference")
public class SignUpAdminAction {

    @Autowired
    private SignUpAdminService signUpAdminService;

    @RequestMapping("admin_list.do")
    public String list(HttpServletRequest request, @RequestParam(defaultValue = "1") Integer pageNo, ModelMap model) {
        Map<String, String> searchMap = RequestUtils.getSearchMap(request);
        Pagination pagination = signUpAdminService.getPage(pageNo, Globals.PAGE_SIZE, searchMap);
        model.put("pagination", pagination);
        RequestUtils.MapToModelMap(searchMap, request);
        String redirectUrl = WeChatConstant.getShowWechatDialog(WeChatConstant.REDIRECT_URL);
        model.put("redirectUrl", redirectUrl);
        return "conference/admin_list";
    }

    @ResponseBody
    @RequestMapping("admin_delete.do")
    public AjaxJson delete(String id) {
        signUpAdminService.delete(id);
        return AjaxJson.success();
    }

}
