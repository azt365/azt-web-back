package com.azt.back.action.content;

import com.azt.api.pojo.Content;
import com.azt.api.service.ChannelService;
import com.azt.api.service.ConfigService;
import com.azt.api.service.ContentService;
import com.azt.back.action.BaseAction;
import com.azt.model.easy.EasyTree;
import com.azt.model.page.Pagination;
import com.azt.utils.BCrypt;
import com.azt.utils.RequestUtils;
import com.xiaoleilu.hutool.date.DateUtil;
import net.sf.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/content")
public class ContentAction extends BaseAction {

	@Autowired
	private ContentService cs;
	
	@Autowired
	private ChannelService chs;

	@Autowired
	private ConfigService configService;
	
	@RequestMapping("/v_content_home.do")
	public String home(HttpServletRequest request, HttpServletResponse response, ModelMap model,Integer pageno) {
		return "content/content_home";
	}
	
	
	@RequestMapping("/v_list.do")
	public String list(ModelMap model) {
		int pageNo = getParaToInt("pageNo", 1);
		Map searchMap = RequestUtils.getSearchMap(request);
		Date sdate = getParaToDate("so_sdate");
		Date edate = getParaToDate("so_edate");
		if(edate != null) {
			edate = DateUtil.offsetDay(edate, 1);
		}
		searchMap.put("sdate", sdate);
		searchMap.put("edate", edate);

		Pagination pagination = cs.getContentPage(pageNo, 10, searchMap);

		searchMap.remove("sdate");
		searchMap.remove("edate");
		RequestUtils.MapToModelMap(searchMap, request);
		model.put("p", pagination);
		
		return "content/content_list";
	}
	
	@RequestMapping("/v_add.do")
	public String add(ModelMap model , Integer channelId , Integer id) {
		if(id != null){
			model.put("content", cs.getContentById(id));
		}
		model.put("channelId", channelId);
		boolean preview = getParaToBool("preview", false);
		if(preview) {
			model.put("token", BCrypt.hashpw("azt_news_preview", BCrypt.gensalt()));
			model.addAttribute("WEB_SITE", configService.getConfigValueByCode("WEB_SITE"));
			model.addAttribute("now", DateUtil.today());
		}
		return "content/content_addoredit";
	}
	
	@RequestMapping(value="/o_add.do")
	@ResponseBody
	public JSONObject ajaxAdd(HttpServletRequest request,
			HttpServletResponse response, ModelMap model ,Content content){
		JSONObject json = new JSONObject();
		try{
			if(content.getId()!=null&&!"".equals(content.getId())){
				json.put("msg" , "编辑成功");
				Content oldContent = cs.getContentById(content.getId());
				//oldContent.setContent(HtmlUtils.html(content.getContent()));//进行一下转码，防止攻击    这里不能转  会有BUG
				oldContent.setContent(content.getContent());
				oldContent.setEditor(content.getEditor());
				oldContent.setTitle(content.getTitle());
				oldContent.setIfRecommend(content.getIfRecommend());
				oldContent.setSeq(content.getSeq());
				oldContent.setTitleImg(content.getTitleImg());
				oldContent.setUpdatetime(new Date());
				oldContent.setHit(content.getHit());
				oldContent.setSubTitle(content.getSubTitle());
				cs.saveOrUpdateContent(oldContent);
			}else{
				json.put("msg" , "新增成功");
//				content.setContent(HtmlUtils.html(content.getContent()));//进行一下转码，防止攻击  这里不能转  会有BUG
				content.setContent(content.getContent());//进行一下转码，方式攻击
				content.setCreatetime(new Date());
				cs.saveOrUpdateContent(content);
			}
			json.put("success", true);
		}catch(Exception e){
			e.printStackTrace();
			json.put("success", false);
			json.put("msg",	"操作失败："+e.getMessage());
		}
		return json;
	}
	
	
	@RequestMapping(value="/o_del.do")
	@ResponseBody
	public JSONObject ajaxDel(HttpServletRequest request,
			HttpServletResponse response, ModelMap model , Content content){
		JSONObject json = new JSONObject();
		try{
			if(content.getId()!=null&&!"".equals(content.getId())){
				json.put("msg" , "删除成功");
				cs.deleteContent(content);
				json.put("success", true);
			}else{
				json.put("msg" , "删除失败，记录不存在");
				json.put("success", false);
			}
		}catch(Exception e){
			e.printStackTrace();
			json.put("success", false);
			json.put("msg",	"删除失败");
		}
		return json;
	}
	

	@RequestMapping("o_delete_more.do")
	@ResponseBody
	public JSONObject batchDelete(@RequestParam("ids") String ids) {
		JSONObject json = new JSONObject();
		json.put("msg" , "删除成功");
		json.put("success", false);
		try {
			cs.deleteByIds(ids);
		} catch (Exception e) {
			e.printStackTrace();
			json.put("success", false);
			json.put("msg",	"删除失败");
		}
		return json;
	}

	@RequestMapping("/v_tree.do")
	@ResponseBody
	public List<EasyTree> getChannelTree(){
		return chs.getChannelTree();
	}
	

}
