/*
 * 文 件 名:  AreaAction.java
 * 版    权:  ZF Technologies Co., Ltd. Copyright 2013-,  All rights reserved
 * 描    述:  <描述>
 * 修 改 人:  周天驰
 * 修改时间:  2016-8-19
 * 跟踪单号:  <跟踪单号>
 * 修改单号:  <修改单号>
 * 修改内容:  <修改内容>
 */
package com.azt.back.action.area;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.azt.api.pojo.Area;
import com.azt.api.service.AreaService;
import com.azt.model.AjaxJson;

import net.sf.json.JSONObject;

/**
 * 区域
 */
@Controller
public class AreaAction
{
    
    @Autowired
    private AreaService manager;
    
    @RequestMapping(value = "area/v_area.do", method = RequestMethod.GET)
    public String arealist(HttpServletRequest request, HttpServletResponse response, ModelMap model)
    {
        // 子菜单的父节点id
        String nextp = request.getParameter("nextP");
        String prevp = request.getParameter("prevP");
        
        Integer parentId = 1;
        Integer prevParent = 1;
        Area d = null;
        if (nextp != null && StringUtils.isNumeric(nextp))
        {
            parentId = Integer.parseInt(nextp);
        }
        if (parentId != 1)
        {
            d = this.manager.getAreaById(parentId);
            model.addAttribute("parentCode", d.getCode());
        }
        List<Area> areas = manager.getAreaByParentId(parentId);
        
        if (prevp == null)
        {
            if (parentId != 1)
            {
                if (areas.size() != 1)
                {
                    prevParent = manager.getAreaById(parentId).getParentId();
                }
            }
        }
        else
        {
            prevParent = Integer.parseInt(prevp);
        }
        // 当前目录下的所有
        model.addAttribute("areas", areas);
        // 当前目录的父节点
        model.addAttribute("parentId", parentId);
        // 用来返回上一层
        model.addAttribute("prevParent", prevParent);
        return "area/area_list";
    }
    
    /**
     * 保存或者更新
     * 
     * @author 张栋
     * @param area
     * @param request
     * @param response
     * @param model
     * @return
     * @see [类、类#方法、类#成员]
     */
    @RequestMapping(value = "area/ajax_save.do", method = RequestMethod.POST)
    @ResponseBody
    public AjaxJson ajaxSave(Area area, HttpServletRequest request, HttpServletResponse response,
        ModelMap model)
    {
        Integer id = area.getId();
        boolean exist = manager.findOtherArea(area);
        if (exist)
        {
            return AjaxJson.error("该code值已经存在");
        }
        if (id != null && !"".equals(id))
        {
            Area old = manager.getAreaById(id);
            old.setName(area.getName());
            old.setSortNum(area.getSortNum());
            old.setCode(area.getCode());
            manager.saveOrUpdate(old);
        }
        else
        {
            manager.save(area);
        }
        return AjaxJson.success();
    }
    
    @RequestMapping(value = "area/changeSort.do", method = RequestMethod.POST)
    @ResponseBody
    public JSONObject changeSort(Area area, HttpServletRequest request, HttpServletResponse response,
        ModelMap model, Integer id, Integer sort)
    {
        JSONObject json = new JSONObject();
        try
        {
            Area d = this.manager.getAreaById(id);
            d.setSortNum(sort);
            this.manager.saveOrUpdate(d);
            json.put("res", true);
        }
        catch (Exception e)
        {
            e.printStackTrace();
            json.put("res", false);
        }
        return json;
    }
    
    /**
     * 删除
     * 
     * @author 张栋
     * @param area
     * @param request
     * @param response
     * @param model
     * @return
     * @see [类、类#方法、类#成员]
     */
    @RequestMapping(value = "area/ajax_del.do", method = RequestMethod.GET)
    @ResponseBody
    public AjaxJson ajaxDel(Area area, HttpServletRequest request, HttpServletResponse response,
        ModelMap model)
    {
        Integer id = area.getId();
        if (id != null && !"".equals(id))
        {
            // 删除父子级
            manager.deleteAllById(id);
        }
        return AjaxJson.success();
    }
    
}
