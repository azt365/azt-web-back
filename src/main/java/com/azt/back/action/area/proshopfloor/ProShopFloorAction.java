package com.azt.back.action.area.proshopfloor;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.azt.api.service.ConfigService;
import com.azt.model.page.Pagination;
import com.azt.utils.RequestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.azt.api.dto.hot.ProShopFloorExt;
import com.azt.api.dto.item.ProductExt;
import com.azt.api.pojo.Category;
import com.azt.api.pojo.ProShopCompany;
import com.azt.api.pojo.ProShopFloor;
import com.azt.api.pojo.ProShopProduct;
import com.azt.api.service.ProCategoryService;
import com.azt.api.service.ProShopFloorService;
import com.azt.model.AjaxJson;


@Controller
@RequestMapping("/shop")
public class ProShopFloorAction{
   @Autowired
   private ProShopFloorService proShopFloorService;
   @Autowired
	private ProCategoryService proCategoryService;
   @Autowired
   private ConfigService configService;

   //查询所有
   @RequestMapping(value="/firstshop_list.do")
   public String shopFloorList(HttpServletRequest request,HttpServletRequest response,ModelMap map){
	   List<ProShopFloorExt> proShopFloorList=proShopFloorService.getProShopFloorList();
	   map.addAttribute("proShopFloorList",proShopFloorList);
	   return "shopfloor/shoplist";
   }
   //新增返回jsp页面
   @RequestMapping(value="/addfloor.do")
   public String addFloor(ModelMap map){
       List<ProShopFloorExt>shopFloorList=proShopFloorService.getProShopFloorList();
       if(shopFloorList.size()>=10){
           map.addAttribute("canAdd", false);
       }
	   //得到数据库里的楼层集合
	   List<Integer> getshopfloor= proShopFloorService.getShopFloor();
	   //总共10层楼层，可变
	   List<Integer> getallfloor=ProShopFloor.getMaxFloors();
	   getallfloor.removeAll(getshopfloor);
	   map.addAttribute("getallfloor",getallfloor);
       List<Category> topCategoryList = proCategoryService.findTopCategory();
       map.addAttribute("topCategoryList", topCategoryList);
       String webSite = configService.getConfigValueByCode("WEB_SITE");
       map.addAttribute("webSite", webSite);
	   return "shopfloor/addfloor";
   }

    @ResponseBody
    @RequestMapping(value="/chooseproduct.do")
    public AjaxJson getChooseProduct(Integer pageNo, HttpServletRequest request) {
        AjaxJson aj = new AjaxJson();
        try {
            Map<String, String> searchMap = RequestUtils.getSearchMap(request);
            searchMap.put("so_cusrole", "2");
            Pagination pagination = proShopFloorService.findCenProduct(pageNo == null ? 1 : pageNo, 6, searchMap);
            Map<String, Object> sources = new HashMap<>();
            sources.put("productList", pagination.getList());
            sources.put("pageNo", pagination.getPageNo());
            sources.put("totalPage", pagination.getTotalPage());
            sources.put("so_companyname", searchMap.get("so_companyname"));
            aj.setAttributes(sources);
        } catch (Exception e) {
            e.printStackTrace();
            aj = AjaxJson.error("请求失败");
        }
        return aj;
    }

   //根据ID删除对象
   @RequestMapping(value="/deletefloor.do")
   @ResponseBody
   public AjaxJson ajaxDelete(HttpServletRequest request,HttpServletResponse response,ProShopFloor psf){
	   AjaxJson aj=new AjaxJson();
	   proShopFloorService.deleteById(psf.getId());
	   return aj;
   }
   
   //提交后台新增
   @RequestMapping(value="/insert.do")
   @ResponseBody
   public AjaxJson ajaxInsert(HttpServletRequest req,HttpServletResponse res,ModelMap map,ProShopFloor shopFloor){
       AjaxJson aj=new AjaxJson();
       proShopFloorService.insert(shopFloor);
       return aj;
   }
   
   //修改返回jsp页面
   @RequestMapping(value="/updatefloor.do")
   public String updateFloor(ModelMap map,ProShopFloor shopFloor){
	   ProShopFloor ps=proShopFloorService.selectById(shopFloor.getId());
	   Integer categoryId=ps.getCategoryId();
	   Category category=proCategoryService.getCategorysById(categoryId);
	   ps.setCategoryName(category.getName());
	   map.addAttribute("ps",ps);
	   List<Integer> getallfloor=ProShopFloor.getMaxFloors();
	   map.addAttribute("getallfloor", getallfloor);
	   //查询所有一级的类目
       List<Category> topCategoryList = proCategoryService.findTopCategory();
       map.addAttribute("topCategoryList", topCategoryList);
       String webSite = configService.getConfigValueByCode("WEB_SITE");
        map.addAttribute("webSite", webSite);
       return "shopfloor/updatefloor";
   }
   
   //提交后台编辑
   @RequestMapping(value="/editfloor.do")
   @ResponseBody
   public AjaxJson editFloor(HttpServletRequest request,HttpServletResponse response,ProShopFloor shopFloor){
	   AjaxJson aj=new AjaxJson();
	   proShopFloorService.editFloor(shopFloor);
	   return aj;
   }
   
}
