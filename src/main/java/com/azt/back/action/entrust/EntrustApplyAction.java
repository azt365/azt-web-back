package com.azt.back.action.entrust;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.azt.api.pojo.Admin;
import com.azt.api.pojo.EntrustApply;
import com.azt.api.service.EntrustApplyService;
import com.azt.back.util.ContextUtils;
import com.azt.back.util.Globals;
import com.azt.model.AjaxJson;
import com.azt.model.page.Pagination;
import com.azt.utils.RequestUtils;

@Controller
@RequestMapping("entrust")
public class EntrustApplyAction {

	@Autowired
	private EntrustApplyService entrustApplyService;

	// @Autowired
	// private AlertMsgService alertMsgService;
	/**
	 * Logger for this class
	 */
	protected final Logger logger = LoggerFactory.getLogger(this.getClass());

	@RequestMapping("/v_list.do")
	public String list(HttpServletRequest request, HttpServletResponse response, ModelMap model, Integer pageNo) {

		Map<String, String> searchMap = RequestUtils.getSearchMap(request);
		Pagination pagination = entrustApplyService.getEntrustApplyPage(pageNo == null ? 1 : pageNo, Globals.PAGE_SIZE,
				searchMap);
		model.put("pagination", pagination);
		RequestUtils.MapToModelMap(searchMap, request);

		return "entrust/entrust_apply_list";

	}

	@RequestMapping("remark.do")
	@ResponseBody
	public AjaxJson remark(@RequestParam("id") Integer id, @RequestParam("memo") String memo) {
		entrustApplyService.updateMemo(id, memo);
		return AjaxJson.success();
	}

	@RequestMapping("/o_changestate.do")
	@ResponseBody
	public AjaxJson changestate(HttpServletRequest request, HttpServletResponse response, ModelMap model) {
		Integer state = RequestUtils.getInteger(request, "state");
		Integer entrustid = RequestUtils.getInteger(request, "entrustid");

		if (state != null) {
			EntrustApply entrust = entrustApplyService.getEntrustApplyByid(entrustid);
			entrust.setState(state);
			entrust.setAdminId(ContextUtils.getAdmin().getId());
			entrustApplyService.updateEntrustApply(entrust);
//			Integer userid = entrust.getUserId();
//			if (state == 1) {// 审核通过
//				sendMsg(userid, "金融服务通知", "您的金融服务企业资质审核已经通过，我们的工作人员将在7个工作日内给您电话联系");
//			} else if (state == 2) {// 审核未通过
//				sendMsg(userid, "金融服务通知", "很抱歉,您的金融服务企业资质没有通过审核，详细情况可联系安装通客服人员。");
//			}

		}
		return AjaxJson.success();
	}
	
	@RequestMapping("/o_delete.do")
	@ResponseBody
	public AjaxJson ajaxDel(HttpServletRequest request, HttpServletResponse response, ModelMap model,EntrustApply apply) {
        Integer id = apply.getId();
        if (id != null && !"".equals(id))
        {
        	entrustApplyService.deleteEntrustApplyById(id);
        }
        return AjaxJson.success();
	}

}
