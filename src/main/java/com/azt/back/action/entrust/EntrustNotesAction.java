package com.azt.back.action.entrust;

import com.azt.api.pojo.EntrustNotes;
import com.azt.api.service.EntrustNotesService;
import com.azt.back.action.BaseAction;
import com.azt.back.util.Globals;
import com.azt.model.AjaxJson;
import com.azt.model.page.Pagination;
import com.azt.utils.CommonUtil;
import com.azt.utils.RequestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Map;

/**
 *
 * Created by LiQZ on 2017/4/19.
 */
@Controller
@RequestMapping("/entrust/notes")
public class EntrustNotesAction extends BaseAction {

    @Autowired
    private EntrustNotesService entrustNotesService;

    @RequestMapping("list.do")
    public String list(Model model) {
        Map searchMap = CommonUtil.getParameterMap(request);
        int pageNo = getParaToInt("pageNo", 1);
        Pagination pagination = entrustNotesService.listPage(pageNo, Globals.PAGE_SIZE, searchMap);
        model.addAttribute("pagination", pagination);
        RequestUtils.MapToModelMap(searchMap, request);
        return "entrust/notes/list";
    }

    /**
     * 增加编辑都是这个页面
     */
    @RequestMapping("edit.do")
    public String edit(@RequestParam(value = "id", required = false) Integer id, Model model) {
        if (id != null) {
            EntrustNotes note = entrustNotesService.get(id);
            model.addAttribute("note", note);
        }
        return "entrust/notes/edit";
    }

    @ResponseBody
    @RequestMapping("save.do")
    public AjaxJson save(EntrustNotes note) {
        entrustNotesService.save(note);
        return AjaxJson.success();
    }

    @ResponseBody
    @RequestMapping("delete.do")
    public AjaxJson delete(Integer id) {
        entrustNotesService.delete(id);
        return AjaxJson.success();
    }

    @ResponseBody
    @RequestMapping("get.do")
    public AjaxJson get(Integer id) {
        EntrustNotes note = entrustNotesService.get(id);
        return AjaxJson.success().setObj(note);
    }

}
