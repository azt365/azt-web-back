package com.azt.back.action.returnvisit;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.azt.api.pojo.ReturnVisit;
import com.azt.api.service.ReturnVisitService;
import com.azt.back.util.ContextUtils;
import com.azt.back.util.Globals;
import com.azt.model.AjaxJson;
import com.azt.model.page.Pagination;
import com.azt.utils.RequestUtils;

/**
 * @ClassName: ReturnVisitAction
 * @Description: TODO(客户回访)
 * @author zhoutianchi
 * @date 2017年1月5日 下午3:07:36
 * 
 */
@Controller
@RequestMapping("/returnvisit")
public class ReturnVisitAction {
	@Autowired
	private ReturnVisitService returnVisitService;

	@RequestMapping(value = "/v_list.do")
	public String returnVisitList(HttpServletRequest request, Integer pageNo, HttpServletResponse response,
			ModelMap model) {
		Integer companyid = RequestUtils.getInteger(request, "so_companyid");
		Integer userid = RequestUtils.getInteger(request, "so_userid");
		Integer type = RequestUtils.getInteger(request, "so_type");
		model.put("companyid", companyid);
		model.put("userid", userid);
		model.put("type", type);
		Map<String, String> searchMap = RequestUtils.getSearchMap(request);
		
		Pagination pagination = returnVisitService.getReturnVisitPage(pageNo == null ? 1 : pageNo, Globals.PAGE_SIZE,
				searchMap);
		model.put("pagination2", pagination);

		RequestUtils.MapToModelMap(searchMap, request);
		return "returnvisit/returnvisit_list";
	}

	@RequestMapping(value = "/v_add.do")
	public String addReturnVisit(HttpServletRequest request, HttpServletResponse response, ModelMap model) {
		return "returnvisit/returnvisit_add";
	}

	@RequestMapping(value = "/v_edit.do")
	public String editReturnVisit(HttpServletRequest request, HttpServletResponse response, ModelMap model) {
		Map<String, String> searchMap = RequestUtils.getSearchMap(request);
		Integer id = Integer.parseInt(searchMap.get("so_id"));
		ReturnVisit returnvisit = returnVisitService.queryById(id);
		model.put("returnvisit", returnvisit);

		return "returnvisit/returnvisit_edit";
	}

	@RequestMapping(value = "/ajax_del.do", method = RequestMethod.GET)
	@ResponseBody
	public AjaxJson ajaxDel(ReturnVisit returnvisit, HttpServletRequest request, HttpServletResponse response,
			ModelMap model) {
		Integer id = returnvisit.getId();
		if (id != null && !"".equals(id)) {
			returnVisitService.deleteAllById(id);
		}
		return AjaxJson.success();
	}

	@RequestMapping(value = "/ajax_save.do", method = RequestMethod.POST)
	@ResponseBody
	public AjaxJson saveFriendlyLink(ReturnVisit returnVisit) {
		returnVisit.setAdminId(ContextUtils.getAdmin().getId());
		returnVisitService.saveMsg(returnVisit);

		return AjaxJson.success();
	}

	@RequestMapping(value = "/ajax_edit.do", method = RequestMethod.POST)
	@ResponseBody
	public AjaxJson saveEdit(ReturnVisit returnvisit) {
		returnvisit.setAdminId(ContextUtils.getAdmin().getId());
		returnVisitService.saveEdit(returnvisit);

		return AjaxJson.success();
	}
}
