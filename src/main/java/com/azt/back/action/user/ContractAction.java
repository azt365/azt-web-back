package com.azt.back.action.user;

import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.azt.api.pojo.Contract;
import com.azt.api.pojo.Dictionary;
import com.azt.api.service.ContractService;
import com.azt.api.service.DictionaryService;
import com.azt.back.util.Globals;

import com.azt.model.AjaxJson;
import com.azt.model.page.Pagination;
import com.azt.utils.RequestUtils;

/**
 * @ClassName: ContractAction
 * @Description: TODO(合同模板管理)
 * @author zhoutianchi
 * @date 2016年8月4日 下午3:07:36
 * 
 */
@Controller
@RequestMapping("/contract")
public class ContractAction {
	@Autowired
	private ContractService contractService;

	@Autowired
	private DictionaryService dictionaryService;

	/**
	 * 列表页面
	 * 
	 * @author 周天驰
	 * @param request
	 * @param pageNo
	 * @param response
	 * @param model
	 * @return String
	 * @see [类、类#方法、类#成员]
	 */
	@RequestMapping(value = "/v_list.do")
	public String contractList(HttpServletRequest request, Integer pageNo, HttpServletResponse response,
			ModelMap model) {
		Map<String, String> searchMap = RequestUtils.getSearchMap(request);
		Pagination pagination = contractService.getContractPageBy(pageNo == null ? 1 : pageNo, Globals.PAGE_SIZE,
				searchMap);
		model.put("pagination", pagination);

		List<Dictionary> dictionarys = dictionaryService.getDictionByParentCode("contract");
		model.put("dictionarys", dictionarys);

		RequestUtils.MapToModelMap(searchMap, request);
		return "contract/contract_list";
	}

	/**
	 * 根据id删除
	 * 
	 * @author 周天驰
	 * @param contract
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 * @see [类、类#方法、类#成员]
	 */
	@RequestMapping(value = "/ajax_del.do", method = RequestMethod.GET)
	@ResponseBody
	public AjaxJson ajaxDel(Contract contract, HttpServletRequest request, HttpServletResponse response,
			ModelMap model) {
		Integer id = contract.getId();
		if (id != null && !"".equals(id)) {
			// 删除父子级
			contractService.deleteAllById(id);
		}
		return AjaxJson.success();
	}

	/**
	 * 新增页面
	 * 
	 * @author 周天驰
	 * @param request
	 * @param response
	 * @return String
	 * @see [类、类#方法、类#成员]
	 */
	@RequestMapping(value = "/v_add.do")
	public String addContract(HttpServletRequest request, HttpServletResponse response, ModelMap model)

	{
		List<Dictionary> dictionarys = dictionaryService.getDictionByParentCode("contract");
		model.put("dictionarys", dictionarys);
		return "contract/contract_add";
	}

	/**
	 * 保存新增
	 * 
	 * @author 周天驰
	 * @param contract
	 * @return boolean
	 * @see [类、类#方法、类#成员]
	 */
	@RequestMapping(value = "/o_add.do")
	public String saveContract(Contract contract) {
		contract.setState(1);
		contract.setCreatetime(new Date());

		contractService.saveContract(contract);
		return "redirect:/contract/v_list.do";
	}

	/**
	 * 跳转编辑页
	 * 
	 * @author 周天驰
	 * @param request
	 * @param response
	 * @param model
	 * @return boolean
	 * @see [类、类#方法、类#成员]
	 */
	@RequestMapping(value = "/v_edit.do")
	public String editContract(HttpServletRequest request, HttpServletResponse response, ModelMap model) {
		Map<String, String> searchMap = RequestUtils.getSearchMap(request);
		Integer id = Integer.parseInt(searchMap.get("so_id"));
		Contract contract = contractService.queryById(id);

		model.put("contract", contract);

		List<Dictionary> dictionarys = dictionaryService.getDictionByParentCode("contract");
		model.put("dictionarys", dictionarys);

		return "contract/contract_edit";
	}

	/**
	 * 保存编辑
	 * 
	 * @author 周天驰
	 * @param contract
	 * @return boolean
	 * @see [类、类#方法、类#成员]
	 */
	@RequestMapping(value = "/o_edit.do")
	@ResponseBody
	public String saveEdit(@RequestBody Contract contract) {
		contractService.saveEdit(contract);
		return null;
	}
}
