package com.azt.back.action.user;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.azt.api.pojo.Role;
import com.azt.api.pojo.User;
import com.azt.api.service.UserPermissionService;
import com.azt.api.service.UserService;
import com.azt.back.util.Globals;
import com.azt.model.AjaxJson;
import com.azt.model.page.Pagination;
import com.azt.utils.BeanUtils;
import com.azt.utils.RequestUtils;

/**
 * 后台用户管理
 * 
 * @author 张栋 2016年4月26日下午5:49:42
 */
@Controller
@RequestMapping("/user")
public class UserAction {

	@Autowired
	private UserService userService;
	@Autowired
	private UserPermissionService userPermissionService;

	@RequestMapping("/v_list.do")
	public String list(HttpServletRequest request, Integer pageNo, HttpServletResponse response, ModelMap model) {

		Map<String, String> searchMap = RequestUtils.getSearchMap(request);

		Pagination pagination = userService.getUserPageBy(pageNo == null ? 1 : pageNo, Globals.PAGE_SIZE, searchMap);
		model.put("pagination", pagination);

		RequestUtils.MapToModelMap(searchMap, request);
		return "user/user_list";
	}

	@RequestMapping("/v_edit.do")
	public String v_edit(HttpServletRequest request, HttpServletResponse response, ModelMap model) {
		Integer userid = RequestUtils.getInteger(request, "userid");
		User user = userService.getUserById(userid);
		if (user != null && user.getCompanyId() != null) {
		}
		List<Role> allDefaultRole = userPermissionService.getAllRole();
		model.put("user", user);
		model.put("allRole", allDefaultRole);
		return "user/user_edit";
	}

	/**
	 * 编辑提交
	 */
	@RequestMapping("/o_save.do")
	@ResponseBody
	public AjaxJson o_save(HttpServletRequest request, HttpServletResponse response, ModelMap model, User user) {
		// 角色
		if (user != null && user.getId() != null) {
			User old = userService.getUserById(user.getId());
			// 判断验证
			if(StringUtils.isNotBlank(user.getMobile())){
				User userByMobile = userService.getUserByMobile(user.getMobile().trim());
				if (userByMobile != null && !userByMobile.getId().equals(old.getId())) {
					return AjaxJson.error("保存失败，号码已存在");
				}else{
					user.setMobile(user.getMobile().trim());
				}
			}
			
			if(StringUtils.isNotBlank(user.getEmail())){
				User userByEmail = userService.getUserByEmail(user.getEmail().trim());
				if (userByEmail != null && !userByEmail.getId().equals(old.getId())) {
					return AjaxJson.error("保存失败，邮箱已存在");
				}else{
					user.setEmail(user.getEmail().trim());
				}
			}
			
			BeanUtils.copyBeanNotNull2Bean(user, old);

			userService.saveOrUpdate(old);
			return AjaxJson.success();
		} else {
			return AjaxJson.error("更新失败");
		}
	}

	/**
	 * 重置密码提交
	 */
	@RequestMapping("/o_resetpwd.do")
	@ResponseBody
	public AjaxJson o_resetpwd(HttpServletRequest request, HttpServletResponse response, ModelMap model) {
		Integer userid = RequestUtils.getInteger(request, "userid");
		String newpassword = RequestUtils.getString(request, "newpassword", "123456");
		userService.resetPassword(userid, newpassword);
		return AjaxJson.success("重置成功！");
	}

	/**
	 * 删除用户(软删除）
	 */
	@RequestMapping(value = "/ajax_del.do", method = RequestMethod.GET)
	@ResponseBody
	public AjaxJson ajaxDel(User user, HttpServletRequest request, HttpServletResponse response, ModelMap model) {
		Integer id = user.getId();
		if (id != null && !"".equals(id)) {
			userService.deleteById(id);
		}
		return AjaxJson.success("删除成功！");
	}

	/**
	 * 启用/禁用
	 */
	@RequestMapping(value = "/ajax_disable.do", method = RequestMethod.GET)
	@ResponseBody
	public AjaxJson ajaxDisable(User user, HttpServletRequest request, HttpServletResponse response, ModelMap model) {
		Integer id = user.getId();
		if (id != null && !"".equals(id)) {
			userService.toggleUserDisabledById(id);
		}
		return AjaxJson.success("操作成功！");
	}

	/*
	 * @RequestMapping("/o_deptrole.do")
	 * 
	 * @ResponseBody public JSONArray o_deptrole(HttpServletRequest request,
	 * HttpServletResponse response,Integer userid,ModelMap model){ JSONArray
	 * jsonarray=new JSONArray(); JSONObject jsonobj=new JSONObject();
	 * //根据userid查询 userid=1970; if (userid != null) { User user =
	 * userService.getUserById(userid); model.put("user", user);
	 * //根据companyid查询部门 List<Dept> depts =
	 * userPermissionService.getDeptByCompanyId(user.getCompanyId());
	 * if(depts!=null && depts.size()>0){ for(Dept dep:depts){
	 * jsonobj.put("value", dep.getId()); jsonobj.put("name", dep.getName());
	 * List<Role> roles = userPermissionService.getRolesByDeptId(dep.getId());
	 * if(roles!=null && roles.size()>0){ for(Role rol:roles){ JSONArray
	 * jsonarray1=new JSONArray(); JSONArray jsonarray2=new JSONArray();
	 * JSONObject jsonobj1=new JSONObject();
	 * 
	 * jsonobj1.put("value", rol.getId()); jsonobj1.put("name", rol.getName());
	 * JSONObject jsonobj2=new JSONObject(); jsonobj2.put("value", 1);
	 * jsonobj2.put("name", "采购商"); JSONObject jsonobj3=new JSONObject();
	 * jsonobj3.put("value", 2); jsonobj3.put("name", "供应商");
	 * jsonarray2.add(jsonobj2); jsonarray2.add(jsonobj3); jsonobj1.put("sub",
	 * jsonarray2); jsonarray1.add(jsonobj1); jsonobj.put("sub", jsonarray1); }
	 * } } } } jsonarray.add(0, jsonobj); return jsonarray; }
	 */

}
