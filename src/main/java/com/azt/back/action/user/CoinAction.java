package com.azt.back.action.user;

import com.azt.api.pojo.*;
import com.azt.api.service.CoinService;
import com.azt.api.service.CompanyService;
import com.azt.api.service.DictionaryService;
import com.azt.back.action.BaseAction;
import com.azt.back.util.ContextUtils;
import com.azt.back.util.Globals;
import com.azt.model.AjaxJson;
import com.azt.model.page.Pagination;
import com.azt.utils.RequestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/coin")
public class CoinAction extends BaseAction {
		
	 @Autowired
     private CoinService coinService;
	 
	 @Autowired
	 private CompanyService companyService;
	 
	 @Autowired
	 private DictionaryService dictionaryService;
	 
	 @RequestMapping("/coin_list.do")
     public String selectCoinList(Model model, HttpServletRequest request, Integer pageNo) {
        Map<String, String> searchMap = RequestUtils.getSearchMap(request);
        Pagination pagination = coinService.coinManagerList(pageNo == null ? 1 : pageNo, Globals.PAGE_SIZE, searchMap);
        model.addAttribute("pagination", pagination);
        model.addAttribute("usercoin", coinService.useAnNouseCoin(null).get("usecoin"));
        model.addAttribute("nousercoin", coinService.useAnNouseCoin(null).get("nousercoin"));
        List<Dictionary> identityList= dictionaryService.getDictionByParentCode("cpIdentity");
        Map<String, String> identityMaps=new HashMap<>();
        for (int i = 0; i < identityList.size(); i++) {
			identityMaps.put(identityList.get(i).getCode(), identityList.get(i).getName());
		}
        model.addAttribute("identityMap", identityMaps);
        RequestUtils.MapToModelMap(searchMap, request);
        return "coin/coin_list";
     }
	 
	 @RequestMapping("/coinRecord_list.do")
	 public String selectCoinRecordList(Model model, HttpServletRequest request, Integer pageNo){
		 Map<String, String> searchMap = RequestUtils.getSearchMap(request);
         Pagination pagination = coinService.coinRecordList(pageNo == null ? 1 : pageNo, Globals.PAGE_SIZE, searchMap);
         model.addAttribute("pagination", pagination);
         RequestUtils.MapToModelMap(searchMap, request);
         return "coin/coinRecord_list";
	 }
	 
	 @RequestMapping("/CompanyCoinRecord_list.do")
	 public String selectCompanyCoinRecordList(Model model, HttpServletRequest request, Integer pageNo){
		 Map<String, String> searchMap = RequestUtils.getSearchMap(request);
         Pagination pagination = coinService.coinRecordList(pageNo == null ? 1 : pageNo, Globals.PAGE_SIZE, searchMap);
         Company c=companyService.showComDetails(searchMap.get("so_companyId"));
		 CompanyInfo companyInfo = companyService.getCompanyInfoByCompanyId(c.getId());
         model.addAttribute("pagination", pagination);
         model.addAttribute("company", c);
		 model.addAttribute("companyInfo", companyInfo);
         model.addAttribute("usercoin", coinService.useAnNouseCoin(searchMap.get("so_companyId")).get("usecoin"));
         model.addAttribute("nousercoin", coinService.useAnNouseCoin(searchMap.get("so_companyId")).get("nousercoin"));
         RequestUtils.MapToModelMap(searchMap, request);
         return "coin/companyCoinRecordList";
	 }
	 
	 @RequestMapping("/coinrule_list.do")
	 public String selectCoinRuleList(Model model, HttpServletRequest request, Integer pageNo){
		 Map<String, String> searchMap = RequestUtils.getSearchMap(request);
         Pagination pagination = coinService.coinRuleList(pageNo == null ? 1 : pageNo, Globals.PAGE_SIZE, searchMap);
         model.addAttribute("pagination", pagination);
         RequestUtils.MapToModelMap(searchMap, request);
         return "coin/coinRule_listNew";
	 }
	 
	 @RequestMapping("/coinruleAdd.do")
	 public String coinRuleAdd(Model model){
		 List<Dictionary> identityList= dictionaryService.getDictionByParentCode("cpIdentity");
		 model.addAttribute("identitylist", identityList);
		 model.addAttribute("rule", new CoinRule());
		 return  "coin/coinRule_form";
	 }
	 
	 @RequestMapping("/coinruleEdit.do")
	 public String coinRuleEdit(Model model,String ruleCode){
		 //List<Dictionary> identityList= dictionaryService.getDictionByParentCode("cpIdentity");
		 //model.addAttribute("identitylist", identityList);
		 CoinRule cr=coinService.selectRuleBycode(ruleCode);
		 model.addAttribute("rule", cr);
		 return  "coin/coinRule_form";
	 }
	 
	 @RequestMapping("/coinRule_add.do")
	 @ResponseBody
	 public AjaxJson addCoinRule(CoinRule coinRule){
		 AjaxJson aj=new AjaxJson();
		 try {
			 coinRule.setRemoved(0);
			coinService.saveCoinRule(coinRule);
		 } catch (Exception e) {
			aj=AjaxJson.error("系统错误");
			e.printStackTrace();
		 }
		 return aj;
	 }
	 
	 @RequestMapping("/remove_coin.do")
	 @ResponseBody
	 public AjaxJson removeCoinRule(Integer id){
		 AjaxJson aj=new AjaxJson();
		 try {
			coinService.removeCoinRule(id);
		 } catch (Exception e) {
			aj=AjaxJson.error("系统错误");
			e.printStackTrace();
		 }
		 return aj;
	 }
	 
	 @RequestMapping("/validateRule.do")
	 @ResponseBody
	 public AjaxJson validateCode(String code){
		 AjaxJson aj=new AjaxJson();
		 try {
			 Integer ruleId = getParaToInt("ruleId");
			 CoinRule rule=coinService.selectRuleBycode(code);
			 if(rule != null) {
				 if(ruleId == null || ruleId != rule.getId().intValue()) {
					 return AjaxJson.error("code已存在");
				 }
			 }
		} catch (Exception e) {
			aj=AjaxJson.error("系统错误");
			e.printStackTrace();
		}
		return aj;
	 }
	 
	 @RequestMapping("/coinKouJian.do")
	 public String toCoinKouJian(Company company,Model model){
		 Company com=companyService.getCompanyById(company.getId());
		 Integer coins=coinService.getCoinByCompanyId(company.getId());
		 model.addAttribute("company", com);
		 model.addAttribute("coins", coins);
		 return "coin/coin_hand";
	 }
	 
	 @RequestMapping("/coin_dikou.do")
	 @ResponseBody
	 public AjaxJson doCoinKouJian(@RequestBody CoinRecord coinRecord){
		 AjaxJson aj=new AjaxJson();
		 try {
			 coinRecord.setOperateType(-1);
			 coinRecord.setOperateid(ContextUtils.getAdmin().getId());
			 coinService.adminChangeCoin(coinRecord);
		 } catch (Exception e) {
			aj=AjaxJson.error("系统异常");
			e.printStackTrace();
		 }
		 return aj;
	 }
}
