package com.azt.back.action.user;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.ArrayUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.azt.api.enums.UserFuncShowTypeEnum;
import com.azt.api.pojo.Authority;
import com.azt.api.pojo.UserFunction;
import com.azt.api.service.FunctionService;
import com.azt.model.AjaxJson;
import com.azt.model.easy.EasyTree;
import com.azt.utils.BeanUtils;
import com.azt.utils.CommonUtil;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

@Controller
@RequestMapping("/ufunc")
public class UserFuncAction {

	@Autowired
	private FunctionService ufuncService;
	
	@RequestMapping(value = "/v_function.do", method = RequestMethod.GET)
	public String functions(HttpServletRequest request,
			HttpServletResponse response, ModelMap model){
		return "admin/function/ufunc_list";
	}
		
	@RequestMapping(value="/ajax_tree.do")
	@ResponseBody
	public List<EasyTree> ajaxForTree(HttpServletRequest request) {
		return  ufuncService.getFuncTree();
	}
	
	@RequestMapping(value="/ajax_authorityTree.do")
	@ResponseBody
	public JSONArray ajaxForauthorityTree(HttpServletRequest request){
		String funcs = request.getParameter("funcs");
		String[] funcids = funcs == null ? null : funcs.split(",");
		List<UserFunction> allParentUfuncs = ufuncService.getAllParentFunction();
		return UfunctionjsonArray(allParentUfuncs,funcids);
	}
	
	/**
	 * @description 转换tree结果
	 * @param allParentUfuncs
	 * @param funcids
	 * @return
	 * @author 查恒  2016年8月11日上午11:33:00
	 */
	public JSONArray UfunctionjsonArray(List<UserFunction> allParentUfuncs,String[] funcids){
		JSONArray arr = new JSONArray();
		for (UserFunction uf : allParentUfuncs) {
			if(uf.getShowType()==UserFuncShowTypeEnum.LEFT.getValue()){
			JSONObject jsonForAttr = new JSONObject();
			jsonForAttr.put("parentId", uf.getParentId());
			JSONObject obj = new JSONObject();
			obj.put("id", uf.getId());
			obj.put("text", uf.getName());
			if (uf.getParentId() == 0) {
				obj.put("state", "closed");
			}
			if (ArrayUtils.contains(funcids,uf.getId().toString())) {
				obj.put("checked", true);
			}
			obj.put("attributes", jsonForAttr);

			List<UserFunction> children = uf.getChildren();
			if (children != null && children.size() != 0) {
				JSONArray atts = UfunctionjsonArray(uf.getChildren(), funcids);
				obj.put("children", atts);
			}
			arr.add(obj);
			}
		}
		return arr;
	}
	
	
	/**
	 * @description 根据角色ID得到它的所有权限
	 * @param request
	 * @param response
	 * @param roleId
	 * @return
	 * @author 查恒  2016年8月11日上午10:43:08
	 */
	@RequestMapping(value="/ajax_Functions.do" , method = RequestMethod.POST)
	@ResponseBody
	public JSONObject getUrolFunc(HttpServletRequest request , HttpServletResponse response , Integer roleId){
		JSONObject json = new JSONObject();
		if(roleId != null){
			try{
				List<Authority> uauthors = this.ufuncService.getAuthorityByRoleId(roleId);
				StringBuffer sb = new StringBuffer();
				for(Authority ua : uauthors){
					UserFunction funcById = ufuncService.getFuncById(ua.getFunctionId());
					if(funcById!=null){
						sb.append(funcById.getId()+",");
					}
				}
				json.put("funcs" , sb.toString());
				json.put("success" , true);
				json.put("msg", "获取成功");
			}catch (Exception e){
				e.printStackTrace();
				json.put("success" , false);
				json.put("msg", "编辑失败,未知错误");
			}
		}else{
			json.put("success" , false);
			json.put("msg", "编辑失败,记录不存在");
		}
		return json;
	}

	@RequestMapping(value="/ajax_add.do")
	@ResponseBody
	public AjaxJson ajaxAdd(UserFunction ufunc, HttpServletRequest request, HttpServletResponse response, ModelMap model) {
		AjaxJson aj = new AjaxJson();
		Integer id = ufunc.getId();
		if(id != null && !"".equals(id)){
			UserFunction _ufunc = ufuncService.getFuncById(id);
			try {
				BeanUtils.copyBean2Bean(_ufunc, ufunc);
				ufuncService.saveOrUpdate(_ufunc);
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else {
			ufunc.setRemoved(0);//默认不删除
			ufuncService.saveOrUpdate(ufunc);
		}
		return aj;
	}
	
	@RequestMapping(value="/ajax_del.do")
	@ResponseBody
	public AjaxJson ajaxDel(HttpServletRequest request, HttpServletResponse response, ModelMap model){
		AjaxJson aj = new AjaxJson();
		Integer id = CommonUtil.safeToInteger(request.getParameter("id"));
		if(id != null) {
			UserFunction ufunc = ufuncService.getFuncById(id);
			if(ufunc != null) {
				ufunc.setRemoved(1);//删除
				ufuncService.saveOrUpdate(ufunc);
			}
		}
		return aj;
	}
	
}
