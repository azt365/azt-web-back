package com.azt.back.action.user;

import com.azt.api.pojo.IntegralRule;
import com.azt.api.service.IntegralService;
import com.azt.back.util.Globals;
import com.azt.model.AjaxJson;
import com.azt.model.page.Pagination;
import com.azt.utils.RequestUtils;
import com.xiaoleilu.hutool.util.CollectionUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/integral")
public class IntegralAction {
		
	 @Autowired
	 private IntegralService integralService;
	 
	 @RequestMapping("/i_record_list.do")
	 public String selectRecordList(Model model, HttpServletRequest request, Integer pageNo) {
        Map<String, String> searchMap = RequestUtils.getSearchMap(request);
        Pagination pagination = integralService.selectIntegralList(pageNo == null ? 1 : pageNo, Globals.PAGE_SIZE, searchMap);
        model.addAttribute("pagination", pagination);
        RequestUtils.MapToModelMap(searchMap, request);
        return "integral/integral_record_list";
	 }
	 
	 @RequestMapping("/i_recordrule_list.do")
	 public String selectRecordRuleList(Model model, HttpServletRequest request, Integer pageNo){
		Map<String, String> searchMap = RequestUtils.getSearchMap(request);
        Pagination pagination = integralService.selectIntegralRuleList(pageNo == null ? 1 : pageNo, Globals.PAGE_SIZE, searchMap);
        model.addAttribute("pagination", pagination);
        RequestUtils.MapToModelMap(searchMap, request);
        return "integral/integral_rule";
	 }
	 
	 @RequestMapping("/queryEditrecord.do")
	 @ResponseBody
	 public AjaxJson queryEditrecord(Integer id){
		 AjaxJson aj=new AjaxJson();
		 try {
			IntegralRule ir=integralService.queryRuleById(id);
			aj.setObj(ir);
		 } catch (Exception e) {
			e.printStackTrace();
			aj=AjaxJson.error("系统出错");
		 }
		 return aj;
	 }
	 
	 @RequestMapping("/editOradd.do")
	 @ResponseBody
	 public AjaxJson doeditOradd(IntegralRule rule){
		 AjaxJson aj=new AjaxJson();
		 try {
			if(rule.getId()==null){
				rule.setCreatetime(new Date());
				integralService.addIngralRule(rule);
			}else{
				rule.setUpdatetime(new Date());
				integralService.editIngralRule(rule);
			}
		 }catch (Exception e) {
			e.printStackTrace();
			aj=AjaxJson.error("系统异常");
		 }
		 return aj;
	 }
	 
	 @RequestMapping("/deleteRule")
	 @ResponseBody
	 public AjaxJson deleteRule(String ruleCode){
		 AjaxJson aj=new AjaxJson();
		 try {
			List<IntegralRule> childRuleList = integralService.findChildRule(ruleCode);
			 if(CollectionUtil.isNotEmpty(childRuleList)) {
				 aj = AjaxJson.error("该规则下有子规则，请先删除子规则");
			 } else {
				 integralService.deleteIngralRule(ruleCode);
			 }
		} catch (Exception e) {
			e.printStackTrace();
			aj=AjaxJson.error("系统异常");
		}
		return aj;
	 }
}
