package com.azt.back.action.user;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.azt.api.pojo.WordSql;
import com.azt.api.service.UseForDevelopService;
import com.azt.back.util.Globals;
import com.azt.model.AjaxJson;
import com.azt.model.OSSServer;
import com.azt.model.page.Pagination;
import com.azt.utils.BeanUtils;
import com.azt.utils.OSSFileUtils;

@Controller
@RequestMapping("/usefordevelopment")
public class UseForDevelopAction {
	@Autowired
	private UseForDevelopService usefordevelopservice;
	@RequestMapping("/v_maintenance.do")
	public String developPanel(HttpServletRequest request,HttpServletResponse response,Model model) {
		
		return "usefordevelop/v_maintenance";
	}

	
	@RequestMapping("/v_analyzeproducttype.do")
	public String producttype(HttpServletRequest request,HttpServletResponse response,Model model){
		return "usefordevelop/d_analyzeproduct";
	}
	
	/**
	 * 解析产品分类excel文件
	 * @author 查恒  2016年7月11日上午11:09:01
	 */
	@RequestMapping("/o_analyzeproductfile.do")
	public void analyProductFile(@RequestParam(value = "excelFile") MultipartFile excelFile, HttpServletRequest request,
			HttpServletResponse response) {
		String filename = excelFile.getOriginalFilename().substring(0,excelFile.getOriginalFilename().lastIndexOf("."));
		String suffix = excelFile.getOriginalFilename().substring(excelFile.getOriginalFilename().lastIndexOf(".") + 1);
		List<Map<String, Integer>> fcatelist = new ArrayList<Map<String, Integer>>();
		List<Map<String, Integer>> scatelist = new ArrayList<Map<String, Integer>>();
		try {
			String uploadFile = OSSFileUtils.uploadFile(OSSServer.getImgServer(), excelFile.getInputStream(),
					excelFile.getSize(), "admin", URLEncoder.encode(filename, "utf-8"), suffix);
			usefordevelopservice.analyProductType(fcatelist, scatelist, uploadFile);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**SQL描述**/
	
	@RequestMapping("/v_developsql.do")
	public String developsql(Integer pageNo, HttpServletRequest request,HttpServletResponse response,Model model){
		//分页查询
		Pagination sqlList = usefordevelopservice.getSqlList(pageNo == null ? 1 : pageNo,Globals.PAGE_SIZE);
		model.addAttribute("sqlList", sqlList);
		return "usefordevelop/v_developsql_list";
	}
	
	@ResponseBody
	@RequestMapping("/ajax_add.do")
	public AjaxJson adddevelopsql(HttpServletRequest request,HttpServletResponse response,WordSql wordsql){
		if(wordsql.getId()!=null){
			WordSql wordSql2 = usefordevelopservice.getWordSql(wordsql.getId());
			try {
				BeanUtils.copyBean2Bean(wordSql2, wordsql);
			} catch (Exception e) {
				e.printStackTrace();
			}
			usefordevelopservice.saveOrUpdate(wordSql2);
		}else{
			usefordevelopservice.saveOrUpdate(wordsql);
		}
		return AjaxJson.success();
	}
	
	
	//删除
	@ResponseBody
	@RequestMapping(value="/o_deldevelop.do")
	public AjaxJson delDevelop(HttpServletRequest request, HttpServletResponse response,Integer id){
		if(id!=null){
			WordSql wordSql = usefordevelopservice.getWordSql(id);
			usefordevelopservice.delWordSql(wordSql);
		}else{
			return AjaxJson.error("id不能为空！");
		}
		return AjaxJson.success();
	}
	
	//编辑和新增SQL弹框
	@RequestMapping(value="/v_addsql.do")
	public String dialogSql(HttpServletRequest request, HttpServletResponse response,Integer id,Model model){
		if(id!=null){
			WordSql wordsql = usefordevelopservice.getWordSql(id);
			model.addAttribute("wordsql", wordsql);
		}
		return "usefordevelop/v_developdialog";
	}
	
}
