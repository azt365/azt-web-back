package com.azt.back.action.admin;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.azt.api.pojo.BackRole;
import com.azt.api.pojo.RoleFunction;
import com.azt.api.service.RoleFunctionService;
import com.azt.api.service.RoleService;

import net.sf.json.JSONObject;

@Controller
public class RoleAction {
	
	@Autowired
	private RoleService roleManager;
	
	@Autowired
	private RoleFunctionService roleFunctionManager;
	
	
	@RequestMapping(value = "role/v_role.do" , method = RequestMethod.GET)
	public String roles(HttpServletRequest request , HttpServletResponse response){
		List<BackRole> rlist = this.roleManager.getAllRole();
		request.setAttribute("rlist", rlist);
		
		return "admin/role/role_list";
	}
	
	@RequestMapping(value = "role/ajax_add.do")
	@ResponseBody
	public JSONObject roleAdd(HttpServletRequest request , HttpServletResponse response , BackRole backRole){
		JSONObject  json = new JSONObject();
		String funcids = request.getParameter("funcids");
		try{
			if(backRole.getId() == null){//新建
				json.put("msg", "新建成功");
				backRole.setCreatetime(new Date());
				this.roleManager.save(backRole);//保存角色表
				
				//保存角色方法role_function表
				String[] funcs = funcids.split(",");
				for(int i = 0; i < funcs.length; i++){
					if( !"".equals(funcs[i])){
						RoleFunction rf = new RoleFunction();
						rf.setRoleId(backRole.getId());
						rf.setFunctionId(Integer.parseInt(funcs[i]));
						rf.setCreatetime(new Date());
						roleFunctionManager.save(rf);
					}
				}
				
			}else{//编辑
				json.put("msg", "编辑成功");
				BackRole r = this.roleManager.getRoleByid(backRole.getId());
				r.setUpdatetime(new Date());
				r.setRoleName(backRole.getRoleName());
				this.roleManager.update(backRole);
				List<RoleFunction> rflist = this.roleFunctionManager.getRFListByRoleId(r.getId());
				this.roleFunctionManager.delAllRoleFunction(rflist);
				
				//保存角色方法role_function表
				String[] funcs = funcids.split(",");
				for(int i = 0; i < funcs.length; i++){
					if( !"".equals(funcs[i])){
						RoleFunction rf = new RoleFunction();
						rf.setRoleId(r.getId());
						rf.setFunctionId(Integer.parseInt(funcs[i]));
						rf.setCreatetime(new Date());
						roleFunctionManager.save(rf);
					}
				}
			}
			json.put("success", true);
		}catch(Exception e){
			e.printStackTrace();
			json.put("success", false);
			json.put("msg", "操作失败");
		}
		
		return json;
	}
	
	@RequestMapping(value="role/ajax_del.do" , method = RequestMethod.POST)
	@ResponseBody
	public JSONObject del(HttpServletRequest request , HttpServletResponse response , BackRole backRole){
		JSONObject json = new JSONObject();
		if(backRole.getId() != null){
			try{
				//删除所有相关联表
				roleManager.deleteAllcorrelation(backRole);
				json.put("success" , true);
				json.put("msg", "删除成功");
			}catch (Exception e){
				e.printStackTrace();
				json.put("success" , false);
				json.put("msg", "删除失败");
			}
		}else{
			json.put("success" , false);
			json.put("msg", "删除失败");
		}
		return json;
	}
	
	@RequestMapping(value="role/ajax_Functions.do" , method = RequestMethod.POST)
	@ResponseBody
	public JSONObject getRol(HttpServletRequest request , HttpServletResponse response , BackRole backRole){
		JSONObject json = new JSONObject();
		if(backRole.getId() != null){
			try{
				List<RoleFunction> rflist = this.roleFunctionManager.getRFListByRoleId(backRole.getId());
				StringBuffer sb = new StringBuffer();
				for(RoleFunction rf : rflist){
					sb.append(rf.getFunctionId()+",");
				}
				json.put("funcs" , sb.toString());
				json.put("success" , true);
				json.put("msg", "获取funcs成功");
			}catch (Exception e){
				e.printStackTrace();
				json.put("success" , false);
				json.put("msg", "编辑失败,未知错误");
			}
		}else{
			json.put("success" , false);
			json.put("msg", "编辑失败,记录不存在");
		}
		return json;
	}
	
	
}
