package com.azt.back.action.admin;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.RandomUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.azt.api.pojo.Admin;
import com.azt.api.service.AdminService;
import com.azt.back.util.ContextUtils;

@Controller
@RequestMapping("/admin")
public class LoginAction {
	@Autowired
	private AdminService manager;

	private static Logger log = LoggerFactory.getLogger(LoginAction.class);


	@RequestMapping(value = "login.do", method = RequestMethod.GET)
	public String login2(HttpServletRequest request, HttpServletResponse response, ModelMap model) {

		Admin admin = ContextUtils.getAdmin();
		try {
			String message = request.getParameter("message");
			if (message != null) {
				message = URLDecoder.decode(message, "utf-8");
				model.put("message", message);
			}

		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		if (admin != null) {
			return "redirect:/frame/index.do";
		}

		return "admin/login2";
	}

	public String login(HttpServletRequest request, HttpServletResponse response, ModelMap model) {
		Admin admin = ContextUtils.getAdmin();
		try {
			String message = request.getParameter("message");
			if (message != null) {
				message = URLDecoder.decode(message, "utf-8");
				model.put("message", message);
			}

		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		if (admin != null) {
			return "redirect:/frame/index.do";
		}

		// 获取图片并随机排序,一次获取10张
		String realPath = request.getSession().getServletContext().getRealPath("/res/back/login/images/banner");
		File files = new File(realPath);
		String[] list = files.list();

		List<String> bannerimages =null;

		if (list.length > 5) {
			bannerimages = new ArrayList<String>(5);
			int i = 0;
			while (i < 5) {
				int nextInt = RandomUtils.nextInt(0,list.length);
				String imagename = list[nextInt];
				if (!bannerimages.contains(imagename)) {
					bannerimages.add(list[nextInt]);
					i++;
				}

			}
		}else{
			bannerimages = Arrays.asList(list);
		}
		model.put("banners", bannerimages);
		return "admin/login";
	}
	

	@RequestMapping(value = "/login.do", method = RequestMethod.POST)
	public String submit(String name, String password, HttpServletRequest request, HttpServletResponse response, ModelMap model) {
		Admin admin = manager.validateSubmit(name, password);
		if (admin == null) {
			String encode = null;
			try {
				encode = URLEncoder.encode("用户名或密码错误!", "utf-8");
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}
			model.put("message", encode);
		} else {

			ContextUtils.setAdmin(admin);
			List<String> functionPath = manager.functionPath(admin.getId());
			ContextUtils.setFunctions(functionPath);
			return "redirect:/frame/index.do";
		}
		return "redirect:/admin/login.do";
	}

	@RequestMapping(value = "/logout.do")
	public String logout(HttpServletRequest request, HttpServletResponse response) {
		Admin admin = ContextUtils.getAdmin();
		if (admin != null) {
			ContextUtils.getSession().invalidate();
		}
		return "redirect:/admin/login.do";
	}

	@RequestMapping(value = "/noperm.do")
	public String noperm(HttpServletRequest request, HttpServletResponse response) {
		return "admin/noperm";
	}

}
