package com.azt.back.action.admin;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import com.azt.utils.RequestUtils;

/**
 * 上传文件的页面
 * @author 张栋  2016年5月9日下午4:19:28
 */
@Controller
@RequestMapping("/uploader")
public class WebUploader {

	@RequestMapping("/v_file.do")
	public String list(HttpServletRequest request, HttpServletResponse response, ModelMap model) {
		//回调js名称
		String callback = RequestUtils.getString(request, "callback");
		model.put("callback", callback);

		//回调js，附加的参数
		String params = RequestUtils.getString(request, "params");
		model.put("params", params);


		//可以选多张图片
		boolean multiple = RequestUtils.getBoolean(request,  "multiple", false);
		model.put("multiple", multiple);
		
		
		//一次上传的数量
		Integer fileNumLimit = RequestUtils.getInteger(request, "fileNumLimit", null);
		if(fileNumLimit!=null)
		model.put("fileNumLimit", fileNumLimit);
		
		//文件允许的大小
		Integer fileSizeLimit = RequestUtils.getInteger(request, "fileSizeLimit", null);
		if(fileNumLimit!=null)
		model.put("fileSizeLimit", fileSizeLimit);

		//图片区别路径
		String userpath = RequestUtils.getString(request, "userpath");
		model.put("userpath", userpath);
		

		
		//允许的后缀
		String extensions = RequestUtils.getString(request, "extensions");
		model.put("extensions", extensions);
		
		return "template/uploadfile";
	}
}
