package com.azt.back.action.admin;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.azt.api.pojo.Admin;
import com.azt.api.pojo.AdminRole;
import com.azt.api.pojo.BackRole;
import com.azt.api.service.AdminService;
import com.azt.api.service.RoleService;
import com.azt.back.util.ContextUtils;
import com.azt.back.util.Globals;
import com.azt.model.AjaxJson;
import com.azt.utils.PasswordUtil;

/**
 * 管理员
 * 
 * @author 张栋 2016年4月7日下午4:29:03
 */
@Controller
public class AdminAction {
	@Autowired
	private AdminService manager;
	@Autowired
	private RoleService roleManager;

	@RequestMapping(value = "admin/v_admin.do", method = RequestMethod.GET)
	public String adminList(HttpServletRequest request, HttpServletResponse response, ModelMap model) {

		List<Admin> allAdmin = manager.getAllAdmin();
		List<BackRole> rlist = this.roleManager.getAllRole();

		model.addAttribute("admins", allAdmin);
		model.addAttribute("rlist", rlist);
		
		
		return "admin/admin_list";
	}

	// 增加与更新管理员
	@RequestMapping(value = "/admin/admin_add.do")
	@ResponseBody
	public AjaxJson ajaxAdd(Admin ad, HttpServletRequest request, HttpServletResponse response, ModelMap model) {
		AjaxJson aj = new AjaxJson();
		Integer id = ad.getId();
		// 如果为空，则为添加
		String password = ad.getPassword();
		if (id == null) {
			getEffect(ad);
			// 如果没有输入密码，则给系统默认密码
			if (password == null || "".equals(password.trim())) {
				password = Globals.PASSWORD;
			}
			newPassword(ad, password);
			manager.saveAdmin(ad);

		} else {// 否则为更新
			getEffect(ad);
			Admin oldadmin = manager.getAdminById(id);
			oldadmin.setName(ad.getName());

			// 更新时用户输入的密码为空时则不更新密码，否则要更新密码
			if (password != null && !"".equals(password.trim())) {
				newPassword(oldadmin, password);
			}

			manager.editAdmin(ad, oldadmin);

		}
		return aj;
	}

	/**
	 * 添加新密码
	 * 
	 * @author 张栋
	 * @param ad
	 * @return
	 * @see [类、类#方法、类#成员]
	 */
	private Admin newPassword(Admin ad, String password) {
		String randomSalt = PasswordUtil.getRandomSalt(6);
		ad.setSalt(randomSalt);
		String encodePassword = PasswordUtil.encodePassword(password, randomSalt);
		ad.setPassword(encodePassword);
		return ad;
	}

	/**
	 * 添加与编辑的时候会用,获取有效的角色
	 * 
	 * @author 张栋
	 * @param ad
	 * @return
	 * @see [类、类#方法、类#成员]
	 */
	private Admin getEffect(Admin ad) {
		List<AdminRole> newar = new ArrayList<AdminRole>();
		List<AdminRole> adminRoles = ad.getAdminRoles();
		for (AdminRole ar : adminRoles) {
			BackRole backRole = ar.getRole();
			if (backRole != null) {
				ar.setAdmin(ad);
				newar.add(ar);
			}
		}
		ad.setAdminRoles(newar);
		return ad;
	}

	@RequestMapping("/admin/edit_password.do")
	@ResponseBody
	public AjaxJson editPassword(Admin ad, HttpServletRequest request) {
		AjaxJson aj = new AjaxJson();
		String password = ad.getPassword();
		Admin admin = ContextUtils.getAdmin();
		if (password != null) {
			ad = manager.getAdminById(admin.getId());
			newPassword(ad, password);
			
			manager.saveOrUpdate(ad);
		}
		return aj;
	}

	// 删除管理员
	@RequestMapping(value = "/admin/admin_del.do")
	@ResponseBody
	public AjaxJson ajaxdel(Admin ad, HttpServletRequest request, HttpServletResponse response, ModelMap model) {
		AjaxJson aj = new AjaxJson();
		Integer id = ad.getId();
		if (id != null && !"".equals(id)) {
			manager.delAdmin(id);
		}
		return aj;
	}

	// 编辑管理员
	@RequestMapping(value = "admin/admin_save.do", method = RequestMethod.POST)
	@ResponseBody
	public AjaxJson ajaxSave(Admin ad, HttpServletRequest request, HttpServletResponse response, ModelMap model) {
		AjaxJson j = new AjaxJson();
		manager.saveAdmin(ad);
		return j;
	}
}
