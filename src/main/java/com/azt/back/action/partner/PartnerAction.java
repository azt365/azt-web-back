package com.azt.back.action.partner;

import com.azt.api.pojo.Partner;
import com.azt.api.service.PartnerService;
import com.azt.back.util.Globals;
import com.azt.model.AjaxJson;
import com.azt.model.page.Pagination;
import com.azt.utils.RequestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;

/**
 * Created by 叶青 on 2017/3/29.
 */
@Controller
@RequestMapping("/partner")
public class PartnerAction {
    @Autowired
    PartnerService partnerService;

    @RequestMapping(value="/partner_list.do")
    public String partnerList(HttpServletRequest req, HttpServletResponse res, ModelMap map,Integer pageNo){
        Map<String, String> searchMap = RequestUtils.getSearchMap(req);
        Pagination pagination=partnerService.partnerList(pageNo == null ? 1 : pageNo, Globals.PAGE_SIZE, searchMap);
        map.addAttribute("pagination",pagination);
        RequestUtils.MapToModelMap(searchMap, req);
        return "partner/partner_list";
    }

    @RequestMapping("/addauditOpinion.do")
    @ResponseBody
    public AjaxJson addAuditOpinion(Integer id, String auditOpinion){
        AjaxJson aj=new AjaxJson();
        partnerService.updateAuditOpinion(id, auditOpinion);
        return aj;
    }
}
