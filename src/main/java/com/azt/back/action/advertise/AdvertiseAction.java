package com.azt.back.action.advertise;

import com.azt.api.dto.IndexAd;
import com.azt.api.pojo.Advertise;
import com.azt.api.pojo.AdvertiseImg;
import com.azt.api.pojo.Dictionary;
import com.azt.api.service.AdvertiseService;
import com.azt.api.service.DictionaryService;
import com.azt.back.action.BaseAction;
import com.azt.back.util.Globals;
import com.azt.model.AjaxJson;
import com.azt.model.Ret;
import com.azt.model.page.Pagination;
import com.azt.utils.RequestUtils;
import com.xiaoleilu.hutool.date.DateUtil;
import com.xiaoleilu.hutool.util.CollectionUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @ClassName: AdvertiseAction
 * @Description: TODO(消息管理)
 * @author zhoutianchi
 * @date 2016年8月10日 下午3:07:36
 *       
 */
@Controller
@RequestMapping("/advertise")
public class AdvertiseAction extends BaseAction {
	@Autowired
	private AdvertiseService advertiseService;
	
    @Autowired
    private DictionaryService dictionaryService;
    
    
    /**
     * 列表页面
     * 
     * @author 周天驰
     * @param request
     * @param pageNo
     * @param response
     * @param model
     * @return String
     * @see [类、类#方法、类#成员]
     */
    @RequestMapping(value = "/v_list.do")
    public String advertiseList(HttpServletRequest request, Integer pageNo, HttpServletResponse response, ModelMap model)
    {
        Map<String, String> searchMap = RequestUtils.getSearchMap(request);
        Pagination pagination = advertiseService.getAdvertisePage(pageNo == null ? 1 : pageNo, Globals.PAGE_SIZE, searchMap);
        model.put("pagination", pagination);
        
        List<Dictionary> dictionarys = dictionaryService.getDictionByParentCode("advertiseType");
        model.put("dictionarys", dictionarys);
        
        RequestUtils.MapToModelMap(searchMap, request);
        return "advertise/advertise_list";
    }
    
    
	@RequestMapping(value = "/v_add.do")
	public String addContract(HttpServletRequest request, HttpServletResponse response, ModelMap model)
	{
        List<Dictionary> dictionarys = dictionaryService.getDictionByParentCode("advertiseType");
        model.put("dictionarys", dictionarys);
		return "advertise/advertise_add";
	}
    
	
	@RequestMapping(value = "/v_edit.do")
	public String editContract(HttpServletRequest request, HttpServletResponse response, ModelMap model) {
		Map<String, String> searchMap = RequestUtils.getSearchMap(request);
		Integer id = Integer.parseInt(searchMap.get("so_id"));
		Advertise advertise = advertiseService.queryById(id);
		model.put("advertise", advertise);

        List<Dictionary> dictionarys = dictionaryService.getDictionByParentCode("advertiseType");
        model.put("dictionarys", dictionarys);

		return "advertise/advertise_edit";
	}
    /**
     * 根据id删除
     * 
     * @author 周天驰
     * @param aztmsg
     * @param request
     * @param response
     * @param model
     * @return
     * @see [类、类#方法、类#成员]
     */
    @RequestMapping(value = "/ajax_del.do", method = RequestMethod.GET)
    @ResponseBody
    public AjaxJson ajaxDel(Advertise advertise, HttpServletRequest request, HttpServletResponse response, ModelMap model)
    {
        Integer id = advertise.getId();
        if (id != null && !"".equals(id))
        {
            // 删除父子级
        	advertiseService.deleteAllById(id);
        }
        return AjaxJson.success();
    }
    
    /**
     * 保存新增
     * 
     * @author 周天驰
     * @param advertise
     * @return
     * @see [类、类#方法、类#成员]
     */
    
    @RequestMapping(value = "/o_add.do", method = RequestMethod.POST)
    public String saveAdvertise(Advertise advertise)
    {
        boolean exist = advertiseService.findOrtherAdvertise(advertise);
        if (!exist)
        {
            advertise.setCreatetime(new Date());
            advertiseService.saveAdvertise(advertise);
        }
        return "redirect:/advertise/v_list.do";
    }
    
    /**
     * 保存编辑
     * 
     * @author 周天驰
     * @param advertise
     * @return
     * @see [类、类#方法、类#成员]
     */
    
    @RequestMapping(value = "/o_edit.do", method = RequestMethod.POST)
    public String saveEdit(Advertise advertise)
    {
        boolean exist = advertiseService.findOrtherAdvertise(advertise);
        if (!exist)
        {
        	advertiseService.saveEdit(advertise);
        }
        return "redirect:/advertise/v_list.do";
    }
    
    /**
     * 列表页面
     * 
     * @author 周天驰
     * @param request
     * @param pageNo
     * @param response
     * @param model
     * @return String
     * @see [类、类#方法、类#成员]
     */
    @RequestMapping(value = "/{adid}/img_list.do")
    public String imgList(HttpServletRequest request, Integer pageNo, HttpServletResponse response, ModelMap model,@PathVariable("adid") String adid)
    {
        Map<String, String> searchMap = RequestUtils.getSearchMap(request);
        searchMap.put("adid", adid);
        Pagination pagination = advertiseService.getAdvertiseImgPage(pageNo == null ? 1 : pageNo, Globals.PAGE_SIZE, searchMap);
        model.put("pagination", pagination);
        
        List<Advertise> list = advertiseService.getAdvertiseList();
        model.put("advertises", list);
        
        RequestUtils.MapToModelMap(searchMap, request);
        return "advertise/advertise_img_list";
    }
    
    @RequestMapping(value = "/img_del.do", method = RequestMethod.GET)
    @ResponseBody
    public AjaxJson imgAjaxDel(AdvertiseImg advertiseimg, HttpServletRequest request, HttpServletResponse response, ModelMap model)
    {
        Integer id = advertiseimg.getId();
        if (id != null && !"".equals(id))
        {
            // 删除父子级
        	advertiseService.deleteImgById(id);
        }
        return AjaxJson.success();
    }
    
    
    @RequestMapping(value = "/img_save.do", method = RequestMethod.POST)
    @ResponseBody
    public AjaxJson saveImg(AdvertiseImg advertiseimg)
    {
        advertiseService.saveAdvertiseImg(advertiseimg);
        return AjaxJson.success();
    }

    
    @RequestMapping(value = "/img_edit.do", method = RequestMethod.POST)
    @ResponseBody
    public AjaxJson imgEdit(AdvertiseImg advertiseimg)
    {

        advertiseService.editAdvertiseImg(advertiseimg);
        return AjaxJson.success();
    }

    /**
     * 载入管理首页轮播广告的页面
     * */
    @RequestMapping(value = "/loadIndexAd.do")
    public String loadIndexAd() {
        return "advertise/index_advertise";
    }

    @ResponseBody
    @RequestMapping(value = "/searchIndexAd.do")
    public AjaxJson searchIndexAd() {
        List<IndexAd> indexAdList = new ArrayList<>();
        //获取首页轮播广告位id
        Advertise advertise = advertiseService.getAdvertiseByCode(Globals.INDEX_AD_CODE);
        if(advertise != null) {
            List<AdvertiseImg> imgList = advertise.getAdvertiseImgs();
            if(CollectionUtil.isNotEmpty(imgList)) {
                for(AdvertiseImg img : imgList) {
                    IndexAd indexAd = new IndexAd();
                    indexAd.setAdId(advertise.getId());
                    indexAd.setImgName(img.getImgName());
                    indexAd.setImgId(img.getId());
                    indexAd.setBgPath(img.getColor());
                    indexAd.setContentPath(img.getImgPath());
                    indexAd.setState(img.getState());
                    indexAd.setSeq(img.getSeq());
                    indexAd.setUrl(img.getImgUrl());
                    indexAdList.add(indexAd);
                }
            }
        }
        return AjaxJson.success().setObj(Ret.create("list", indexAdList).set("adId", advertise.getId()));
    }

    @ResponseBody
    @RequestMapping(value = "/saveIndexAd.do", method = RequestMethod.POST)
    public AjaxJson saveIndexAd(IndexAd indexAd) {
        if(indexAd != null) {
            if(indexAd.getImgId() != null) {
                //更新
                AdvertiseImg img = advertiseService.getAdImgById(indexAd.getImgId());
                if(img != null) {
                    img.setImgName(indexAd.getImgName());
                    img.setImgPath(indexAd.getContentPath());
                    img.setColor(indexAd.getBgPath());
                    img.setImgUrl(indexAd.getUrl());
                    img.setSeq(indexAd.getSeq());
                    advertiseService.saveAdvertiseImg(img);
                    return AjaxJson.success();
                }
            } else if(indexAd.getImgId() == null) {
                //新增
                AdvertiseImg img = new AdvertiseImg();
                img.setAdId(indexAd.getAdId());
                img.setImgName(indexAd.getImgName());
                img.setImgPath(indexAd.getContentPath());
                img.setColor(indexAd.getBgPath());
                img.setImgUrl(indexAd.getUrl());
                img.setState(1);
                img.setStarttime(new Date());
                img.setEndtime(DateUtil.offsetMonth(new Date(), 12));
                img.setSeq(indexAd.getSeq());
                advertiseService.saveAdvertiseImg(img);
                return AjaxJson.success();
            }
        }
        return AjaxJson.error("操作失败");
    }

    @ResponseBody
    @RequestMapping(value = "/unable.do", method = RequestMethod.POST)
    public AjaxJson unable(IndexAd indexAd) {
        if(indexAd != null && indexAd.getImgId() != null) {
            AdvertiseImg img = new AdvertiseImg(indexAd.getImgId());
            img.setState(0);
            advertiseService.updateAdImg(img);
            return AjaxJson.success();
        }
        return AjaxJson.error("操作失败");
    }
}
