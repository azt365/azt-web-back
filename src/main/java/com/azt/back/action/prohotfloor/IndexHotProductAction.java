package com.azt.back.action.prohotfloor;

import com.azt.api.pojo.ProHotProductShow;
import com.azt.api.pojo.ProShopHotProductReq;
import com.azt.api.pojo.ProShopHotProductVO;
import com.azt.api.service.ConfigService;
import com.azt.api.service.ProHotFloorService;
import com.azt.model.page.TPagination;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 首页爆品楼层
 * Created by Anonymous on 2017/3/16.
 */
@Controller
@RequestMapping("/hotfloor")
public class IndexHotProductAction {

    @Autowired
    private ProHotFloorService proHotFloorService;

    @Autowired
    private ConfigService configService;

    @RequestMapping(value = "/index_hot_product.do", method = RequestMethod.GET)
    public String manager(Model model) {
        // 加载已有商品列表
        List<ProShopHotProductVO> products = proHotFloorService.getHotShopProducts();
        model.addAttribute("products", products);
        return "hotfloor/index_hot_product";
    }

    @RequestMapping(value = "/index_hot_product.do", method = RequestMethod.POST)
    public void addIndexHotProduct(ProShopHotProductReq req) {
        proHotFloorService.saveHotShopProduct(req.getShopHotProducts());
    }

    @RequestMapping(value = "/choose_hot_product.do")
    public String chooseProduct(Model model) {
        model.addAttribute("WEB_SITE", configService.getConfigValueByCode("WEB_SITE"));
        return "hotfloor/choose_product";
    }

    @ResponseBody
    @RequestMapping(value = "/hot_product_list.do", produces="application/json;charset=UTF-8")
    public TPagination<ProHotProductShow> hotProductList(@RequestParam(value = "page", defaultValue = "1") Integer page, String name) {
        Map<String, Object> condition = new HashMap<>();
        condition.put("name", name);
        TPagination<ProHotProductShow> pagin = proHotFloorService.listHotProduct(page, 10, condition);
        return pagin;

    }


}
