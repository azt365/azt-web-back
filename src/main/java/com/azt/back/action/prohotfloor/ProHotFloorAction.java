package com.azt.back.action.prohotfloor;

import com.alibaba.fastjson.JSON;
import com.azt.api.pojo.Category;
import com.azt.api.pojo.ProHotFloor;
import com.azt.api.pojo.ProHotProduct;
import com.azt.api.service.ConfigService;
import com.azt.api.service.ProCategoryService;
import com.azt.api.service.ProHotFloorService;
import com.azt.back.action.BaseAction;
import com.azt.model.AjaxJson;
import com.azt.model.page.Pagination;
import com.azt.utils.RequestUtils;
import com.google.common.collect.Lists;
import com.xiaoleilu.hutool.util.CollectionUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.*;

@Controller
@RequestMapping("/hotfloor")
public class ProHotFloorAction extends BaseAction {

    public static final Logger logger = LoggerFactory.getLogger(ProHotFloorAction.class);

    @Autowired
    private ProHotFloorService proHotFloorService;

    @Autowired
    private ProCategoryService proCategoryService;

    @Autowired
    private ConfigService configService;

    //爆品页面
    @RequestMapping(value="/hotfloorlist.do")
    public String selectHotFloorList(ModelMap map) {
    	 Map<String, String> searchMap = RequestUtils.getSearchMap(request);
    	 List<ProHotFloor> getHotFloorList=proHotFloorService.getHotProductList(searchMap);
    	 map.addAttribute("getHotFloorList", getHotFloorList);
		 return "hotfloor/hotfloorlist";
    }
    //得到楼层集合
    @RequestMapping(value="/addhotproduct.do")
    public String addHotProduct(Model model) {
        Integer floorId = getParaToInt("floorId");
    	ProHotFloor proHotFloor=new ProHotFloor();
    	List<Integer> allFloors=proHotFloor.getFloorList();
    	List<Integer> selectFloors=proHotFloorService.getFloors();
    	allFloors.removeAll(selectFloors);
        model.addAttribute("allFloors", allFloors);

        List<Category> topCategoryList = proCategoryService.findTopCategory();
        model.addAttribute("topCategoryList", topCategoryList);

        if(floorId != null) {
            //更新页面
            ProHotFloor hotFloor = proHotFloorService.getHotFloorById(floorId);
            List<ProHotProduct> hotProductList = proHotFloorService.findHotProductByFloorId(floorId);
            hotFloor.setHotProductList(hotProductList);
            model.addAttribute("hotFloor", hotFloor);
        }
        model.addAttribute("WEB_SITE", configService.getConfigValueByCode("WEB_SITE"));

    	return "hotfloor/edit_hotproduct";
    }

    @ResponseBody
    @RequestMapping(value = "/saveHotProduct.do")
    public AjaxJson saveHotProduct(ProHotFloor hotFloor) {
        AjaxJson aj=new AjaxJson();
        boolean update = false;
        if(hotFloor != null) {
            update = hotFloor.getId() != null;
            if(!update) {
                //新增
                proHotFloorService.saveHotFloor(hotFloor);
                for(ProHotProduct hotProduct : hotFloor.getHotProductList()) {
                    hotProduct.setFloorId(hotFloor.getId());
                    proHotFloorService.insertHotProduct(hotProduct);
                }
            } else {
                proHotFloorService.updateHotFloor(hotFloor);
            }
        }
        return aj;
    }

    //分页查询
    @ResponseBody
    @RequestMapping(value="/chooseproduct.do")
    public AjaxJson getChooseProduct(Integer pageNo, HttpServletRequest request) {
        AjaxJson aj = new AjaxJson();
        try {
            Map<String, String> searchMap = RequestUtils.getSearchMap(request);
            searchMap.put("so_cusrole", "2");
            Pagination pagination = proHotFloorService.findCenProduct(pageNo == null ? 1 : pageNo, 6, searchMap);
            Map<String, Object> sources = new HashMap<>();
            sources.put("productList", pagination.getList());
            sources.put("pageNo", pagination.getPageNo());
            sources.put("totalPage", pagination.getTotalPage());
            sources.put("so_companyname", searchMap.get("so_companyname"));
            aj.setAttributes(sources);
        } catch (Exception e) {
            e.printStackTrace();
            aj = AjaxJson.error("请求失败");
        }
        return aj;
    }
    
    @RequestMapping(value="/managehotproduct.do")
    public String manageHotProduct(HttpServletRequest req,HttpServletResponse res,ModelMap map){
    	return "hotfloor/managehotproduct";
    }

    @ResponseBody
    @RequestMapping(value="is_hot_product_in_index.do")
    public AjaxJson isHotProductInIndex(Integer floorId){
        AjaxJson aj=new AjaxJson();
        String message = proHotFloorService.isHotProductInIndex(floorId);
        aj.setMsg(message);
        return aj;
    }

    @ResponseBody
    @RequestMapping(value="is_hot_product_in_other_floor.do")
    public AjaxJson isHotProductInOtherFloor(@RequestParam(value = "floorId", required = false) Integer floorId, @RequestParam("productIds[]") Integer[] productIds){
        AjaxJson aj=new AjaxJson();
        String message = proHotFloorService.isHotProductInOtherFloor(floorId, Lists.newArrayList(productIds));
        aj.setMsg(message);
        return aj;
    }


    @RequestMapping(value="/delete_floor.do")
    @ResponseBody
    public AjaxJson deleteFloor(Integer floorId){
        AjaxJson aj=new AjaxJson();
        proHotFloorService.deleteHotFloorById(floorId);
        return aj;
    }


    /**
     * 修改爆品楼层
     */
    @RequestMapping("modify_hot_product.do")
    public String modifyHotProduct(Integer id, Model model) {
        logger.debug("modify hot product id {}", id);
        ProHotFloor proHotFloor=new ProHotFloor();
        List<Integer> allFloors=proHotFloor.getFloorList();
        List<Integer> selectFloors = proHotFloorService.getFloors();
        allFloors.removeAll(selectFloors);

        List<Category> topCategoryList = proCategoryService.findTopCategory();
        model.addAttribute("topCategoryList", topCategoryList);

        ProHotFloor floor = proHotFloorService.getHotFloorAndProducts(id);
        model.addAttribute("hotFloor", floor);
        // 把当前楼层加进去
        allFloors.add(floor.getFloor());
        CollectionUtil.sort(allFloors, new Comparator<Integer>() {
            @Override
            public int compare(Integer o1, Integer o2) {
                return o1 - o2;
            }
        });
        model.addAttribute("allFloors", allFloors);

        // 加载所有首页爆品，删除的时候，用来提示
        List<Integer> shopHotProductIds = proHotFloorService.listShopHotProductIds();
        model.addAttribute("shopHotProductIds", JSON.toJSONString(shopHotProductIds));

        model.addAttribute("WEB_SITE", configService.getConfigValueByCode("WEB_SITE"));

        return "hotfloor/edit_hotproduct";
    }


}
