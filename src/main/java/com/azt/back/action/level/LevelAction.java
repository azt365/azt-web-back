package com.azt.back.action.level;

import com.azt.api.service.LevelService;
import com.azt.model.AjaxJson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 模拟增加成长值
 * Created by LiQZ on 2017/4/14.
 */
@Controller
@RequestMapping("/level")
public class LevelAction {

    @Autowired
    private LevelService levelService;

    /**
     * $.post('/level/addAmount.do', {"companyId":"1", "type": 1, "code": "p_xx"}, function (data) { console.log(data); });
     */
    @ResponseBody
    @RequestMapping("/addAmount.do")
    public AjaxJson addAmount(Integer companyId, Integer type, String code) {
        levelService.updateLevel(companyId, type, code);
        return AjaxJson.success();
    }

}
