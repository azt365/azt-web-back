package com.azt.back.action.level;

import com.azt.api.pojo.LevelRule;
import com.azt.api.pojo.req.LevelRuleReq;
import com.azt.api.service.LevelRuleService;
import com.azt.back.action.BaseAction;
import com.azt.back.util.Globals;
import com.azt.model.AjaxJson;
import com.azt.model.page.Pagination;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 等级规则
 * Created by LiQZ on 2017/4/13.
 */
@Controller
@RequestMapping("/level/rule")
public class LevelRuleAction extends BaseAction {

    @Autowired
    private LevelRuleService levelRuleService;

    @RequestMapping("list.do")
    public String list(Model model, LevelRuleReq req) {
        model.addAttribute("ruleName", req.getRuleName());
        model.addAttribute("removed", req.getRemoved());
        Pagination pagination = levelRuleService.listPage(1, Globals.PAGE_SIZE, model.asMap());
        model.addAttribute("pagination", pagination);
        return "level/rule/list";
    }

    @ResponseBody
    @RequestMapping("get.do")
    public AjaxJson get(Integer id) {
        AjaxJson result = AjaxJson.success();
        LevelRule rule = levelRuleService.get(id);
        result.setObj(rule);
        return result;
    }

    @ResponseBody
    @RequestMapping("save.do")
    public AjaxJson save(LevelRuleReq req) {
        levelRuleService.save(req);
        return AjaxJson.success();
    }

    @ResponseBody
    @RequestMapping("delete.do")
    public AjaxJson delete(Integer id, Integer value) {
        levelRuleService.delete(id, value);
        return AjaxJson.success();
    }


}
