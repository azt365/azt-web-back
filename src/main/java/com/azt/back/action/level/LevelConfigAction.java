package com.azt.back.action.level;

import com.azt.api.pojo.LevelConfig;
import com.azt.api.pojo.req.LevelConfigReq;
import com.azt.api.service.LevelConfigService;
import com.azt.model.AjaxJson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;


/**
 * 积分规则管理
 * Created by LiQZ on 2017/4/13.
 */
@Controller
@RequestMapping("/level/config")
public class LevelConfigAction {

    @Autowired
    private LevelConfigService levelConfigService;

    @RequestMapping("list.do")
    public String list(Model model, Integer type) {
        List<LevelConfig> list = levelConfigService.list(type);
        model.addAttribute("list", list);
        model.addAttribute("levelType", type);
        return "level/config/list";
    }

    @ResponseBody
    @RequestMapping("get.do")
    public AjaxJson get(Integer id) {
        LevelConfig config = levelConfigService.get(id);
        AjaxJson json = AjaxJson.success();
        json.setObj(config);
        return json;
    }

    @ResponseBody
    @RequestMapping("save.do")
    public AjaxJson save(LevelConfigReq req) {
        levelConfigService.save(req);
        return AjaxJson.success();
    }

    @ResponseBody
    @RequestMapping("delete.do")
    public AjaxJson delete(Integer id) {
        levelConfigService.delete(id);
        return AjaxJson.success();
    }

}
