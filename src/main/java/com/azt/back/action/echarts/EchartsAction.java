package com.azt.back.action.echarts;

import com.azt.back.action.BaseAction;
import com.xiaoleilu.hutool.date.DateUtil;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by 张栋 on 2016/12/1513:10
 */
@Controller
@RequestMapping("/echarts")
public class EchartsAction extends BaseAction {
    @RequestMapping("/index.do")
    public String companyInfo( ModelMap model){
        return "echarts/echarts_index";
    }

    @RequestMapping("/project_grid.do")
    public String project_grid( ModelMap model){
        return "echarts/echarts_project_grid";
    }

    @RequestMapping("/load.do")
    public String load( ModelMap model){
        return "echarts/statistics";
    }

    // todo
    @RequestMapping("/export.do")
    public void export() {
        Integer type = getParaToInt("type", 1);
        Date sdate = getParaToDate("sdate", "yyyy-MM");
        Date edate = getParaToDate("edate", "yyyy-MM");
        Map param = new HashMap();
        param.put("sdate", sdate);
        if(edate != null) {
            param.put("edate", DateUtil.beginOfMonth(DateUtil.offsetMonth(edate, 1)));
        }

    }
}
