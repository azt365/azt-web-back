package com.azt.back.action.quartz.task;

import java.util.Calendar;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.azt.api.pojo.IntegralRecord;
import com.azt.api.pojo.User;
import com.azt.api.pojo.UserAccount;
import com.azt.api.service.IntegralService;
import com.azt.api.service.UserService;
import com.azt.back.action.quartz.TaskInterface;
import com.azt.utils.FormatUtil;

/**
 * @ClassName: IntegralTimeTask
 * @Description: 积分定时任务 定时任务算法描述：
 *               例：当前时间2017年，在2017年12月31号这天启动积分定时任务执行将2017年之前的积分记录状态全部更改为过期
 *               假如2015年所有积分500分，2016年1000分，2017年当前2000分
 *               需过期1500分，当前若已使用2000分，则说明全部用过了不需要扣除，若当前已使用1200分，
 *               则有300分自动扣除加入积分记录表
 * @author: 查恒
 * @date: 2017年2月23日 上午10:25:51
 */
@Component
public class IntegralTimeTask implements TaskInterface {
	private Logger logger = LoggerFactory.getLogger("myQuartz");

	@Autowired
	private IntegralService integralService;
	@Autowired
	private UserService userService;
	@Override
	public void execute() {
		
		// 获取当前时间的年份
		Calendar currCal = Calendar.getInstance();
		int currentYear = currCal.get(Calendar.YEAR);
		String recordTime=currentYear+"-01-01";
		Integer lastYear = FormatUtil.getLastYear();
		Integer overdue=1;//未过期
		Integer operateType=1;//加的积分
		//查询所有需要处理的用户
		List<User> userByIngral = userService.getUserByIngralRecord(recordTime);
		if(userByIngral!=null && userByIngral.size()>0){
			for(User user :userByIngral){
				//将要过期的积分设置为过期
				boolean setOverdue = integralService.setOverdueToIntegralRecord(user.getId(), operateType, overdue, recordTime);
				if(setOverdue){
					//获取今年之前的所有没有过期的积分的总和
					Integer overIntegral = integralService.getOverIntegralRecordByUserId(user.getId(), operateType,lastYear);
					UserAccount userAccount = userService.getUserAccountByUserId(user.getId());
					Integer disIntegral=userAccount.getDisIntegral();
					String descr = lastYear.toString()+"年" ;
					if(disIntegral<overIntegral){
						Integer kouchu=overIntegral-disIntegral;
						integralService.addUserRecord(user.getId(), "overdue_integral",kouchu.doubleValue(),descr, null);
					}
				}
			}
		}
      
	}

}
