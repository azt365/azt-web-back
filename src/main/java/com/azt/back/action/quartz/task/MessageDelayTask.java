package com.azt.back.action.quartz.task;

import com.azt.api.pojo.MsgUser;
import com.azt.api.service.MsgConfigService;
import com.azt.api.service.MsgUserService;
import com.azt.back.action.quartz.TaskInterface;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;

/**
 * 延迟发短信
 * Created by 张栋 on 2017/1/99:57
 */
@Component
public class MessageDelayTask implements TaskInterface {
    private Logger logger = LoggerFactory.getLogger("myQuartz");



    @Autowired
    private MsgUserService ms;

    @Autowired
    private MsgConfigService cm;

    @Override
    public void execute() {
        logger.info("--------MessageDelayOldTask  start ---------");
        List<MsgUser> msgs = ms.getDelayMsg();
        for (MsgUser msg : msgs) {
            msg.setDelay(0);//delay 设置为0,相当于发站内信
            String email = msg.getEmail();
            if(StringUtils.isNotBlank(email)){
                //发邮件
                cm.sendDelayEmail(email,msg.getMsgTitle(),msg.getEmailContent(),msg.getMsgUrl());
            }
            String mobile = msg.getMobile();
            if(StringUtils.isNotBlank(mobile)){
                cm.sendDelayMobile(mobile,msg.getMobileContent(),msg.getMsgType());
            }
            msg.setSorttime(new Date());
            ms.updateMsgUser(msg);
        }

        logger.info("--------MessageDelayOldTask  end ---------");
    }
}
