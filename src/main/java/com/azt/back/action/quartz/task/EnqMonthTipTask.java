package com.azt.back.action.quartz.task;

import com.azt.api.dto.UserExt;
import com.azt.api.dto.UserForMonthTip;
import com.azt.api.pojo.Company;
import com.azt.api.pojo.User;
import com.azt.api.service.CompanyService;
import com.azt.api.service.MsgConfigService;
import com.azt.api.service.UserService;
import com.azt.back.action.quartz.TaskInterface;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.*;

/**
 * 每月推送给采供双方短信提示
 * Created by 张栋 on 2016/12/28 10:44
 */
@Component
public class EnqMonthTipTask implements TaskInterface {

    private Logger logger = LoggerFactory.getLogger("myQuartz");

    @Autowired
    private UserService userService;
    @Autowired
    private CompanyService companyService;


    @Autowired
    private MsgConfigService msgConfigService;

    @Override
    public void execute() {
        logger.info("--------EnqMonthTipTask  start ---------");
        List<UserExt> cleader = userService.getAllLeaderIds("cleader");//采购
        List<UserExt> pleader = userService.getAllLeaderIds("pleader");//供应

        Calendar ca = Calendar.getInstance();
        ca.add(Calendar.MONTH, -1);
        Integer year = ca.get(Calendar.YEAR);
        Integer month = ca.get(Calendar.MONTH) + 1;


        int x = 0;
        while (x < 2) {
            x++;
            String type = "cleader";//采购
            if (x == 2) {
                cleader = pleader;
                type = "pleader";//供应
            }

            //获取
            for (int i = 0; i < cleader.size(); i++) {

                UserExt userExt = cleader.get(i);
                logger.info("{} leader, id:{},name:{}", type, userExt.getId(), userExt.getUserName());

                Map<String, String> params = new HashMap<>();
                //尊敬的{companyName}公司负责人{userName}，小通提示您，贵公司{month}月使用安装通平台完成了￥{totalPrice}的采购，请登录平台，在消息中查看详情，感谢您对安装通平台一直以来的支持。
                params.put("companyName", userExt.getCompanyName());
                params.put("userName", userExt.getUserName());
                params.put("month", month + "");
                //获取该领导公司里的所有员工
                List<User> users = userService.getUsersBYCompanyId(userExt.getCompanyId());
                StringBuffer sb = new StringBuffer();
                if(type.equals("pleader")){//供应商,供应商没有分公司的概念,因此就一层员工
                    sb.append("公司员工列表");
                    sb = userMonthTip(year, month, type, params, users,sb);
                    params.put("messageTable", sb.toString());
                    msgConfigService.sendMessage("TASK_MONTH_PROVIDER_TIP", params, null, userExt.getId());
                    continue;
                }else{
                    sb.append("集团公司员工列表");
                }

                sb = userMonthTip(year, month, type, params, users,sb);
                //查区域公司
                List<Company> companies = companyService.getCompanyByPId(userExt.getCompanyId());
                for (int i1 = 0; i1 < companies.size(); i1++) {
                    Company company = companies.get(i1);
                    List<User> users1 = userService.getUsersBYCompanyId(company.getId());
                    if(!users1.isEmpty()){
                        sb.append("区域公司员工列表");
                        sb = userMonthTip(year, month, type, params, users1,sb);
                    }

                    List<Company> companies3 = companyService.getCompanyByPId(company.getId());
                    for (int i2 = 0; i2 < companies3.size(); i2++) {
                        Company company3 = companies3.get(i2);
                        List<User> u3 = userService.getUsersBYCompanyId(company3.getId());
                        if(!u3.isEmpty()){
                            sb.append("分公司员工列表");
                            sb = userMonthTip(year, month, type, params, u3,sb);
                        }
                    }
                }

                params.put("messageTable", sb.toString());
                //发送信息5745
                //msgConfigService.sendMessage("TASK_MONTH_TIP",params,null,5745);
                msgConfigService.sendMessage("TASK_MONTH_PURCHASE_TIP", params, null, userExt.getId());

            }
        }
        logger.info("--------EnqMonthTipTask  end ---------");
    }

    private StringBuffer userMonthTip(Integer year, Integer month, String type, Map<String, String> params, List<User> users,StringBuffer sb ) {
        List<UserForMonthTip> tips = new ArrayList<>();
        BigDecimal totalp = new BigDecimal(0);
        Long totalLogintimes = 0l;
        Long totalEnqTimes = 0l;
        Long enqStrustTimes = 0l;
        for (int j = 0; j < users.size(); j++) {
            User u = users.get(j);
            UserForMonthTip userForMonthTip = userService.getUserForMonthTip(u.getId(), type, year, month);
            tips.add(userForMonthTip);
            if (userForMonthTip.getTotalprice() == null)
                userForMonthTip.setTotalprice(0d);
            totalp = totalp.add(new BigDecimal(userForMonthTip.getTotalprice()));
            totalLogintimes = totalLogintimes + userForMonthTip.getLoginTimes();
            totalEnqTimes = totalEnqTimes + userForMonthTip.getEnqTimes();
            if (userForMonthTip.getEnqStrustTimes() == null) {
                userForMonthTip.setEnqStrustTimes(0l);
            }
            enqStrustTimes = enqStrustTimes + userForMonthTip.getEnqStrustTimes();
        }
        params.put("totalPrice", totalp.doubleValue() + "");

        sb.append("<table class='message-tip'>");
        sb.append("<tr>");
        sb.append("<td style='width:200px;' >员工名称</td>");
        sb.append("<td>登录次数</td>");
        if (type.equals("cleader")) {
            sb.append("<td>发布询价数</td>");
            sb.append("<td>发布委托采购数</td>");
            sb.append("<td>完成采购金额</td>");
        } else {
            sb.append("<td>报价次数</td>");
            sb.append("<td>完成销售金额</td>");
        }


        sb.append("</tr>");
        for (int j = 0; j < tips.size(); j++) {
            sb.append("<tr>");
            UserForMonthTip userForMonthTip = tips.get(j);
            sb.append("<td>");
            sb.append(userForMonthTip.getRealName());
            sb.append("</td>");
            sb.append("<td>");
            sb.append(userForMonthTip.getLoginTimes());
            sb.append("</td>");
            sb.append("<td>");
            sb.append(userForMonthTip.getEnqTimes());
            sb.append("</td>");

            if (type.equals("cleader")) {
                sb.append("<td>");
                sb.append(userForMonthTip.getEnqStrustTimes());
                sb.append("</td>");
            }

            sb.append("<td>");
            sb.append("￥" + userForMonthTip.getTotalprice());
            sb.append("</td>");
            sb.append("</tr>");
        }
        sb.append("<tr>");
        sb.append("<td>合计</td>");
        sb.append("<td>");
        sb.append(totalLogintimes);
        sb.append("</td>");

        sb.append("<td>");
        sb.append(totalEnqTimes);
        sb.append("</td>");

        if (type.equals("cleader")) {
            sb.append("<td>");
            sb.append(enqStrustTimes);
            sb.append("</td>");
        }

        sb.append("<td>");
        sb.append("￥" + totalp.doubleValue());
        sb.append("</td>");
        sb.append("</tr>");
        sb.append("</table>");
        return sb;
    }

}
