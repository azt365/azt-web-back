package com.azt.back.action.quartz.task;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.azt.api.enums.EnqEnquiryEnum;
import com.azt.api.pojo.EnqEnquiry;
import com.azt.api.service.EnqEnquiryService;
import com.azt.api.service.EnqQuoteService;
import com.azt.back.action.quartz.TaskInterface;


/**
 * 询价状态变为截止
 * Created by 张栋 on 2016/11/23 17:28
 */
@Component
public class EnqDeadTimeTask implements TaskInterface{

	private Logger  logger  = LoggerFactory.getLogger("myQuartz");

	@Autowired
	private EnqEnquiryService enq;

	@Autowired
	private EnqQuoteService quoteService;


	@Override
	public void execute() {
		logger.info("--------EnqDeadTimeTask  start ---------");
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Calendar ca = Calendar.getInstance();
		String format = sdf.format(ca.getTime());
		long nowtime = 0;
		try {
			Date parse = sdf.parse(format);
			nowtime = parse.getTime();
		} catch (ParseException e) {
			e.printStackTrace();
		}
		//除去  已经关闭的,已经截止的,其他都要变成 已截止的
		List<EnqEnquiry> enquiryList = enq.getTaskEnqByStates();
		for(EnqEnquiry e:enquiryList){
			Date deadline = e.getDeadline();
			try {
				String format1 = sdf.format(deadline);
				long enqtime = sdf.parse(format1).getTime();
				if(nowtime > enqtime){
					e.setState(EnqEnquiryEnum.ENQ_OVER.getIndex());
					enq.saveOrUpdateEnq(e);
					logger.info("关闭--id:"+e.getId()+" date:"+format1+":name"+e.getEnquiryName());
					quoteService.changeQuteStatFromToByEnqid(e.getId(),1,5);
				}
			} catch (ParseException e1) {
				e1.printStackTrace();
			}
		}
		logger.info("--------EnqDeadTimeTask  end ---------");
	}

}
