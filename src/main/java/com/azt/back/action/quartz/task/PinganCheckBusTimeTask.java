package com.azt.back.action.quartz.task;

import com.azt.back.action.quartz.TaskInterface;
import com.azt.utils.HttpClientUtil;
import com.azt.utils.PabankPropertiesUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;


/**
 * 平安每日核查前一日的交易信息
 * zouheyuan
 */
@Component
public class PinganCheckBusTimeTask implements TaskInterface{

	private Logger  logger  = LoggerFactory.getLogger("myQuartz");


	@Override
	public void execute() {
		logger.info("--------PinganCheckBusTimeTask  start ---------");
		HttpClientUtil.doPost(PabankPropertiesUtils.readProperites("frontAddress") + "/pingan/quartzCheckBus", null);
		logger.info("--------PinganCheckBusTimeTask  end ---------");
	}

}
