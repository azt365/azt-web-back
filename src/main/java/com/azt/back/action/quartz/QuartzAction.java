package com.azt.back.action.quartz;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.azt.api.pojo.quartz.ScheduleJob;
import com.azt.api.service.quartz.TimeTaskService;
import org.quartz.CronScheduleBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.azt.model.AjaxJson;
import com.azt.utils.BeanUtils;

/**
 * 定时任务控制
 */
@Controller
@RequestMapping("/quartz")
public class QuartzAction {

	@Autowired
	private TimeTaskService titmeTaskManager;
	//
	@Autowired
	private LoadTask loadTask;

	//
	@RequestMapping("v_tasklist.do")
	public String v_list(HttpServletRequest request, HttpServletResponse response, ModelMap model) {
		List<ScheduleJob> itmeTask = titmeTaskManager.getAllScheduleJob();

		model.put("tasks", itmeTask);

		return "quartz/task_list";
	}

	/**
	 * 添加 或者 更新
	 */
	@RequestMapping("o_add.do")
	@ResponseBody
	public AjaxJson o_add(ScheduleJob task, HttpServletRequest request, HttpServletResponse response, ModelMap model) {
		AjaxJson j = new AjaxJson();

		/**
		 * 检验
		 */
		try {
			CronScheduleBuilder.cronSchedule(task.getCron_expression());
		} catch (Exception e) {
			j.setMsg("cron表达式有误，不能被解析！");
			return j;
		}

		if (task.getId() == null) {
			task.setJob_status(0);
			titmeTaskManager.save(task);
		} else {
			ScheduleJob old = titmeTaskManager.getScheduleJobById(task.getId());
			// 更新时，将任务删除掉,需要手动启动
			if (old.getJob_status() == 1) {
				// 如果bean 名称不同，则停止掉并且重新开始
				if (!old.getJob_name().equals(task.getJob_name())) {
					loadTask.delete(old.getId());
					try {
						BeanUtils.copyBeanNotNull2Bean(task, old);
						titmeTaskManager.updateEntitie(old);
					} catch (Exception e) {
						e.printStackTrace();
					}

					loadTask.start(old.getId());

				} else if (!old.getCron_expression().equals(task.getCron_expression())) {
					// 如果表达式不同,则进行更新
					loadTask.update(old.getId());
					try {
						BeanUtils.copyBeanNotNull2Bean(task, old);
						titmeTaskManager.updateEntitie(old);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}

			} else {
				try {
					BeanUtils.copyBeanNotNull2Bean(task, old);
					titmeTaskManager.updateEntitie(old);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
		return j;
	}

	@RequestMapping("o_start.do")
	@ResponseBody
	public AjaxJson o_start(ScheduleJob task, HttpServletRequest request, HttpServletResponse response, ModelMap model) {
		AjaxJson j = new AjaxJson();
		loadTask.start(task.getId());
		return j;
	}

	@RequestMapping("o_close.do")
	@ResponseBody
	public AjaxJson o_close(ScheduleJob task, HttpServletRequest request, HttpServletResponse response, ModelMap model) {
		AjaxJson j = new AjaxJson();
		loadTask.stop(task.getId());
		return j;
	}

	@RequestMapping("o_del.do")
	@ResponseBody
	public AjaxJson o_del(ScheduleJob task, HttpServletRequest request, HttpServletResponse response, ModelMap model) {
		AjaxJson j = new AjaxJson();
		loadTask.delete(task.getId());
		return j;
	}

	/**
	 * 立即执行一次
	 * Created by 张栋 on 2016/11/23 15:20
	 */
	@RequestMapping("o_once.do")
	@ResponseBody
	public AjaxJson o_once(ScheduleJob task){
	    AjaxJson j = new AjaxJson();
		boolean b = loadTask.runAJobNow(task.getId());
		j.setSuccess(b);
		return j;
	}


}
