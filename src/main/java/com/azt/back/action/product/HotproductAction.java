package com.azt.back.action.product;
import com.azt.api.pojo.Hotproduct;
import com.azt.api.pojo.Newpub;
import com.azt.api.pojo.OldProduct;
import com.azt.api.service.HotproductService;
import com.azt.model.page.Pagination;
import com.azt.utils.RequestUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.List;
import java.util.Map;


@Controller
@RequestMapping("/hotproduct")
public class HotproductAction {
	
	@Autowired
	private HotproductService hotproductservice;
	
	@RequestMapping("/hotproduct_list.do")
	public String queryPagehotpruduct(Model model,Hotproduct product,Integer pageNo,HttpServletRequest request){
		Map<String, String> searchMap=RequestUtils.getSearchMap(request);
		//List<Hotproduct> prolist= hotproductservice.queryListHotprocuts();
		//model.addAttribute("hotpros", prolist);
		//Pagination page=hotproductservice.getHotproductBysql(pageNo==null?1:pageNo, Globals.PAGE_SIZE, searchMap);
		Pagination page=hotproductservice.getHotproductPage(pageNo==null?1:pageNo, 8, searchMap);
		model.addAttribute("pagination", page);
		RequestUtils.MapToModelMap(searchMap, request);
		return "hotproduct/product";
	}
	
	@RequestMapping("/delhotproduct.do")
	@ResponseBody
	public Integer delHotproduct(Integer id){
		return hotproductservice.delHotproduct(id);
	}
	
	@RequestMapping("/changeEnable.do")
	@ResponseBody
	public Integer changeHotproductEnable(Integer id,Integer enable){
		return hotproductservice.changeHotproEnable(id, enable);
	}
	
	@RequestMapping("/getnewpro.do")
	@ResponseBody
	public List getNewpubByName(Integer pageindexnew,Integer pagesizenew,String productname,Integer isproOrpub){
		if(isproOrpub==0){
			List<Newpub> publist=hotproductservice.getNewproByname(pageindexnew, pagesizenew, productname);
			return  publist;
		}else{
			List<OldProduct> publist=hotproductservice.getProBynameOrid(pageindexnew, pagesizenew, productname);
			return  publist;
		}
	}
	
	@RequestMapping("/insertHotpro.do")
	public String isertNewHotpro(Hotproduct pro,String hotproid){
		if(!StringUtils.isNotBlank(hotproid)){
			pro.setCreatetime(new Date());
			pro.setUpdatetime(new Date());
			pro.setIsabled(1);
			hotproductservice.insertNewHotpro(pro);;
			return "redirect:/hotproduct/hotproduct_list.do";
		}else{
			//hotproductservice.insertNewHotpro(pro);
			Hotproduct hot=hotproductservice.getHotproductByid(Integer.parseInt(hotproid));
			//pro.setId(Integer.parseInt(hotproid));
			hot.setUpdatetime(new Date());
			hot.setSeq(pro.getSeq());
			hot.setImage(pro.getImage());
			hotproductservice.updateHotpro(hot);
			return "redirect:/hotproduct/hotproduct_list.do";
		} 
	}
	
}