package com.azt.back.action.product;

import com.azt.api.pojo.Category;
import com.azt.api.pojo.CategoryBrand;
import com.azt.api.service.ProBrandService;
import com.azt.api.service.ProCategoryBrandService;
import com.azt.api.service.ProCategoryService;
import com.azt.back.util.Globals;
import com.azt.model.page.Pagination;
import com.azt.utils.RequestUtils;
import net.sf.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/categoryBrand")
public class CategoryBrandAction {

	@Autowired
	private ProCategoryBrandService pcbs;

	@Autowired
	private ProBrandService pbs;

	@Autowired
	private ProCategoryService proCategoryService;

	@RequestMapping("/v_list.do")
	public String list(HttpServletRequest request, HttpServletResponse response, ModelMap model, Integer pageNo) {

		Map<String, String> searchMap = RequestUtils.getSearchMap(request);

		Pagination pagination = pcbs.getProCategoryBrandPage(pageNo == null ? 1 : pageNo, Globals.PAGE_SIZE_TEN,
				searchMap);

		model.put("p", pagination);

		return "category/ProCategoryBrand_list";
	}

	@RequestMapping("/get_brand.do")
	@ResponseBody
	public List<Map<String, Object>> getProBrandLikeName(Integer pageno, String queryName) {
		return pbs.getLikeName(queryName, pageno);
	}

	@RequestMapping(value="/o_add.do")
	@ResponseBody
	public JSONObject ajaxAdd(HttpServletRequest request,
			HttpServletResponse response, ModelMap model ,CategoryBrand procategorybrand){
		JSONObject json = new JSONObject();
		Integer categoryTId = procategorybrand.getCategoryTId();
		Map<String, Integer> CategorysMapId = proCategoryService.getParentCategorysIdBycategoryTId(categoryTId);
		
		try{
			//先判断是不是已经存在，存在就不添加
			Boolean validate = pcbs.ValidateProCategoryBrands(categoryTId,procategorybrand.getBrandId());
	        if(!validate){
	        	json.put("msg",	"已有此品牌");
	        	return json;
			}
			if(procategorybrand.getId()!=null&&!"".equals(procategorybrand.getId())){
				json.put("msg" , "编辑成功");
				CategoryBrand oldpcb = pcbs.getEntityById(procategorybrand.getId());
				oldpcb.setBrandId(procategorybrand.getBrandId());
			    oldpcb.setCategoryFId(CategorysMapId==null?null:CategorysMapId.get("id"));
				oldpcb.setCategorySId(CategorysMapId==null?null:CategorysMapId.get("pid"));
				oldpcb.setCategoryTId(CategorysMapId==null?null:CategorysMapId.get("tid")); 
				oldpcb.setSeq(procategorybrand.getSeq());
				oldpcb.setUpdatetime(new Date());
				this.pcbs.saveOrUpdate(oldpcb);
			}else{
				json.put("msg" , "新增成功");
				procategorybrand.setCategoryFId(CategorysMapId==null?null:CategorysMapId.get("id"));
				procategorybrand.setCategorySId(CategorysMapId==null?null:CategorysMapId.get("pid"));
				procategorybrand.setCategoryTId(CategorysMapId==null?null:CategorysMapId.get("tid")); 
				procategorybrand.setCreatetime(new Date());
				this.pcbs.saveOrUpdate(procategorybrand);
			}
			json.put("success", true);
		}catch(Exception e){
			e.printStackTrace();
			json.put("success", false);
			json.put("msg",	"操作失败");
		}
		return json;
	}

	@RequestMapping(value = "/o_del.do")
	@ResponseBody
	public JSONObject ajaxDel(HttpServletRequest request, HttpServletResponse response, ModelMap model,
			CategoryBrand pcb) {
		JSONObject json = new JSONObject();
		try {
			if (pcb.getId() != null && !"".equals(pcb.getId())) {
				json.put("msg", "删除成功");
				pcbs.delete(pcb);
				json.put("success", true);
			} else {
				json.put("msg", "删除失败，记录不存在");
				json.put("success", false);
			}
		} catch (Exception e) {
			e.printStackTrace();
			json.put("success", false);
			json.put("msg", "删除失败");
		}
		return json;
	}

	@RequestMapping(value = "/o_delete_more.do")
	@ResponseBody
	public JSONObject ajaxDel(HttpServletRequest request, HttpServletResponse response, String bids) {
		JSONObject json = new JSONObject();
		try {
			if (bids.length() > 0) {
				pcbs.deleteMore(bids.split(","));
			}
			json.put("msg", "删除成功");
			json.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			json.put("success", false);
			json.put("msg", "删除失败");
		}
		return json;
	}

}
