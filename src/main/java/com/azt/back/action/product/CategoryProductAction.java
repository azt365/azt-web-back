package com.azt.back.action.product;

import com.azt.api.dto.ProCategoryAttributes;
import com.azt.api.pojo.Category;
import com.azt.api.pojo.ProCategoryAttrItem;
import com.azt.api.pojo.ProCategoryAttribute;
import com.azt.api.pojo.ProCategorySeo;
import com.azt.api.service.ProCategoryAttrItemService;
import com.azt.api.service.ProCategoryAttributeService;
import com.azt.api.service.ProCategorySeoService;
import com.azt.api.service.ProCategoryService;
import com.azt.back.action.BaseAction;
import com.azt.model.AjaxJson;
import com.azt.utils.RequestUtils;
import com.xiaoleilu.hutool.util.CollectionUtil;
import com.xiaoleilu.hutool.util.StrUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;

/**
 * 
 * @author 查恒 2016年7月15日下午2:27:30
 */
@Controller
@RequestMapping("/category")
public class CategoryProductAction extends BaseAction {
	@Autowired
	private ProCategoryService proCategoryService;
	@Autowired
	private ProCategoryAttributeService proCategoryAttributeService;
	@Autowired
	private ProCategoryAttrItemService ProCategoryAttrItemService;
	@Autowired
	private ProCategorySeoService proCategorySeoService;

	// 产品类别
	@RequestMapping("/v_productattribute_list.do")
	public String productlist(HttpServletRequest request, HttpServletResponse response, ModelMap model) {
		// 查询
		Map<String, String> searchMap = RequestUtils.getSearchMap(request);
		String proId = searchMap.get("so_id");
		if (StringUtils.isNotBlank(proId)) {
			Category category = proCategoryService.getEntityById(Integer.parseInt(proId));
			List<ProCategoryAttribute> proCategoryAttributes = proCategoryAttributeService
					.getProCategoryAttributes(category.getId());
			model.addAttribute("category", category);// 页面得到产品类型
			model.addAttribute("proCategoryAttributes", proCategoryAttributes);
		}

		return "category/productattribute_list";

	}

	/** 保存和更新代码 */
	@ResponseBody
	@RequestMapping("/o_productattribute.do")
	public AjaxJson productattribute(ProCategoryAttributes pat) {
		Integer proCategoryId = getParaToInt("proCategoryId");
		if(proCategoryId != null) {
			if (pat != null) {
				// 先删除之前的数据，再更新新的数据
				List<ProCategoryAttribute> oldattributes = proCategoryAttributeService.getProCategoryAttributeByCategoryId(proCategoryId);
				if (oldattributes != null && oldattributes.size() > 0) {
					for (ProCategoryAttribute oldatt : oldattributes) {
						List<ProCategoryAttrItem> oldAttrItems = oldatt.getProCategoryAttrItems();
						for (ProCategoryAttrItem oldattrItem : oldAttrItems) {
							ProCategoryAttrItemService.deleteItem(oldattrItem);
						}
						proCategoryAttributeService.deleteAttribute(oldatt);
					}
				}

				List<ProCategoryAttribute> proCategoryAttribute = pat.getProCategoryAttribute();
				if (proCategoryAttribute != null && proCategoryAttribute.size() > 0) {
					for (ProCategoryAttribute pro : proCategoryAttribute) {
						pro.setCategoryId(proCategoryId);
						pro.setRemoved(0);//更新为可用
						proCategoryAttributeService.saveOrUpdate(pro);
						// 获取选项值
						List<ProCategoryAttrItem> proCategoryAttrItems = pro.getProCategoryAttrItems();
						if (CollectionUtil.isNotEmpty(proCategoryAttrItems)) {
							for (ProCategoryAttrItem cateItem : proCategoryAttrItems) {
								if(StrUtil.isNotBlank(cateItem.getName())) {
									cateItem.setAttrId(pro.getId());
									cateItem.setRemoved(0);
									ProCategoryAttrItemService.saveOrUpdate(cateItem);
								}
							}
						}
					}
				}
			}
		}
		return AjaxJson.success();
	}

	/** SEO设置 **/
	@RequestMapping("/v_productseo_list.do")
	public String productseo(HttpServletRequest request, HttpServletResponse response, ModelMap model) {
		// 查询
		Map<String, String> searchMap = RequestUtils.getSearchMap(request);
		String proId = searchMap.get("so_id");
		if (StringUtils.isNotBlank(proId)) {
			Category category = proCategoryService.getEntityById(Integer.parseInt(proId));
			ProCategorySeo proCategorySeo = proCategorySeoService.getProCategorySeo(category.getId());
			model.addAttribute("category", category);// 页面得到产品类型
			model.addAttribute("proCategorySeo", proCategorySeo);
		}
		return "category/productseo_list";
	}

	// 保存或编辑SEO
	@ResponseBody
	@RequestMapping("/o_productseo.do")
	public AjaxJson productseoEdit(HttpServletRequest request, HttpServletResponse response,
			ProCategorySeo proCategorySeo) {
		String proCategoryId = request.getParameter("proCategoryId");
		try {
			proCategorySeo.setCategoryId(Integer.parseInt(proCategoryId));
			proCategorySeoService.saveOrUpdate(proCategorySeo);
			return AjaxJson.success();
		} catch (Exception e) {
			return AjaxJson.error(e.getMessage());
		}
	}

}
