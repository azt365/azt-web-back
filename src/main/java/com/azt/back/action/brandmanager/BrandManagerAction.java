package com.azt.back.action.brandmanager;

import com.alibaba.fastjson.JSON;
import com.azt.api.pojo.Brand;
import com.azt.api.service.ProBrandService;
import com.azt.api.service.ProCategoryBrandService;
import com.azt.model.AjaxJson;
import com.azt.model.page.Pagination;
import com.azt.utils.RequestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;

/**
 * @ClassName: BrandManagerAction
 * @Description: 品牌管理的控制类
 * @author: zhaheng
 * @date: 2017年1月12日 上午10:36:37
 */
@Controller
@RequestMapping("/brandmanager")
public class BrandManagerAction {
	@Autowired
	private ProBrandService proBrandService;
	@Autowired
	private ProCategoryBrandService proCategoryBrandService;

	@RequestMapping("/v_list.do")
	public String list(HttpServletRequest request, HttpServletResponse response, ModelMap model, Integer pageNo) {
		Map<String, String> searchMap = RequestUtils.getSearchMap(request);
		Pagination pagination = proBrandService.getProBrandPage(pageNo == null ? 1 : pageNo, 10, searchMap);
		RequestUtils.MapToModelMap(searchMap, request);
		model.put("pagination", pagination);

		return "brandmanager/probrand_list";
	}

	// 添加或编辑
	@RequestMapping("/getBrandById.do")
	public String editBrand(HttpServletRequest request, HttpServletResponse response, ModelMap model, Integer id) {
		if (id != null) {
			Brand probrand = proBrandService.getBrandById(id);
			model.addAttribute("probrand", probrand);
		}

		return "brandmanager/probrand_edit";
	}

	// 删除
	@ResponseBody
	@RequestMapping("/removeBrand.do")
	public AjaxJson delBrand(HttpServletRequest request, HttpServletResponse response,Integer id) {
		try {
			if (id != null) {
				Brand probrand = proBrandService.getBrandById(id);
				probrand.setRemoved(1);//删除
				proBrandService.saveBrand(probrand);
			}
		} catch (Exception e) {
			return AjaxJson.error(e.getMessage());
		}
	
		return AjaxJson.success();
	}

	// 保存或更新
	/**
	 * @Title: saveCateBrand
	 * @Description: TODO
	 * @param request
	 * @param response
	 * @param probrand
	 * @return
	 * @return: AjaxJson
	 * @author: 查恒 2017年1月17日 上午11:31:51
	 */
	@ResponseBody
	@RequestMapping("/o_saveBrand.do")
	public AjaxJson saveCateBrand(HttpServletRequest request, HttpServletResponse response,
			@RequestBody Brand probrand) {
		String ztreeArray = probrand.getZtreeArray();
		List<Map> parseArray = JSON.parseArray(ztreeArray, Map.class);
		try {
			// 有一級就全部包含，二級就全部包含二級，三級就三級
			if (probrand != null && parseArray != null && parseArray.size() > 0) {
				// 保存或更新Brand
				probrand.setRemoved(0);
				probrand.setState(1);
				probrand.setIfSelf(0);
				proBrandService.saveBrand(probrand);
				proCategoryBrandService.CategoryBrandHandle(probrand, parseArray);
			} else {
				return AjaxJson.error("请填写信息完整");
			}
			return AjaxJson.success();
		} catch (Exception e) {
			return AjaxJson.error(e.getMessage());
		}
	}

}
