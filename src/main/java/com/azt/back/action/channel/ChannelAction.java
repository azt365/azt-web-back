package com.azt.back.action.channel;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.azt.api.pojo.Channel;
import com.azt.api.pojo.Function;
import com.azt.api.service.ChannelService;
import com.azt.model.easy.EasyTree;

import net.sf.json.JSONObject;

/**
 * 栏目管理
 * @author 张栋  2016年4月29日上午9:16:26
 */
@Controller
@RequestMapping("channel")
public class ChannelAction {

	@Autowired
	private ChannelService cs;
	
	@RequestMapping(value = "/v_list.do")
	public String channels(HttpServletRequest request, HttpServletResponse response ){
		return "channel/channel_list";
	}
	
	@RequestMapping(value="/v_tree.do")
	@ResponseBody
	public List<EasyTree> ajaxForTree(){
		return cs.getChannelTree();
	}
	
	@RequestMapping(value="o_add.do")
	@ResponseBody
	public JSONObject ajaxAdd(Function func,HttpServletRequest request,
			HttpServletResponse response, ModelMap model ,Channel channel){
		JSONObject json = new JSONObject();
		try{
			if(channel.getId()!=null&&!"".equals(channel.getId())){
				json.put("msg" , "编辑成功");
				Channel oldChannel  =cs.getChannelById(channel.getId());
				oldChannel.setChannelName(channel.getChannelName());
				oldChannel.setCode(channel.getCode());
				oldChannel.setUpdatetime(new Date());
				this.cs.saveOrUpdate(oldChannel);
			}else{
				json.put("msg" , "新增成功");
				channel.setCreatetime(new Date());
				this.cs.saveOrUpdate(channel);
			}
			json.put("success", true);
		}catch(Exception e){
			e.printStackTrace();
			json.put("success", false);
			json.put("msg",	"操作失败");
		}
		return json;
	}
	
	
	
	@RequestMapping(value="o_del.do")
	@ResponseBody
	public JSONObject ajaxDel(Function func,HttpServletRequest request,
			HttpServletResponse response, ModelMap model ,Channel channel){
		JSONObject json = new JSONObject();
		try{
			if(channel.getId()!=null&&!"".equals(channel.getId())){
				json.put("msg" , "删除成功");
				Channel c =  cs.getChannelById(channel.getId());
//				List<Channel> childrens = this.getChannelChildrens(c);
				cs.deleteAllEntitie(c.getId());
				json.put("success", true);
			}else{
				json.put("msg" , "删除失败，记录不存在");
				json.put("success", false);
			}
		}catch(Exception e){
			e.printStackTrace();
			json.put("success", false);
			json.put("msg",	"删除失败");
		}
		return json;
	}
	
//	private List<Channel> getChannelChildrens(Channel channel){
//		List<Channel> clists = new ArrayList<Channel>();
//		clists.add(channel);
//		if(channel.getChildren() != null && channel.getChildren().size() > 0){
//			//clists.addAll(channel.getChildren());
//			for(Channel c : channel.getChildren()){
//				clists.addAll(getChannelChildrens(c));
//			}
//		}
//		return clists;
//	}
	
}
