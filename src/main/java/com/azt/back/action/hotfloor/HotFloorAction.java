package com.azt.back.action.hotfloor;

import com.azt.api.pojo.ProHotFloor;
import com.azt.api.service.ProHotFloorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

/**
 * 爆品楼层
 * Created by LiQZ on 2017/3/17.
 */
@Controller
@RequestMapping("hot_floor")
public class HotFloorAction {

    @Autowired
    private ProHotFloorService proHotFloorService;

    /**
     * 爆品楼层列表页
     */
    @RequestMapping("list.do")
    public String list(Model model) {
        // 查询爆品楼层
        List<ProHotFloor> floors = proHotFloorService.listProHotFloor();
        model.addAttribute("floors", floors);
        return "hotfloor/hot_floor_list";
    }

}
