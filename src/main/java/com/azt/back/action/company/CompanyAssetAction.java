package com.azt.back.action.company;

import com.azt.api.pojo.Admin;
import com.azt.api.pojo.CompanyAsset;
import com.azt.api.service.CompanyAssetService;
import com.azt.back.action.BaseAction;
import com.azt.back.util.ContextUtils;
import com.azt.back.util.Globals;
import com.azt.model.AjaxJson;
import com.azt.model.page.Pagination;
import com.azt.utils.CommonUtil;
import com.azt.utils.RequestUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Map;

/**
 * 公司资质
 * Created by LiQZ on 2017/4/18.
 */
@Controller
@RequestMapping("company")
public class CompanyAssetAction extends BaseAction {

    private static final Logger logger = LoggerFactory.getLogger(CompanyAssetAction.class);

    @Autowired
    private CompanyAssetService companyAssetService;

    @RequestMapping("/asset/list.do")
    public String list(Model model) {
        Map searchMap = CommonUtil.getParameterMap(request);
        int pageNo = getParaToInt("pageNo", 1);
        Pagination pagination = companyAssetService.listPage(pageNo, Globals.PAGE_SIZE, searchMap);
        model.addAttribute("pagination", pagination);
        RequestUtils.MapToModelMap(searchMap, request);
        return "company/asset/list";
    }

    @ResponseBody
    @RequestMapping("/asset/delete.do")
    public AjaxJson delete(Integer id) {
        Admin admin = ContextUtils.getAdmin();
        logger.info("delete company asset by {} id is {}", admin.getId(), id);
        companyAssetService.delete(id);
        return AjaxJson.success();
    }

    @RequestMapping("/asset/detail.do")
    public String detail(Integer id, Model model) {
        CompanyAsset asset = companyAssetService.get(id);
        model.addAttribute("asset", asset);
        return "company/asset/detail";
    }

    @RequestMapping("/asset/check.do")
    public String check(Integer id, Integer status) {
        companyAssetService.updateStatus(id, status);
        return "redirect:/company/asset/detail.do?id=" + id;
    }

}
