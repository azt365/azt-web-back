package com.azt.back.action.msg;

import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.azt.api.pojo.Dictionary;
import com.azt.api.pojo.MsgConfig;
import com.azt.api.service.DictionaryService;
import com.azt.api.service.MsgConfigService;
import com.azt.back.util.Globals;
import com.azt.model.AjaxJson;
import com.azt.model.page.Pagination;
import com.azt.utils.RequestUtils;

/**
 * @ClassName: MsgConfigAction
 * @Description: TODO(消息管理)
 * @author zhoutianchi
 * @date 2016年8月10日 下午3:07:36
 *       
 */
@Controller
@RequestMapping("/msg")
public class MsgConfigAction
{
    @Autowired
    private MsgConfigService aztMsgService;
    
    @Autowired
    private DictionaryService dictionaryService;
    
    
    /**
     * 列表页面
     * 
     * @author 周天驰
     * @param request
     * @param pageNo
     * @param response
     * @param model
     * @return String
     * @see [类、类#方法、类#成员]
     */
    @RequestMapping(value = "/v_list.do")
    public String msgList(HttpServletRequest request, Integer pageNo, HttpServletResponse response, ModelMap model)
    {
        Map<String, String> searchMap = RequestUtils.getSearchMap(request);
        Pagination pagination = aztMsgService.getAztMsgPage(pageNo == null ? 1 : pageNo, Globals.PAGE_SIZE, searchMap);
        model.put("pagination", pagination);
        
        List<Dictionary> dictionarys = dictionaryService.getDictionByParentCode("msgType");
        model.put("dictionarys", dictionarys);
        
        RequestUtils.MapToModelMap(searchMap, request);
        return "msg/msg_list";
    }
    
    
	@RequestMapping(value = "/v_add.do")
	public String addMsg(HttpServletRequest request, HttpServletResponse response, ModelMap model)
	{
        List<Dictionary> dictionarys = dictionaryService.getDictionByParentCode("msgType");
        model.put("dictionarys", dictionarys);
		return "msg/msg_add";
	}
	
	@RequestMapping(value = "/v_edit.do")
	public String editMsg(HttpServletRequest request, HttpServletResponse response, ModelMap model)
	{
		Map<String, String> searchMap = RequestUtils.getSearchMap(request);
		Integer id = Integer.parseInt(searchMap.get("so_id"));
		MsgConfig msg = aztMsgService.queryById(id);
		model.put("msg", msg);
		
        List<Dictionary> dictionarys = dictionaryService.getDictionByParentCode("msgType");
        model.put("dictionarys", dictionarys);
		return "msg/msg_edit";
	}
    /**
     * 根据id删除
     * 
     * @author 周天驰
     * @param aztmsg
     * @param request
     * @param response
     * @param model
     * @return
     * @see [类、类#方法、类#成员]
     */
    @RequestMapping(value = "/ajax_del.do", method = RequestMethod.GET)
    @ResponseBody
    public AjaxJson ajaxDel(MsgConfig aztmsg, HttpServletRequest request, HttpServletResponse response, ModelMap model)
    {
        Integer id = aztmsg.getId();
        if (id != null && !"".equals(id))
        {
            // 删除父子级
            aztMsgService.deleteAllById(id);
        }
        return AjaxJson.success();
    }
    
    /**
     * 保存新增
     * 
     * @author 周天驰
     * @param aztmsg
     * @return
     * @see [类、类#方法、类#成员]
     */
    
    @RequestMapping(value = "/o_add.do", method = RequestMethod.POST)
    public String saveMsg(HttpServletRequest request,MsgConfig aztMsg)
    {
    	String[] sendtype = request.getParameterValues("sendtype");
    	for(String type : sendtype){
    		if(type.equals("mobile")){aztMsg.setIfMobile(1);}
    		if(type.equals("email")){aztMsg.setIfEmail(1);}
    		if(type.equals("mail")){aztMsg.setIfMail(1);}
    	}
        boolean exist = aztMsgService.findOrtherMsg(aztMsg);
        if (!exist)
        {
            aztMsg.setCreatetime(new Date());
            aztMsgService.saveMsg(aztMsg);
        }
        return "redirect:/msg/v_list.do";
    }
    
    /**
     * 保存编辑
     * 
     * @author 周天驰
     * @param aztmsg
     * @return
     * @see [类、类#方法、类#成员]
     */
    
    @RequestMapping(value = "/o_edit.do", method = RequestMethod.POST)
    public String saveEdit(HttpServletRequest request, MsgConfig aztmsg)
    {
    	String a = "mail";
    	String[] sendtype = request.getParameterValues("sendtype");
    	for(String type : sendtype){
    		if(type.equals("mobile")){aztmsg.setIfMobile(1);}
    		if(type.equals("email")){aztmsg.setIfEmail(1);}
    		if(type.equals("mail")){aztmsg.setIfMail(1);}
    	}
        boolean exist = aztMsgService.findOrtherMsg(aztmsg);
        if (!exist)
        {
            aztMsgService.saveEdit(aztmsg);
        }
        return "redirect:/msg/v_list.do";
    }
}
