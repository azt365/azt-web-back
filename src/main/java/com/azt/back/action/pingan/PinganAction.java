package com.azt.back.action.pingan;

import com.azt.api.enums.BatchTypeEnum;
import com.azt.api.enums.BusOperationFlag;
import com.azt.api.service.PinganAccountService;
import com.azt.back.action.BaseAction;
import com.azt.back.util.Globals;
import com.azt.exception.BaseException;
import com.azt.model.AjaxJson;
import com.azt.model.page.Pagination;
import com.azt.utils.HttpClientUtil;
import com.azt.utils.PabankPropertiesUtils;
import com.azt.utils.RequestUtils;
import net.sf.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * 后台平安银行业务类
 *
 * Created by zouheyuan on 2017/2/13.
 */
@Controller
@RequestMapping(value = "/pingan")
public class PinganAction extends BaseAction {

    public static Logger logger = LoggerFactory.getLogger("paInfo");
    public static Logger loggerError = LoggerFactory.getLogger("paError");
    @Autowired
    private PinganAccountService pinganAccountService;

    /**
     * 平安银行每天凌晨三点进行账户信息对账
     * 核对交易网和平安银行间账户的一致。
     */
//    @RequestMapping(value = "/quartzCheckAccount.do")
//    public void checkAccountQuartz() {
//        logger.info("==========定时任务开始核对账户信息==========");
//        try {
//            pinganAccountService.checkAccountQuartz();
//            logger.info("==========定时任务核对账户信息完成==========");
//        }catch (Exception e) {
//            logger.info("==========定时任务核对账户出现未知错误==========");
//            LOGGER.error("==========定时任务核对账户出现未知错误：" + e.getMessage() + "==========");
////            e.printStackTrace();
//        }
//
//    }

    /**
     *解冻列表页面
     * @return
     */
    @RequestMapping("/thawList.do")
    public String thawList(HttpServletRequest request, ModelMap model, Integer pageNo) {

        Map<String, String> searchMap = RequestUtils.getSearchMap(request);

        Pagination pagination = pinganAccountService.getThawPage(pageNo == null ? 1 : pageNo, Globals.PAGE_SIZE, searchMap);

        model.put("pagination", pagination);

        // 项目状态
        Map<String, String> getAllTypes = BatchTypeEnum.getAllTypes();
        model.put("allTypes", getAllTypes);

        RequestUtils.MapToModelMap(searchMap, request);

        return "pingan/thaw_list";
    }

    /**
     *交易操作列表
     * @return
     */
    @RequestMapping("/busOperationList.do")
    public String busOperationList(HttpServletRequest request, ModelMap model, Integer pageNo) {

        Map<String, String> searchMap = RequestUtils.getSearchMap(request);

        Pagination pagination = pinganAccountService.getOperationPage(pageNo == null ? 1 : pageNo, Globals.PAGE_SIZE, searchMap);

        model.put("pagination", pagination);

        // 项目状态
        Map<String, String> getAllFlags = BusOperationFlag.getAllFlags();
        model.put("allFlags", getAllFlags);

        RequestUtils.MapToModelMap(searchMap, request);

        return "pingan/bus_operation_list";
    }

    /**
     * 解冻功能
     *
     * @param payNumber    支付订单流水   唯一依据
     * @return
     */
    @RequestMapping(value = "/thaw.do", method = RequestMethod.POST)
    @ResponseBody
    public String thaw(String payNumber) {
        Map<String , String > params = new HashMap<String , String>();
        params.put("payNumber", payNumber);
        String res = HttpClientUtil.doPost(PabankPropertiesUtils.readProperites("frontAddress") + "/pingan/thaw", params);
        return res;
    }

}
