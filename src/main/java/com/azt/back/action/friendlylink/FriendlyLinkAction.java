package com.azt.back.action.friendlylink;

import java.util.Date;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.azt.api.pojo.Dictionary;
import com.azt.api.pojo.FriendlyLink;
import com.azt.api.service.FriendlyLinkService;
import com.azt.back.util.Globals;
import com.azt.model.AjaxJson;
import com.azt.model.page.Pagination;
import com.azt.utils.RequestUtils;

import net.sf.json.JSONObject;

/**
 * @ClassName: FriendlyLinkAction
 * @Description: 友情链接
 * @author zhoutianchi
 * @date 2016年8月10日 下午3:07:36
 *       
 */
@Controller
@RequestMapping("/friendly_link")
public class FriendlyLinkAction
{
	@Autowired
	private FriendlyLinkService friendlyLinkService;
	

    
    
    /**
     * 列表页面
     * 
     * @author 周天驰
     * @param request
     * @param pageNo
     * @param response
     * @param model
     * @return String
     * @see [类、类#方法、类#成员]
     */
    @RequestMapping(value = "/v_list.do")
    public String friendlyLinkList(HttpServletRequest request, Integer pageNo, HttpServletResponse response, ModelMap model)
    {
        Map<String, String> searchMap = RequestUtils.getSearchMap(request);
        Pagination pagination = friendlyLinkService.getFriendlyLinkPage(pageNo == null ? 1 : pageNo, Globals.PAGE_SIZE, searchMap);
        model.put("pagination", pagination);
        
        
        RequestUtils.MapToModelMap(searchMap, request);
        return "friendlylink/friendlylink_list";
    }
    
    /**
     * 根据id删除
     * 
     * @author 周天驰
     * @param friendlyLink
     * @param request
     * @param response
     * @param model
     * @return
     * @see [类、类#方法、类#成员]
     */
    @RequestMapping(value = "/ajax_del.do", method = RequestMethod.GET)
    @ResponseBody
    public AjaxJson ajaxDel(FriendlyLink friendlyLink, HttpServletRequest request, HttpServletResponse response, ModelMap model)
    {
        friendlyLinkService.deleteFriendlyLink(friendlyLink);
        return AjaxJson.success();
    }
    
    /**
     * 保存新增
     * 
     * @author 周天驰
     * @param friendlyLink
     * @return
     * @see [类、类#方法、类#成员]
     */
    
    @RequestMapping(value = "/ajax_save.do", method = RequestMethod.POST)
    @ResponseBody
    public AjaxJson saveFriendlyLink(FriendlyLink friendlyLink)
    {

        friendlyLink.setCreatetime(new Date());
        friendlyLinkService.saveOrUpdateFriendlyLink(friendlyLink);
        return AjaxJson.success();
    }
    
    /**
     * 保存编辑
     * 
     * @author 周天驰
     * @param friendlyLink
     * @return
     * @see [类、类#方法、类#成员]
     */
    
    @RequestMapping(value = "/ajax_edit.do", method = RequestMethod.POST)
    @ResponseBody
    public AjaxJson saveEdit(FriendlyLink friendlyLink)
    {
        friendlyLinkService.saveOrUpdateFriendlyLink(friendlyLink);
        return AjaxJson.success();
    }
    
    @RequestMapping(value = "/changeSort.do", method = RequestMethod.POST)
    @ResponseBody
    public JSONObject changeSort(HttpServletRequest request, HttpServletResponse response,
        ModelMap model, Integer id, Integer sort)
    {
        JSONObject json = new JSONObject();
        try
        {
        	FriendlyLink link = this.friendlyLinkService.getFriendlyLinkById(id);
        	link.setSeq(sort);
            this.friendlyLinkService.saveOrUpdateFriendlyLink(link);
            json.put("res", true);
        }
        catch (Exception e)
        {
            e.printStackTrace();
            json.put("res", false);
        }
        return json;
    }

}
