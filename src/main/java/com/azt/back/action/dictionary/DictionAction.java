/*
 * 文 件 名:  DictionaryAction.java
 * 版    权:  ZF Technologies Co., Ltd. Copyright 2013-,  All rights reserved
 * 描    述:  <描述>
 * 修 改 人:  张栋
 * 修改时间:  2014-4-11
 * 跟踪单号:  <跟踪单号>
 * 修改单号:  <修改单号>
 * 修改内容:  <修改内容>
 */
package com.azt.back.action.dictionary;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.azt.api.pojo.Dictionary;
import com.azt.api.service.DictionaryService;
import com.azt.model.AjaxJson;

import net.sf.json.JSONObject;

/**
 * 字典
 */
@Controller
public class DictionAction
{
    
    @Autowired
    private DictionaryService manager;
    
    @RequestMapping(value = "diction/v_diction.do", method = RequestMethod.GET)
    public String dictionslist(HttpServletRequest request, HttpServletResponse response, ModelMap model)
    {
        // 子菜单的父节点id
        String nextp = request.getParameter("nextP");
        String prevp = request.getParameter("prevP");
        
        Integer parentId = 0;
        Integer prevParent = 0;
        Dictionary d = null;
        if (nextp != null && StringUtils.isNumeric(nextp))
        {
            parentId = Integer.parseInt(nextp);
        }
        if (parentId != 0)
        {
            d = this.manager.getDictionById(parentId);
            model.addAttribute("parentCode", d.getCode());
        }
        List<Dictionary> dictions = manager.getDictionByParentId(parentId);
        
        if (prevp == null)
        {
            if (parentId != 0)
            {
                if (dictions.size() != 0)
                {
                    prevParent = manager.getDictionById(parentId).getParentId();
                }
            }
        }
        else
        {
            prevParent = Integer.parseInt(prevp);
        }
        // 当前目录下的所有
        model.addAttribute("dictions", dictions);
        // 当前目录的父节点
        model.addAttribute("parentId", parentId);
        // 用来返回上一层
        model.addAttribute("prevParent", prevParent);
        return "diction/diction_list";
    }
    
    /**
     * 保存或者更新
     * 
     * @author 张栋
     * @param diction
     * @param request
     * @param response
     * @param model
     * @return
     * @see [类、类#方法、类#成员]
     */
    @RequestMapping(value = "diction/ajax_save.do", method = RequestMethod.POST)
    @ResponseBody
    public AjaxJson ajaxSave(Dictionary diction, HttpServletRequest request, HttpServletResponse response,
        ModelMap model)
    {
        AjaxJson j = new AjaxJson();
        Integer id = diction.getId();
        boolean exist = manager.findOrtherDiction(diction);
        if (exist)
        {
            j.setSuccess(false);
            j.setMsg("该code值已经存在");
            return j;
        }
        if (id != null && !"".equals(id))
        {
            Dictionary old = manager.getDictionById(id);
            old.setName(diction.getName());
            old.setSortnum(diction.getSortnum());
            old.setCode(diction.getCode());
            old.setDescr(diction.getDescr());
            manager.saveOrUpdate(old);
        }
        else
        {
            manager.save(diction);
        }
        return j;
    }
    
    @RequestMapping(value = "diction/changeSort.do", method = RequestMethod.POST)
    @ResponseBody
    public JSONObject changeSort(Dictionary diction, HttpServletRequest request, HttpServletResponse response,
        ModelMap model, Integer id, Integer sort)
    {
        JSONObject json = new JSONObject();
        try
        {
            Dictionary d = this.manager.getDictionById(id);
            d.setSortnum(sort);
            this.manager.saveOrUpdate(d);
            json.put("res", true);
        }
        catch (Exception e)
        {
            e.printStackTrace();
            json.put("res", false);
        }
        return json;
    }
    
    /**
     * 删除
     * 
     * @author 张栋
     * @param diction
     * @param request
     * @param response
     * @param model
     * @return
     * @see [类、类#方法、类#成员]
     */
    @RequestMapping(value = "diction/ajax_del.do", method = RequestMethod.GET)
    @ResponseBody
    public AjaxJson ajaxDel(Dictionary diction, HttpServletRequest request, HttpServletResponse response,
        ModelMap model)
    {
        AjaxJson j = new AjaxJson();
        Integer id = diction.getId();
        if (id != null && !"".equals(id))
        {
            // 删除父子级
            manager.deleteAllById(id);
        }
        return j;
    }
    
}
