package com.azt.back.action.recommendbrand;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.azt.api.pojo.Category;
import com.azt.api.pojo.RecommendBrand;
import com.azt.api.service.ProCategoryService;
import com.azt.api.service.RecommendBrandService;
import com.azt.back.util.Globals;
import com.azt.model.AjaxJson;
import com.azt.model.page.Pagination;
import com.azt.utils.RequestUtils;

import net.sf.json.JSONObject;

/**
 * @ClassName: RecommendBrandAction
 * @Description: 推荐pinp
 * @author zhoutianchi
 * @date 2016年12月16日 下午3:07:36
 * 
 */
@Controller
@RequestMapping("/recommendbrand")
public class RecommendBrandAction {
	@Autowired
	private RecommendBrandService recommendBrandService;

	@Autowired
	private ProCategoryService ps;

	/**
	 * 列表页面
	 * 
	 * @author 周天驰
	 * @param request
	 * @param pageNo
	 * @param response
	 * @param model
	 * @return String
	 * @see [类、类#方法、类#成员]
	 */
	@RequestMapping(value = "/v_list.do")
	public String contractList(HttpServletRequest request, Integer pageNo, HttpServletResponse response,
			ModelMap model) {
		Map<String, String> searchMap = RequestUtils.getSearchMap(request);
		Pagination pagination = recommendBrandService.getRecommendBrandPage(pageNo == null ? 1 : pageNo,
				Globals.PAGE_SIZE, searchMap);
		model.put("pagination", pagination);

		RequestUtils.MapToModelMap(searchMap, request);
		return "recommendbrand/recommendbrand_list";
	}

	@RequestMapping(value = "/v_add.do")
	public String addContract(HttpServletRequest request, HttpServletResponse response, ModelMap model) {
		List<Category> categorys = ps.findTopCategory();// 经营系统
		model.put("categorys", categorys);
		return "recommendbrand/recommendbrand_add";
	}

	@RequestMapping(value = "/v_edit.do")
	public String editContract(HttpServletRequest request, HttpServletResponse response, ModelMap model) {
		List<Category> categorys = ps.findTopCategory();// 经营系统
		model.put("categorys", categorys);

		Map<String, String> searchMap = RequestUtils.getSearchMap(request);
		Integer id = Integer.parseInt(searchMap.get("so_id"));
		RecommendBrand recommendbrand = recommendBrandService.queryById(id);
		model.put("recommendbrand", recommendbrand);

		String catrgory = recommendbrand.getBrandCategory();
		String[] cats = null;
		if (null != catrgory && "" != catrgory) {
			cats = catrgory.split(",");
		}
		model.put("cats", cats);
		return "recommendbrand/recommendbrand_edit";
	}

	/**
	 * 根据id删除
	 * 
	 * @author 周天驰
	 * @param recommendBrand
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 * @see [类、类#方法、类#成员]
	 */
	@RequestMapping(value = "/ajax_del.do", method = RequestMethod.GET)
	@ResponseBody
	public AjaxJson ajaxDel(RecommendBrand recommendBrand, HttpServletRequest request, HttpServletResponse response,
			ModelMap model) {
		recommendBrandService.deleteRecommendBrand(recommendBrand);
		return AjaxJson.success();
	}

	/**
	 * 保存新增
	 * 
	 * @author 周天驰
	 * @param recommendBrand
	 * @return
	 * @see [类、类#方法、类#成员]
	 */

	@RequestMapping(value = "/o_add.do", method = RequestMethod.POST)
	public String addRecommendBrand(HttpServletRequest request, RecommendBrand recommendBrand) {
		recommendBrandService.saveOrUpdateRecommendBrand(recommendBrand);
		return "redirect:/recommendbrand/v_list.do";
	}

	/**
	 * 保存编辑
	 * 
	 * @author 周天驰
	 * @param recommendBrand
	 * @return
	 * @see [类、类#方法、类#成员]
	 */

	@RequestMapping(value = "/o_edit.do", method = RequestMethod.POST)
	public String editRecommendBrand(HttpServletRequest request, RecommendBrand recommendBrand) {
		recommendBrandService.saveOrUpdateRecommendBrand(recommendBrand);
		return "redirect:/recommendbrand/v_list.do";
	}

	@RequestMapping(value = "/changeSort.do", method = RequestMethod.POST)
	@ResponseBody
	public JSONObject changeSort(HttpServletRequest request, HttpServletResponse response, ModelMap model, Integer id,
			Integer sort) {
		JSONObject json = new JSONObject();
		try {
			RecommendBrand recommendBrand = this.recommendBrandService.queryById(id);
			recommendBrand.setSeq(sort);
			this.recommendBrandService.saveOrUpdateRecommendBrand(recommendBrand);
			json.put("res", true);
		} catch (Exception e) {
			e.printStackTrace();
			json.put("res", false);
		}
		return json;
	}
}
